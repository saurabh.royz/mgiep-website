//Client side of MGIEP Website
var DashManager = new Backbone.Marionette.Application();
//Initialize variables and functions
var ENTER_KEY = 13;
var DELETE_KEY = 46;
var BACKSPACE_KEY = 8;
var ESCAPE_KEY = 27;
//Maximum upload file size
var MAX_FILE_SIZE = 51457280;
//Page size
var PAGE_SIZE = 20;
//Scroll value and scroll handler
var prevScroll, scrollHandler;
//Variable to save link data
var linkEmbedData;
//Find timer
var findTimer;
//Data array for each block
var blockDataArray = [];
//Calendar events array
var calEventsArray = [];
//Carousel array
var carouselImagesArray = [];
//Add regions of the application
DashManager.addRegions({
    mainRegion: '.mainWrap',
    overlayRegion: '.overlay',
    modalRegion: '.modal'
});
//Navigate function to change url
DashManager.navigate = function(route, options){
    options || (options = {});
    Backbone.history.navigate(route, options);
};
//Find current route
DashManager.getCurrentRoute = function(){
  return Backbone.history.fragment;
};
//Start
DashManager.on('start', function(){
    if(Backbone.history){
        Backbone.history.start({pushState: true});
    }
    //Show pages
    $('.js-pages').click(function(ev){
        if(ev.metaKey || ev.ctrlKey) return;
        ev.preventDefault();
        DashManager.vent.trigger('pages:show');
    });
    //Show articles
    $('.js-articles').click(function(ev){
        if(ev.metaKey || ev.ctrlKey) return;
        ev.preventDefault();
        DashManager.vent.trigger('articles:show');
    });
    //Show files
    $('.js-files').click(function(ev){
        if(ev.metaKey || ev.ctrlKey) return;
        ev.preventDefault();
        DashManager.vent.trigger('files:show');
    });
    //Show people
    $('.js-people').click(function(ev){
        if(ev.metaKey || ev.ctrlKey) return;
        ev.preventDefault();
        DashManager.vent.trigger('people:show');
    });
    //Show site settings
    $('.js-settings').click(function(ev){
        if(ev.metaKey || ev.ctrlKey) return;
        ev.preventDefault();
        DashManager.vent.trigger('settings:show');
    });
    //Show activity
    $('.js-activity').click(function(ev){
        if(ev.metaKey || ev.ctrlKey) return;
        ev.preventDefault();
        DashManager.vent.trigger('activity:show');
    });
    //Show profile
    $('.js-profile').click(function(ev){
        if(ev.metaKey || ev.ctrlKey) return;
        ev.preventDefault();
        DashManager.vent.trigger('profile:show');
    });
});
//Show overlay
DashManager.commands.setHandler('show:overlay', function(){
    //Hide scroll on main page
    prevScroll = $('body').scrollTop();
    $('body').css('overflow', 'hidden');
    if($('body').width() < 700 || $('html').hasClass('touchevents')){
        $('html').css('overflow', 'hidden');
        $('html, body').css('position', 'fixed');
    }
    $('body').scrollTop(prevScroll);
    //Add box-shadow
    setTimeout(function(){
        $('.overlay').addClass('with-shadow');
    }, 300);
});
DashManager.commands.setHandler('close:overlay', function(view){
    if(!$('.overlay > div').length) return;
    //Check if modal is present
    if($('.modal > div').length){
        //remove modal
        $('.modal-box').removeClass('animate');
        $('.modal > div').remove();
        $('.modal').hide();
        //remove animate class on overlay box
        $('.overlay').removeClass('with-shadow');
        $('.overlay-box').removeClass('animate');
        //after animation, remove view, change route and hide overlay
        setTimeout(function(){
            $('.overlay > div').remove();
            $('.overlay').hide();
            $('html, body').css('overflow', '').css('position', '');
            if(prevScroll) {
                $('html, body').scrollTop(prevScroll);
                prevScroll = '';
            }
        }, 300);
    } else {
        //remove animate class on overlay box
        $('.overlay').removeClass('with-shadow');
        $('.overlay-box').removeClass('animate');
        //after animation, remove view, change route and hide overlay
        setTimeout(function(){
            $('.overlay > div').remove();
            $('.overlay').hide();
            $('html, body').css('overflow', '').css('position', '');
            if(prevScroll) {
                $('html, body').scrollTop(prevScroll);
                prevScroll = '';
            }
        }, 300);
    }
});
DashManager.commands.setHandler('close:modal', function(view){
    if(!$('.modal > div').length) return;
    //remove animate class on modal box
    $('.modal-box').removeClass('animate');
    //after animation, remove view, change route and hide overlay
    setTimeout(function(){
        $('.modal > div').remove();
        $('.modal').hide();
    }, 300);
});
//Router of the application
DashManager.module('DashApp', function (DashApp, DashManager, Backbone, Marionette, $, _) {
    DashApp.Router = Marionette.AppRouter.extend({
        appRoutes: {
            'dashboard' : 'pagesView',
            'dashboard/page/:page_id': 'pageBlocksView',
            'dashboard/articles' : 'articlesView',
            'dashboard/articles/:category' : 'articlesView',
            'dashboard/article/:article_id': 'articleBlocksView',
            'dashboard/files' : 'filesView',
            'dashboard/people': 'peopleView',
            'dashboard/people/:type': 'peopleView',
            'dashboard/settings': 'settingsView',
            'dashboard/activity': 'activityView',
            'dashboard/profile': 'profileView'
        }
    });
    //API functions for each route
    var API = {
        newPageOverlayView: function(){
            DashManager.DashApp.EntityController.Controller.showNewPageOverlay();
        },
        editPageOverlayView: function(page_id){
            DashManager.DashApp.EntityController.Controller.showEditPageOverlay(page_id);
        },
        pagesView: function(){
            DashManager.DashApp.EntityController.Controller.showPages();
        },
        pageBlocksView: function(page_id){
            DashManager.DashApp.EntityController.Controller.showPageBlocks(page_id);
        },
        allBlocksOverlayView: function(){
            DashManager.DashApp.EntityController.Controller.showAllBlocksOverlay();
        },
        newBlockOverlayView: function(type){
            DashManager.DashApp.EntityController.Controller.showNewBlockOverlay(type);
        },
        editBlockOverlayView: function(block_id, container_id){
            DashManager.DashApp.EntityController.Controller.showEditBlockOverlay(block_id, container_id);
        },
        editThemeOverlayView: function(block_id, container_id){
            DashManager.DashApp.EntityController.Controller.showEditThemeOverlay(block_id, container_id);
        },
        blockTagsOverlayView: function(block_id){
            DashManager.DashApp.EntityController.Controller.showBlockTagsOverlay(block_id);
        },
        newArticleOverlayView: function(){
            DashManager.DashApp.EntityController.Controller.showNewArticleOverlay();
        },
        articlePersonsOverlayView: function(article_id){
            DashManager.DashApp.EntityController.Controller.showArticlePersonsOverlay(article_id);
        },
        relatedPagsOverlayView: function(article_id){
            DashManager.DashApp.EntityController.Controller.showRelatedPagesOverlay(article_id);
        },
        articleTagsOverlayView: function(article_id){
            DashManager.DashApp.EntityController.Controller.showArticleTagsOverlay(article_id);
        },
        editArticleOverlayView: function(article_id){
            DashManager.DashApp.EntityController.Controller.showEditArticleOverlay(article_id);
        },
        editContentOverlayView: function(article_id){
            DashManager.DashApp.EntityController.Controller.showEditContentOverlay(article_id);
        },
        articlesView: function(category){
            DashManager.DashApp.EntityController.Controller.showArticles(category);
        },
        articleBlocksView: function(article_id){
            DashManager.DashApp.EntityController.Controller.showArticleBlocks(article_id);
        },
        allArticleBlocksOverlayView: function(){
            DashManager.DashApp.EntityController.Controller.showAllArticleBlocksOverlay();
        },
        newArticleBlockOverlayView: function(type){
            DashManager.DashApp.EntityController.Controller.showNewArticleBlockOverlay(type);
        },
        editArticleBlockOverlayView: function(block_id){
            DashManager.DashApp.EntityController.Controller.showEditArticleBlockOverlay(block_id);
        },
        mcqOptionsOverlayView: function(block_id, is_edit){
            DashManager.DashApp.EntityController.Controller.showMcqOptionsOverlay(block_id, is_edit);
        },
        filesView: function(){
            DashManager.DashApp.EntityController.Controller.showFiles();
        },
        editFileOverlayView: function(file_id){
            DashManager.DashApp.EntityController.Controller.showEditFileOverlay(file_id);
        },
        newPersonOverlayView: function(){
            DashManager.DashApp.EntityController.Controller.showNewPersonOverlay();
        },
        editPersonOverlayView: function(person_id){
            DashManager.DashApp.EntityController.Controller.showEditPersonOverlay(person_id);
        },
        peopleView: function(type){
            DashManager.DashApp.EntityController.Controller.showPeople(type);
        },
        settingsView: function(){
            DashManager.DashApp.EntityController.Controller.showSettings();
        },
        activityView: function(){
            DashManager.DashApp.EntityController.Controller.showActivity();
        },
        profileView: function(){
            DashManager.DashApp.EntityController.Controller.showProfile();
        },
        selectImageModalView: function(){
            DashManager.DashApp.EntityController.Controller.showSelectImageModal();
        }
    };
    //Triggers to particular views
    //Show new page overlay
    DashManager.vent.on('newPageOverlay:show', function(){
        API.newPageOverlayView();
    });
    //Show edit page overlay
    DashManager.vent.on('editPageOverlay:show', function(page_id){
        API.editPageOverlayView(page_id);
    });
    //Show all pages
    DashManager.vent.on('pages:show', function(){
        DashManager.navigate('dashboard');
        API.pagesView();
    });
    //Show page blocks
    DashManager.vent.on('pageBlocks:show', function(page_id){
        DashManager.navigate('dashboard/page/' + page_id);
        API.pageBlocksView(page_id);
    });
    //Show list of all blocks in overlay
    DashManager.vent.on('allBlocksOverlay:show', function(){
        API.allBlocksOverlayView();
    });
    //Show new block overlay
    DashManager.vent.on('newBlockOverlay:show', function(type){
        API.newBlockOverlayView(type);
    });
    //Show edit block overlay
    DashManager.vent.on('editBlockOverlay:show', function(block_id, container_id){
        API.editBlockOverlayView(block_id, container_id);
    });
    //Show edit theme overlay
    DashManager.vent.on('editThemeOverlay:show', function(block_id, container_id){
        API.editThemeOverlayView(block_id, container_id);
    });
    //Show block tags overlay
    DashManager.vent.on('blockTags:show', function(block_id){
        API.blockTagsOverlayView(block_id);
    });
    //Show new article overlay
    DashManager.vent.on('newArticleOverlay:show', function(){
        API.newArticleOverlayView();
    });
    //Show article persons overlay
    DashManager.vent.on('articlePersons:show', function(article_id){
        API.articlePersonsOverlayView(article_id);
    });
    //Show related pages overlay
    DashManager.vent.on('relatedPages:show', function(article_id){
        API.relatedPagsOverlayView(article_id);
    });
    //Show article tags overlay
    DashManager.vent.on('articleTags:show', function(article_id){
        API.articleTagsOverlayView(article_id);
    });
    //Show edit article overlay
    DashManager.vent.on('editArticleOverlay:show', function(article_id){
        API.editArticleOverlayView(article_id);
    });
    //Show edit content overlay
    DashManager.vent.on('editContentOverlay:show', function(article_id){
        API.editContentOverlayView(article_id);
    });
    //Show all articles
    DashManager.vent.on('articles:show', function(category){
        if(category){
            DashManager.navigate('dashboard/articles/' + category);
            API.articlesView(category);
        } else {
            DashManager.navigate('dashboard/articles');
            API.articlesView();
        }
    });
    //Show article blocks
    DashManager.vent.on('articleBlocks:show', function(article_id){
        DashManager.navigate('dashboard/article/' + article_id);
        API.articleBlocksView(article_id);
    });
    //Show list of all articleblocks in overlay
    DashManager.vent.on('allArticleBlocksOverlay:show', function(){
        API.allArticleBlocksOverlayView();
    });
    //Show new articleblock overlay
    DashManager.vent.on('newArticleBlockOverlay:show', function(type){
        API.newArticleBlockOverlayView(type);
    });
    //Show edit articleblock overlay
    DashManager.vent.on('editArticleBlockOverlay:show', function(block_id){
        API.editArticleBlockOverlayView(block_id);
    });
    //Show mcq options overlay
    DashManager.vent.on('mcqOptions:show', function(block_id, is_edit){
        API.mcqOptionsOverlayView(block_id, is_edit);
    });
    //Show all files
    DashManager.vent.on('files:show', function(){
        DashManager.navigate('dashboard/files');
        API.filesView();
    });
    //Show edit file overlay
    DashManager.vent.on('editFileOverlay:show', function(file_id){
        API.editFileOverlayView(file_id);
    });
    //Show new person overlay
    DashManager.vent.on('newPersonOverlay:show', function(){
        API.newPersonOverlayView();
    });
    //Show edit person overlay
    DashManager.vent.on('editPersonOverlay:show', function(person_id){
        API.editPersonOverlayView(person_id);
    });
    //Show all people
    DashManager.vent.on('people:show', function(type){
        if(type){
            DashManager.navigate('dashboard/people/' + type);
            API.peopleView(type);
        } else {
            DashManager.navigate('dashboard/people');
            API.peopleView();
        }
    });
    //Show site settings
    DashManager.vent.on('settings:show', function(){
        DashManager.navigate('dashboard/settings');
        API.settingsView();
    });
    //Show activity
    DashManager.vent.on('activity:show', function(){
        DashManager.navigate('dashboard/activity');
        API.activityView();
    });
    //Show profile
    DashManager.vent.on('profile:show', function(){
        DashManager.navigate('dashboard/profile');
        API.profileView();
    });
    //Show select image modal
    DashManager.vent.on('selectImageModal:show', function(){
        API.selectImageModalView();
    });
    //Show
    //Initialize router with API
    DashManager.addInitializer(function(){
        new DashApp.Router({ controller: API });
    });
});
//Models and Collections of the application
DashManager.module('Entities', function (Entities, DashManager, Backbone, Marionette, $, _) {
    //Page Models and Collections
    Entities.Page = Backbone.Model.extend({
        initialize: function(options){
            this._action = options._action;
            this._id = options._id;
        },
        url: function(){
            if(this._action){
                return '/api/page/' + this._id + '/' + this._action
            } else if(this._id) {
                return '/api/page/' + this._id
            } else {
                return '/api/page'
            }
        },
        idAttribute: '_id'
    });
    Entities.PageCollection = Backbone.Collection.extend({
        initialize: function(models, options){
            //_type is type of pages: all, public etc
            this._type = options._type;
        },
        url: function(){
            return '/api/pages/' + this._type
        },
        model: Entities.Page
    });
    //Block Models and Collection
    Entities.Block = Backbone.Model.extend({
        initialize: function(options){
            //type of block: header, content etc.
            //_action are block actions - edit, add_people etc.
            this._container = options._container;
            this._type = options.type;
            this._action = options._action;
            this._id = options._id;
        },
        url: function(){
            if(this._container){
                if(this._action){
                    return '/api/subblock/' + this._container + '/' + this._id + '/' + this._action
                } else if(this._id) {
                    return '/api/subblock/' + this._container + '/' + this._id
                }
            } else {
                if(this._action){
                    return '/api/block/' + this._id + '/' + this._action
                } else if(this._id) {
                    return '/api/block/' + this._id
                } else if(this._type){
                    return '/api/block/' + this._type
                } else {
                    return '/api/block'
                }
            }
        },
        idAttribute: '_id'
    });
    Entities.BlockCollection = Backbone.Collection.extend({
        initialize: function(models, options){
            //_type is type of blocks: page, content
            this._type = options._type;
            this._category = options._category;
            this._page = options._page
        },
        url: function(){
            if(this._type && this._category){
                return '/api/blocks/' + this._type + '?category=' + this._category + '&page=' + this._page
            } else if(this._type){
                return '/api/blocks/' + this._type + '?page=' + this._page
            }
        },
        model: Entities.Block
    });
    //ArticleBlock Models and Collection
    Entities.ArticleBlock = Backbone.Model.extend({
        initialize: function(options){
            //type of articleblock: text, gallery etc.
            //_action are block actions - edit, add_people etc.
            this._type = options.type;
            this._action = options._action;
            this._id = options._id;
        },
        url: function(){
            if(this._action){
                return '/api/articleblock/' + this._id + '/' + this._action
            } else if(this._id) {
                return '/api/articleblock/' + this._id
            } else if(this._type){
                return '/api/articleblock/' + this._type
            } else {
                return '/api/articleblock'
            }
        },
        idAttribute: '_id'
    });
    Entities.ArticleBlockCollection = Backbone.Collection.extend({
        initialize: function(models, options){
            //_id is article id
            this._id = options._id;
        },
        url: function(){
            return '/api/articleblocks/' + this._id
        },
        model: Entities.ArticleBlock
    });
    //File Models and Collection
    Entities.File = Backbone.Model.extend({
        urlRoot: '/api/file',
        idAttribute: '_id'
    });
    Entities.FileCollection = Backbone.Collection.extend({
        initialize: function(models, options){
            this._type = options._type;
            this._page = options._page
        },
        url: function(){
            if(this._type){
                return '/api/files?type=' + this._type + '&page=' + this._page
            } else {
                return '/api/files' + '?page=' + this._page
            }
        },
        model: Entities.File
    });
    //Person Models and Collection
    Entities.Person = Backbone.Model.extend({
        initialize: function(options){
            this._action = options._action;
            this._id = options._id;
        },
        url: function(){
            if(this._action){
                return '/api/person/' + this._id + '/' + this._action
            } else if(this._id) {
                return '/api/person/' + this._id
            } else {
                return '/api/person'
            }
        },
        idAttribute: '_id'
    });
    Entities.PersonCollection = Backbone.Collection.extend({
        initialize: function(models, options){
            //_type is type of persons: team, partner, author etc
            this._type = options._type;
            this._page = options._page
        },
        url: function(){
            if(this._type){
                return '/api/persons?type=' + this._type + '&page=' + this._page
            } else {
                return '/api/persons' + '?page=' + this._page
            }
        },
        model: Entities.Person
    });
    //Site
    Entities.Site = Backbone.Model.extend({
        initialize: function(options){
            if(options) this._id = options._id;
        },
        url: function(){
            if (this._id) {
                return '/api/site/' + this._id
            } else {
                return '/api/site'
            }
        },
        idAttribute: '_id'
    });
    //Link Preview
    Entities.LinkPreview = Backbone.Model.extend({
        initialize: function(options){
            this._url = options._url;
        },
        url: function(){
            if (this._url) {
                return '/api/embedlink?url=' + this._url
            }
        },
        idAttribute: '_id'
    });
    //App results
    Entities.AppResults = Backbone.Model.extend({
        initialize: function(options){
            this._type = options._type;
            this._search = options._search;
        },
        url: function(){
            if (this._type && this._search) {
                return '/api/search/' + this._type + '?search=' + this._search
            }
        },
        idAttribute: '_id'
    });
    //Functions to get data
    var API = {
        getPages: function(_type){
            var pages = new Entities.PageCollection([], {
                _type: _type
            });
            var defer = $.Deferred();
            pages.fetch({
                success: function(data){
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
        getOnePage: function(_id){
            var page = new Entities.Page({
                _id: _id
            });
            var defer = $.Deferred();
            page.fetch({
                success: function(data){
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
        getArticles: function(_category, _page){
            var blocks = new Entities.BlockCollection([], {
                _type: 'content',
                _category: _category,
                _page: _page
            });
            var defer = $.Deferred();
            blocks.fetch({
                success: function (data) {
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
        getOneBlock: function(_id, _container){
            var block = new Entities.Block({
                _id: _id,
                _container: _container
            });
            var defer = $.Deferred();
            block.fetch({
                success: function(data){
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
        getArticleBlocks: function(_id){
            var articleBlocks = new Entities.ArticleBlockCollection([], {
                _id: _id
            });
            var defer = $.Deferred();
            articleBlocks.fetch({
                success: function (data) {
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
        getOneArticleBlock: function(_id){
            var block = new Entities.ArticleBlock({
                _id: _id
            });
            var defer = $.Deferred();
            block.fetch({
                success: function(data){
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
        getFiles: function(_type, _page){
            var files = new Entities.FileCollection([], {
                _type: _type,
                _page: _page
            });
            var defer = $.Deferred();
            files.fetch({
                success: function (data) {
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
        getOneFile: function(_id){
            var file = new Entities.File({
                _id: _id
            });
            var defer = $.Deferred();
            file.fetch({
                success: function(data){
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
        getPersons: function(_type, _page){
            var persons = new Entities.PersonCollection([], {
                _type: _type,
                _page: _page
            });
            var defer = $.Deferred();
            persons.fetch({
                success: function (data) {
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
        getOnePerson: function(_id){
            var person = new Entities.Person({
                _id: _id
            });
            var defer = $.Deferred();
            person.fetch({
                success: function(data){
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
        getSiteDetails: function(){
            var site = new Entities.Site();
            var defer = $.Deferred();
            site.fetch({
                success: function (data) {
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
        getLinkPreview: function(_url){
            var linkpreview = new Entities.LinkPreview({
                _url: _url
            });
            var defer = $.Deferred();
            linkpreview.fetch({
                success: function (data) {
                    defer.resolve(data);
                }, error: function(){
                    defer.reject();
                }
            });
            return defer.promise();
        },
        getAppResults: function(_type, _search){
            var appresults = new Entities.AppResults({
                _type: _type,
                _search: _search
            });
            var defer = $.Deferred();
            appresults.fetch({
                success: function (data) {
                    defer.resolve(data);
                }, error: function(){
                    defer.reject();
                }
            });
            return defer.promise();
        }
    };
    //Request Response Callbacks
    DashManager.reqres.setHandler('page:entities', function(_type){
        return API.getPages(_type);
    });
    DashManager.reqres.setHandler('page:entity', function(_id){
        return API.getOnePage(_id);
    });
    DashManager.reqres.setHandler('article:entities', function(_category, _page){
        return API.getArticles(_category, _page);
    });
    DashManager.reqres.setHandler('block:entity', function(_id, _container){
        return API.getOneBlock(_id, _container);
    });
    DashManager.reqres.setHandler('articleBlock:entities', function(_id){
        return API.getArticleBlocks(_id);
    });
    DashManager.reqres.setHandler('articleBlock:entity', function(_id){
        return API.getOneArticleBlock(_id);
    });
    DashManager.reqres.setHandler('person:entities', function(_type, _page){
        return API.getPersons(_type, _page);
    });
    DashManager.reqres.setHandler('person:entity', function(_id){
        return API.getOnePerson(_id);
    });
    DashManager.reqres.setHandler('file:entities', function(_type, _page){
        return API.getFiles(_type, _page);
    });
    DashManager.reqres.setHandler('file:entity', function(_id){
        return API.getOneFile(_id);
    });
    DashManager.reqres.setHandler('siteDetails:entity', function(){
        return API.getSiteDetails();
    });
    DashManager.reqres.setHandler('linkPreview:entity', function(_url){
        return API.getLinkPreview(_url);
    });
    DashManager.reqres.setHandler('appResults:entity', function(_type, _search){
        return API.getAppResults(_type, _search);
    });
});
//Views of the application
DashManager.module('DashApp.EntityViews', function (EntityViews, DashManager, Backbone, Marionette, $, _) {
    //New page view
    EntityViews.NewPageView = Marionette.ItemView.extend({
        template: 'newPageTemplate',
        events: {
            'click .js-close': 'closeOverlay',
            'click input, textarea': 'showSelectImageModal',
            'click .content-choose span': 'selectType',
            'click .js-save-page': 'savePage'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        },
        showSelectImageModal: function(ev){
            var $target = $(ev.currentTarget);
            if($target.hasClass('js-select-image')){
                if($target.hasClass('selected')) return;
                //Select current input
                this.$('.js-select-image').removeClass('selected');
                $target.addClass('selected');
                //Show modal
                if(!$('.modal > div').length)
                    DashManager.vent.trigger('selectImageModal:show');
            } else {
                //Close modal
                this.$('.js-selected-image').removeClass('selected');
                DashManager.commands.execute('close:modal');
            }
        },
        selectType: function(ev){
            var $target = $(ev.currentTarget);
            if($target.hasClass('selected')){
                $target.removeClass('selected');
            } else {
                this.$('.content-choose span').removeClass('selected');
                $target.addClass('selected');
            }
            //Show custom url if not institute or newsroom
            if(this.$('.choose-institute').hasClass('selected') || this.$('.choose-newsroom').hasClass('selected')){
                this.$('.page-url').addClass('u-hide');
            } else {
                this.$('.page-url').removeClass('u-hide');
            }
            //If external show page-ref
            if(this.$('.choose-external').hasClass('selected')){
                this.$('.page-url, .page-desc, .page-images').addClass('u-hide');
                this.$('.page-ref').removeClass('u-hide');
            } else {
                this.$('.page-desc, .page-images').removeClass('u-hide');
                this.$('.page-ref').addClass('u-hide');
            }
            //If project show page-other
            if(this.$('.choose-project').hasClass('selected')){
                this.$('.page-other').removeClass('u-hide');
            } else {
                this.$('.page-other').addClass('u-hide');
            }
            //Close modal
            this.$('.js-select-image').removeClass('selected');
            DashManager.commands.execute('close:modal');
        },
        savePage: function(ev){
            ev.preventDefault();
            var value = {
                title: this.$('.page-title').val().trim(),
                url: this.$('.page-url').val().trim(),
                desc: this.$('.page-desc').val().trim(),
                image_m: this.$('.page-image-m').val(),
                image_l: this.$('.page-image-l').val(),
                favicon: this.$('.page-favicon').val(),
                apple: this.$('.page-apple').val(),
                ref_url: this.$('.page-ref-url').val(),
                program: this.$('.page-program').val()
            }
            //Category
            if(this.$('.choose-institute').hasClass('selected')){
                value.category = 'institute';
            } else if(this.$('.choose-newsroom').hasClass('selected')){
                value.category = 'newsroom';
            } else if(this.$('.choose-project').hasClass('selected')){
                value.category = 'project';
            } else if(this.$('.choose-event').hasClass('selected')){
                value.category = 'event';
            } else if(this.$('.choose-external').hasClass('selected')){
                value.category = 'external';
            } else {
                value.category = 'other';
            }
            //Order
            if($('.one-page').hasClass('selected')){
                value.order = $('.one-page.selected').data('order') + 1;
                value.level = $('.one-page.selected').data('level');
            } else {
                value.order = $(".one-page[data-level='1']").length;
                value.level = 1;
            }
            //Remove selected class
            $('.one-page.selected').removeClass('selected');
            //Create - Edit page
            if(this.$('.overlay-box').hasClass('edit-box')){
                this.trigger('update:page', value);
            } else {
                this.trigger('save:page', value);
            }
        }
    });
    //Page item view
    EntityViews.PageItemView = Marionette.ItemView.extend({
        template: 'pageItemTemplate',
        className: 'one-page',
        initialize: function(){
            this.$el.attr('data-order', this.model.get('order'));
            this.$el.attr('data-level', this.model.get('level'));
        },
        events: {
            'click': 'showPageBlocks',
            'click .add-below': 'showNewPageOverlay',
            'click .edit-page': 'showEditPageOverlay',
            'click .move-right': 'increaseLevel',
            'click .move-left': 'decreaseLevel',
            'click .move-up': 'moveUp',
            'click .move-down': 'moveDown',
            'click .publish-page': 'publishPage',
            'click .unpublish-page': 'unpublishPage'
        },
        showPageBlocks: function(ev){
            ev.preventDefault();
            DashManager.vent.trigger('pageBlocks:show', this.model.get('_id'));
        },
        showNewPageOverlay: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            var $target = $(ev.currentTarget);
            $target.parent().parent().addClass('selected');
            DashManager.vent.trigger('newPageOverlay:show');
        },
        showEditPageOverlay: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            DashManager.vent.trigger('editPageOverlay:show', this.model.get('_id'));
        },
        increaseLevel: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            this.trigger('increase:level', this.model);
        },
        decreaseLevel: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            this.trigger('decrease:level', this.model);
        },
        moveUp: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            this.trigger('move:up', this.model);
        },
        moveDown: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            this.trigger('move:down', this.model);
        },
        publishPage: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            var $target = $(ev.currentTarget);
            //Update class
            $target.removeClass('publish-page').addClass('unpublish-page');
            this.trigger('publish:page', this.model);
        },
        unpublishPage: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            var $target = $(ev.currentTarget);
            //Update class
            $target.removeClass('unpublish-page').addClass('publish-page');
            this.trigger('unpublish:page', this.model);
        }
    });
    //Pages view
    EntityViews.PagesView = Marionette.CompositeView.extend({
        template: 'pagesTemplate',
        childView: EntityViews.PageItemView,
        childViewContainer: 'div.all-items',
        events: {
            'click .js-add-page': 'showNewPageOverlay'
        },
        showNewPageOverlay: function(ev){
            ev.preventDefault();
            DashManager.vent.trigger('newPageOverlay:show');
        }
    });
    //All blocks list view
    EntityViews.AllBlocksView = Marionette.ItemView.extend({
        template: 'allBlocksTemplate',
        events: {
            'click .js-close': 'closeOverlay',
            'click .block-select': 'selectBlock'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        },
        selectBlock: function(ev){
            ev.preventDefault();
            var $target = $(ev.currentTarget);
            if($target.hasClass('js-header-block')){
                DashManager.vent.trigger('newBlockOverlay:show', 'header');
            } else if($target.hasClass('js-header-v-block')){
                DashManager.vent.trigger('newBlockOverlay:show', 'header-v');
            } else if($target.hasClass('js-container-block')){
                DashManager.vent.trigger('newBlockOverlay:show', 'container');
            } else if($target.hasClass('js-section-block')){
                DashManager.vent.trigger('newBlockOverlay:show', 'section');
            } else if($target.hasClass('js-body-t-block')){
                DashManager.vent.trigger('newBlockOverlay:show', 'body-t');
            } else if($target.hasClass('js-body-h-block')){
                DashManager.vent.trigger('newBlockOverlay:show', 'body-h');
            } else if($target.hasClass('js-body-c-block')){
                DashManager.vent.trigger('newBlockOverlay:show', 'body-c');
            } else if($target.hasClass('js-body-e-block')){
                DashManager.vent.trigger('newBlockOverlay:show', 'body-e');
            } else if($target.hasClass('js-feed-block')){
                DashManager.vent.trigger('newBlockOverlay:show', 'feed');
            } else if($target.hasClass('js-calendar-block')){
                DashManager.vent.trigger('newBlockOverlay:show', 'calendar');
            } else if($target.hasClass('js-people-block')){
                DashManager.vent.trigger('newBlockOverlay:show', 'people');
            } else if($target.hasClass('js-logos-block')){
                DashManager.vent.trigger('newBlockOverlay:show', 'logos');
            }
        }
    });
    //New block view
    EntityViews.NewBlockView = Marionette.ItemView.extend({
        template: 'newBlockTemplate',
        events: {
            'click .js-close': 'closeOverlay',
            'click input, textarea': 'showSelectImageModal',
            'click .choose-formula span': 'selectFormula',
            'click .choose-article-type span': 'selectType',
            'click .remove-person': 'removePerson',
            'click .js-add-event': 'addEvent',
            'click .remove-event': 'removeEvent',
            'click .js-add-image': 'addImage',
            'click .remove-image': 'removeImage',
            'click .js-save-block': 'saveBlock',
            'click .js-delete-block': 'deleteBlock'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        },
        showSelectImageModal: function(ev){
            var $target = $(ev.currentTarget);
            if($target.hasClass('js-select-image')){
                if($target.hasClass('selected')) return;
                //Select current input
                this.$('.js-select-image').removeClass('selected');
                $target.addClass('selected');
                //Show modal
                if(!$('.modal > div').length)
                    DashManager.vent.trigger('selectImageModal:show');
            } else {
                //Close modal
                this.$('.js-selected-image').removeClass('selected');
                DashManager.commands.execute('close:modal');
            }
        },
        selectFormula: function(ev){
            var $target = $(ev.currentTarget);
            if($target.hasClass('selected')){
                $target.removeClass('selected');
            } else {
                this.$('.choose-formula span').removeClass('selected');
                $target.addClass('selected');
            }
            //If empty
            if(!$target.hasClass('choose-empty')){
                this.$('.container-rows').removeClass('u-hide');
            } else {
                this.$('.container-rows').addClass('u-hide');
            }
            //If articles
            if($target.hasClass('choose-articles')){
                this.$('.choose-article-type').removeClass('u-hide');
            } else {
                this.$('.choose-article-type').addClass('u-hide');
            }
        },
        selectType: function(ev){
            var $target = $(ev.currentTarget);
            if($target.hasClass('selected')){
                $target.removeClass('selected');
            } else {
                this.$('.choose-article-type span').removeClass('selected');
                $target.addClass('selected');
            }
        },
        removePerson: function(ev){
            ev.preventDefault();
            var $target = $(ev.currentTarget);
            var value = {
                person_id: $target.parent().data('id')
            }
            $target.parent().remove();
            //Save
            if(this.$('.overlay-box').hasClass('edit-box')){
                this.trigger('removeCreated:person', value);
            } else {
                this.trigger('remove:person', value);
            }
        },
        addEvent: function(ev){
            ev.preventDefault();
            if(this.$('.event-title').val().trim()){
                //Date
                var startDate, endDate;
                //Get GMT Time
                if(this.$('.event-start').datepicker('getDate')){
                    var date = this.$('.event-start').datepicker('getDate');
                    var timeZoneOffset = new Date().getTimezoneOffset();
                    date = new Date(new Date(date).getTime() + timeZoneOffset * 60 * 1000);
                    startDate = date;
                }
                if(this.$('.event-end').datepicker('getDate')){
                    var date = this.$('.event-end').datepicker('getDate');
                    var timeZoneOffset = new Date().getTimezoneOffset();
                    date = new Date(new Date(date).getTime() + timeZoneOffset * 60 * 1000);
                    endDate = date;
                }
                //Event
                if(this.$('.overlay-box').hasClass('edit-box')){
                    var value = {
                        title: this.$('.event-title').val().trim(),
                        desc: this.$('.event-desc').val().trim(),
                        start_date: startDate,
                        end_date: endDate,
                        image_m: this.$('.event-image-m').val(),
                        image_l: this.$('.event-image-l').val(),
                        url: this.$('.event-url').val().trim(),
                        location: this.$('.event-location').val().trim()
                    }
                    this.trigger('add:event', value);
                } else {
                    var event = {
                        title: this.$('.event-title').val().trim(),
                        desc: this.$('.event-desc').val().trim(),
                        'date.start': startDate,
                        'date.end': endDate,
                        'image.m': this.$('.event-image-m').val(),
                        'image.l': this.$('.event-image-l').val(),
                        url: this.$('.event-url').val().trim(),
                        location: this.$('.event-location').val().trim()
                    }
                    this.$('.events-list').append("<div class='one-event'><p class='title'>"+event.title+"</p><span class='remove-event u-delete'>Remove</span></div>");
                    //Add to array
                    calEventsArray.push(event);
                }
                //Empty states
                this.$('.event-title').val('');
                this.$('.event-desc').val('');
                this.$('.event-start').val('');
                this.$('.event-end').val('');
                this.$('.event-image-m').val('');
                this.$('.event-image-l').val('');
                this.$('.event-url').val('');
                this.$('.event-location').val('');
            } else {
                return;
            }
        },
        removeEvent: function(ev){
            ev.preventDefault();
            var $target = $(ev.currentTarget);
            //Save
            if(this.$('.overlay-box').hasClass('edit-box')){
                var value = {
                    event_id: $target.parent().data('id')
                }
                this.trigger('remove:event', value);
            } else {
                var index = $target.parent().index();
                //Remove from array
                if(index > -1){
                    calEventsArray.splice(index, 1);
                }
            }
            //Remove element
            $target.parent().remove();
        },
        addImage: function(ev){
            ev.preventDefault();
            if(this.$('.body-c-image-l').val().trim()){
                //Image
                if(this.$('.overlay-box').hasClass('edit-box')){
                    var value = {
                        image_l: this.$('.body-c-image-l').val().trim(),
                        image_m: this.$('.body-c-image-m').val().trim(),
                        title: this.$('.body-c-title').val().trim(),
                        button_text: this.$('.body-c-button-text').val().trim(),
                        button_url: this.$('.body-c-button-url').val().trim()
                    }
                } else {
                    var image_item = {
                        'file.l': this.$('.body-c-image-l').val().trim(),
                        'file.m': this.$('.body-c-image-m').val().trim(),
                        title: this.$('.body-c-title').val().trim(),
                        'button.text': this.$('.body-c-button-text').val().trim(),
                        'button.url': this.$('.body-c-button-url').val().trim(),
                    };
                }
                var image_src = this.$('.body-c-image-l').val().trim();
                //Get bound
                var _this = this;
                var image = new Image();
                image.src = image_src;
                image.onload = function() {
                    var bound = ( image.naturalWidth * 400 ) / image.naturalHeight;
                    if(bound) {
                        bound = parseInt(bound);
                        if(_this.$('.overlay-box').hasClass('edit-box')){
                            value.bound = bound;
                        } else {
                            image_item.bound = bound;
                        }
                    }
                    window.URL.revokeObjectURL(image.src);
                    //Add image or add to array
                    if(_this.$('.overlay-box').hasClass('edit-box')){
                        _this.trigger('add:image', value);
                    } else {
                        carouselImagesArray.push(image_item);
                        _this.$('.body-images-list').append("<div class='one-image'><p class='title'>"+image_src+"</p><span class='remove-image u-delete'>Remove</span></div>");
                    }
                    //Empty states
                    _this.$('.body-c-image-l').val('');
                    _this.$('.body-c-image-m').val('');
                    _this.$('.body-c-title').val('');
                    _this.$('.body-c-button-text').val('');
                    _this.$('.body-c-button-url').val('');
                };
                image.onerror = function(){
                    if(_this.$('.overlay-box').hasClass('edit-box')){
                        value.bound = 600;
                    } else {
                        image_item.bound = 600;
                    }
                    //Add image or add to array
                    if(_this.$('.overlay-box').hasClass('edit-box')){
                        _this.trigger('add:image', value);
                    } else {
                        carouselImagesArray.push(image_item);
                        _this.$('.body-images-list').append("<div class='one-image'><p class='title'>"+image_src+"</p><span class='remove-image u-delete'>Remove</span></div>");
                    }
                    //Empty states
                    _this.$('.body-c-image-l').val('');
                    _this.$('.body-c-image-m').val('');
                    _this.$('.body-c-title').val('');
                    _this.$('.body-c-button-text').val('');
                    _this.$('.body-c-button-url').val('');
                };
            } else {
                return;
            }
        },
        removeImage: function(ev){
            ev.preventDefault();
            var $target = $(ev.currentTarget);
            //Save
            if(this.$('.overlay-box').hasClass('edit-box')){
                var value = {
                    image_id: $target.parent().data('id')
                }
                this.trigger('remove:image', value);
            } else {
                var index = $target.parent().index();
                //Remove from array
                if(index > -1){
                    carouselImagesArray.splice(index, 1);
                }
            }
            //Remove element
            $target.parent().remove();
        },
        saveBlock: function(ev){
            ev.preventDefault();
            if(!this.$('.block-header').hasClass('u-hide')){
                //Header block
                var value = {
                    type: 'header',
                    title: this.$('.header-title').val().trim(),
                    desc: this.$('.header-desc').val().trim(),
                    image_m: this.$('.header-image-m').val().trim(),
                    image_l: this.$('.header-image-l').val().trim(),
                    button_text: this.$('.header-button-text').val().trim(),
                    buttonb_text: this.$('.header-buttonb-text').val().trim(),
                    story_title: this.$('.header-story-title').val().trim(),
                    story_text: this.$('.header-story-text').val().trim(),
                    story_url: this.$('.header-story-url').val().trim()
                }
                var button_url = this.$('.header-button-url').val();
                var buttonb_url = this.$('.header-buttonb-url').val();
            } else if(!this.$('.block-header-v').hasClass('u-hide')){
                //Header video block
                var value = {
                    type: 'header_video',
                    title: this.$('.header-v-title').val().trim(),
                    desc: this.$('.header-v-desc').val().trim(),
                    embed: this.$('.header-v-embed').val(),
                    button_text: this.$('.header-v-button-text').val().trim(),
                    buttonb_text: this.$('.header-v-buttonb-text').val().trim()
                }
                var button_url = this.$('.header-v-button-url').val();
                var buttonb_url = this.$('.header-v-buttonb-url').val();
            } else if(!this.$('.block-container').hasClass('u-hide')){
                //Container
                var value = {
                    type: 'container',
                    row_count: this.$('.container-rows').val().trim()
                }
                //Select formula
                if($('.block-container .choose-tags').hasClass('selected')){
                    value.formula = 'tags';
                } else if($('.block-container .choose-projects').hasClass('selected')){
                    value.formula = 'projects';
                } else if($('.block-container .choose-events').hasClass('selected')){
                    value.formula = 'events';
                } else if($('.block-container .choose-articles').hasClass('selected')){
                    if($('.block-container .choose-news').hasClass('selected')){
                        value.formula = 'news';
                    } else if($('.block-container .choose-directors').hasClass('selected')){
                        value.formula = 'directors';
                    } else if($('.block-container .choose-resources').hasClass('selected')){
                        value.formula = 'resources';
                    } else {
                        value.formula = 'blog';
                    }
                } else {
                    value.formula = 'empty';
                }
            } else if(!this.$('.block-section').hasClass('u-hide')){
                //Section
                var value = {
                    type: 'section',
                    title: this.$('.section-title').val().trim(),
                    desc: this.$('.section-desc').val(),
                    button_text: this.$('.section-button-text').val().trim()
                }
                var button_url = this.$('.section-button-url').val();
            } else if(!this.$('.block-body-t').hasClass('u-hide')){
                //Body text block
                var value = {
                    type: 'body_text',
                    title: this.$('.body-t-title').val().trim(),
                    desc: this.$('.body-t-desc').val(),
                    image_m: this.$('.body-t-image-m').val().trim(),
                    icon: this.$('.body-t-icon').val().trim(),
                    button_text: this.$('.body-t-button-text').val().trim()
                }
                var button_url = this.$('.body-t-button-url').val();
            } else if(!this.$('.block-body-h').hasClass('u-hide')){
                var value = {
                    type: 'body_html'
                }
            } else if(!this.$('.block-body-e').hasClass('u-hide')){
                //Body embed block
                var value = {
                    type: 'body_embed',
                    title: this.$('.body-e-title').val().trim(),
                    desc: this.$('.body-e-desc').val(),
                    embed: this.$('.body-e-embed').val(),
                    button_text: this.$('.body-e-button-text').val().trim()
                }
                var button_url = this.$('.body-e-button-url').val();
            } else if(!this.$('.block-feed').hasClass('u-hide')){
                //Feed
                var value = {
                    type: 'feed',
                    title: this.$('.feed-title').val().trim(),
                    embed: this.$('.feed-embed').val()
                }
            } else if(!this.$('.block-people').hasClass('u-hide')){
                //People
                var value = {
                    type: 'people',
                    title: this.$('.people-title').val().trim(),
                    desc: this.$('.people-desc').val()
                }
            } else if(!this.$('.block-logos').hasClass('u-hide')){
                //Logos
                var value = {
                    type: 'logos',
                    title: this.$('.logos-title').val().trim(),
                    desc: this.$('.logos-desc').val()
                }
            } else if(!this.$('.block-calendar').hasClass('u-hide')){
                //Calendar
                var value = {
                    type: 'calendar',
                    title: this.$('.calendar-title').val().trim(),
                    events: calEventsArray
                }
            } else if(!this.$('.block-body-c').hasClass('u-hide')){
                //Carousel
                var value = {
                    type: 'body_carousel',
                    gallery: carouselImagesArray
                }
            }
            //Button URL is embed or URL
            if(button_url && validator.isURL(button_url)){
                value.button_url = button_url;
            } else if(button_url){
                value.button_embed = button_url;
            }
            if(buttonb_url && validator.isURL(buttonb_url)){
                value.buttonb_url = buttonb_url;
            } else if(buttonb_url){
                value.buttonb_embed = buttonb_url;
            }
            //Order
            if($('.one-block').hasClass('selected')){
                value.order = $('.one-block.selected').data('order') + 1;
            } else {
                var num_blocks = $('.all-blocks .one-block').length;
                value.order = num_blocks + 1;
            }
            //Page id
            value.page = $('.all-blocks').data('page');
            //Parent block
            if($('.one-block').hasClass('selected-container')){
                value.block = $('.one-block.selected-container').data('id');
            }
            //Remove selected class
            $('.one-block.selected').removeClass('selected');
            $('.one-block.selected-container').removeClass('selected');
            //Save
            if(this.$('.overlay-box').hasClass('edit-box')){
                if(!this.$('.block-body-h').hasClass('u-hide')){
                    var $target = $(ev.currentTarget);
                    if($target.hasClass('uploading')){
                        return;
                    } else {
                        this.trigger('update:htmlBlock', value);
                    }
                } else {
                    this.trigger('update:block', value);
                }
            } else {
                if(!this.$('.block-body-h').hasClass('u-hide')){
                    var $target = $(ev.currentTarget);
                    if($target.hasClass('uploading')){
                        return;
                    } else {
                        this.trigger('save:htmlBlock', value);
                    }
                } else {
                    this.trigger('save:block', value);
                }
            }
        },
        deleteBlock: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            if(confirm('Are you sure you want to delete this block?')){
                this.trigger('delete:block');
            }
        }
    });
    //Block theme view
    EntityViews.BlockThemeView = Marionette.ItemView.extend({
        template: 'blockThemeTemplate',
        events: {
            'click .js-close': 'closeOverlay',
            'click .js-save-theme': 'saveTheme',
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        },
        saveTheme: function(ev){
            ev.preventDefault();
            if(!this.$('.color-a').val().trim() && !this.$('.color-b').val().trim())
                return;
            var value = {
                color_a: this.$('.color-a').val().trim(),
                color_b: this.$('.color-b').val().trim()
            }
            this.trigger('update:block', value);
        }
    });
    //Sub block item view
    EntityViews.SubBlockItemView = Marionette.ItemView.extend({
        className: 'sub-block',
        template: 'subBlockItemTemplate',
        events: {
            'click .edit-block': 'showEditBlockOverlay',
            'click .edit-theme': 'showEditThemeOverlay',
            'click .move-up': 'moveUp',
            'click .move-down': 'moveDown'
        },
        showEditBlockOverlay: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            //Get container id
            var $target = $(ev.currentTarget);
            var container_id = $target.parents().eq(3).data('id');
            DashManager.vent.trigger('editBlockOverlay:show', this.model.get('_id'), container_id);
        },
        showEditThemeOverlay: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            //Get container id
            var $target = $(ev.currentTarget);
            var container_id = $target.parents().eq(3).data('id');
            DashManager.vent.trigger('editThemeOverlay:show', this.model.get('_id'), container_id);
        },
        moveUp: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            //Get container id
            var $target = $(ev.currentTarget);
            var container_id = $target.parents().eq(3).data('id');
            var value = {
                block_id: this.model.get('_id'),
                container_id: container_id
            }
            this.trigger('move:up', value);
        },
        moveDown: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            //Get container id
            var $target = $(ev.currentTarget);
            var container_id = $target.parents().eq(3).data('id');
            var value = {
                block_id: this.model.get('_id'),
                container_id: container_id
            }
            this.trigger('move:down', value);
        },
    });
    //Block item view
    EntityViews.BlockItemView = Marionette.CompositeView.extend({
        className: 'one-block',
        template: 'blockItemTemplate',
        childView: EntityViews.SubBlockItemView,
        childViewContainer: 'div.sub-blocks',
        initialize: function(){
            this.$el.attr('data-id', this.model.get('_id'));
            this.$el.attr('data-order', this.model.get('order'));
            //Get sub blocks
            if(this.model.get('type') == 'container' && this.model.get('formula') == 'empty'){
                var sub_blocks = this.model.get('blocks');
                this.collection = new Backbone.Collection(sub_blocks);
            }
        },
        events: {
            'click .add-below, .add-inside': 'showAllBlocksOverlay',
            'click .add-tags': 'showTagsOverlay',
            'click .move-up': 'moveUp',
            'click .move-down': 'moveDown',
            'click .edit-block': 'showEditBlockOverlay',
            'click .edit-theme': 'showEditThemeOverlay'
        },
        showAllBlocksOverlay: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            var $target = $(ev.currentTarget);
            if($target.hasClass('add-below')){
                $target.parent().parent().addClass('selected');
            } else {
                $target.parent().parent().addClass('selected-container');
            }
            DashManager.vent.trigger('allBlocksOverlay:show');
        },
        showTagsOverlay: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            DashManager.vent.trigger('blockTags:show', this.model.get('_id'));
        },
        moveUp: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            this.trigger('move:up', this.model);
        },
        moveDown: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            this.trigger('move:down', this.model);
        },
        showEditBlockOverlay: function(ev){
            DashManager.vent.trigger('editBlockOverlay:show', this.model.get('_id'));
        },
        showEditThemeOverlay: function(ev){
            DashManager.vent.trigger('editThemeOverlay:show', this.model.get('_id'));
        }
    });
    //Page blocks view
    EntityViews.PageBlocksView = Marionette.CompositeView.extend({
        template: 'pageBlocksTemplate',
        childView: EntityViews.BlockItemView,
        childViewContainer: 'div.all-blocks',
        initialize: function(){
            var blocks = this.model.get('blocks');
            this.collection = new Backbone.Collection(blocks);
        },
        events: {
            'click .js-add-block': 'showAllBlocksOverlay'
        },
        showAllBlocksOverlay: function(ev){
            ev.preventDefault();
            DashManager.vent.trigger('allBlocksOverlay:show');
        }
    });
    //New article view
    EntityViews.NewArticleView = Marionette.ItemView.extend({
        template: 'newArticleTemplate',
        events: {
            'click .js-close': 'closeOverlay',
            'click input, textarea': 'showSelectImageModal',
            'click .content-choose span': 'selectType',
            'click .js-save-article': 'saveArticle',
            'click .js-delete-article': 'deleteArticle'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        },
        showSelectImageModal: function(ev){
            var $target = $(ev.currentTarget);
            if($target.hasClass('js-select-image')){
                if($target.hasClass('selected')) return;
                //Select current input
                this.$('.js-select-image').removeClass('selected');
                $target.addClass('selected');
                //Show modal
                if(!$('.modal > div').length)
                    DashManager.vent.trigger('selectImageModal:show');
            } else {
                //Close modal
                this.$('.js-selected-image').removeClass('selected');
                DashManager.commands.execute('close:modal');
            }
        },
        selectType: function(ev){
            var $target = $(ev.currentTarget);
            if($target.hasClass('selected')){
                $target.removeClass('selected');
            } else {
                this.$('.content-choose span').removeClass('selected');
                $target.addClass('selected');
            }
            //Close modal
            this.$('.js-select-image').removeClass('selected');
            DashManager.commands.execute('close:modal');
        },
        saveArticle: function(ev){
            ev.preventDefault();
            var value = {
                title: this.$('.article-title').val().trim(),
                desc: this.$('.article-desc').val().trim(),
                image_m: this.$('.article-image').val(),
                icon: this.$('.article-icon').val()
            }
            //Category
            if(this.$('.choose-news').hasClass('selected')){
                value.category = 'news';
            } else if(this.$('.choose-directors').hasClass('selected')){
                value.category = 'directors';
            } else if(this.$('.choose-resources').hasClass('selected')){
                value.category = 'resources';
            } else {
                value.category = 'blog';
            }
            //URL or embed code
            if(this.$('.article-url').val().trim()){
                var url = this.$('.article-url').val().trim();
                if(validator.isURL(url)){
                    value.ref = url;
                    //Get extension from url and check if image
                    var index = url.lastIndexOf('.');
                    if(index){
                        var file_ext = url.substring(index+1, url.length);
                        var image_extensions = ['jpg', 'png', 'gif', 'jpeg'];
                        if(image_extensions.indexOf(file_ext) >= 0) {
                            value.image_l = url;
                        }
                    }
                } else {
                    value.embed = url;
                }
            } else {
                value.ref = '';
                value.embed = '';
            }
            //Create - Edit article
            if(this.$('.overlay-box').hasClass('edit-box')){
                this.trigger('update:article', value);
            } else {
                this.trigger('save:article', value);
            }
        },
        deleteArticle: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            if(confirm('Are you sure you want to delete this article?')){
                this.trigger('delete:article');
            }
        }
    });
    //Article content view
    EntityViews.ArticleContentView = Marionette.ItemView.extend({
        template: 'articleContentTemplate',
        events: {
            'click .js-close': 'closeOverlay',
            'click .js-update-article': 'updateArticleContent'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        },
        updateArticleContent: function(ev){
            ev.preventDefault();
            var $target = $(ev.currentTarget);
            if($target.hasClass('uploading')){
                return;
            } else {
                this.trigger('update:articleContent');
            }
        }
    });
    //Article item view
    EntityViews.ArticleItemView = Marionette.ItemView.extend({
        template: 'articleItemTemplate',
        className: 'one-item',
        events: {
            'click': 'showArticleBlocks',
            'click .js-edit-meta': 'showEditArticleOverlay',
            'click .js-add-person': 'showPersonsOverlay',
            'click .js-add-related': 'showRelatedPagesOverlay',
            'click .js-add-tags': 'showTagsOverlay',
            'click .js-edit-html': 'showContentOverlay'
        },
        showArticleBlocks: function(ev){
            ev.preventDefault();
            DashManager.vent.trigger('articleBlocks:show', this.model.get('_id'));
        },
        showEditArticleOverlay: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            DashManager.vent.trigger('editArticleOverlay:show', this.model.get('_id'));
        },
        showPersonsOverlay: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            DashManager.vent.trigger('articlePersons:show', this.model.get('_id'));
        },
        showRelatedPagesOverlay: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            DashManager.vent.trigger('relatedPages:show', this.model.get('_id'));
        },
        showTagsOverlay: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            DashManager.vent.trigger('articleTags:show', this.model.get('_id'));
        },
        showContentOverlay: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            DashManager.vent.trigger('editContentOverlay:show', this.model.get('_id'));
        }
    });
    //Articles view
    EntityViews.ArticlesView = Marionette.CompositeView.extend({
        template: 'articlesTemplate',
        childView: EntityViews.ArticleItemView,
        childViewContainer: 'div.all-items',
        events: {
            'click .js-add-article': 'showNewArticleOverlay',
            'click .filter-items span': 'filterArticles'
        },
        showNewArticleOverlay: function(ev){
            ev.preventDefault();
            DashManager.vent.trigger('newArticleOverlay:show');
        },
        filterArticles: function(ev){
            ev.preventDefault();
            var $target = $(ev.currentTarget);
            if($target.hasClass('selected')){
                //Show all people
                DashManager.vent.trigger('articles:show');
            } else {
                //Select
                if($target.hasClass('filter-news')){
                    DashManager.vent.trigger('articles:show', 'news');
                } else if($target.hasClass('filter-blog')){
                    DashManager.vent.trigger('articles:show', 'blog');
                } else if($target.hasClass('filter-directors')){
                    DashManager.vent.trigger('articles:show', 'directors');
                } else if($target.hasClass('filter-resources')){
                    DashManager.vent.trigger('articles:show', 'resources');
                }
            }
        }
    });
    //All article blocks list view
    EntityViews.AllArticleBlocksView = Marionette.ItemView.extend({
        template: 'allArticleBlocksTemplate',
        events: {
            'click .js-close': 'closeOverlay',
            'click .block-select': 'selectBlock'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        },
        selectBlock: function(ev){
            ev.preventDefault();
            var $target = $(ev.currentTarget);
            if($target.hasClass('js-article-text-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'text');
            } else if($target.hasClass('js-article-gallery-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'gallery');
            } else if($target.hasClass('js-article-audio-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'audio');
            } else if($target.hasClass('js-article-video-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'video');
            } else if($target.hasClass('js-article-file-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'file');
            } else if($target.hasClass('js-article-link-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'link');
            } else if($target.hasClass('js-article-gif-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'gif');
            } else if($target.hasClass('js-article-button-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'button');
            } else if($target.hasClass('js-article-embed-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'embed');
            } else if($target.hasClass('js-article-people-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'people');
            } else if($target.hasClass('js-article-logos-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'logos');
            } else if($target.hasClass('js-article-discussion-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'discussion');
            } else if($target.hasClass('js-article-mcq-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'mcq');
            } else if($target.hasClass('js-article-journal-block')){
                DashManager.vent.trigger('newArticleBlockOverlay:show', 'journal');
            }
        }
    });
    //New articleblock view
    EntityViews.NewArticleBlockView = Marionette.ItemView.extend({
        template: 'newArticleBlockTemplate',
        events: {
            'click .js-close': 'closeOverlay',
            'input .link-embed': 'showPreviewLink',
            'click .link-add': 'embedLink',
            'click .one-shot': 'selectImage',
            'click #drop': 'openFileBrowser',
            'click .file-input': 'doNothing',
            'click .select-journal span': 'selectJournalType',
            'click .block-button-colors span': 'selectButtonColor',
            'input .search-gifs': 'findGIFResults',
            'click .one-gif': 'saveGIFBlock',
            'click .remove-person': 'removePerson',
            'click .js-add-image': 'addImage',
            'click .remove-image': 'removeImage',
            'click .js-save-block': 'saveBlock',
            'click .js-delete-block': 'deleteBlock'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        },
        showPreviewLink: function(ev){
            var url = this.$('.link-embed').val().trim();
            if(validator.isURL(url)){
                this.$('.link-add').removeClass('u-hide');
            } else {
                this.$('.link-add').addClass('u-hide');
            }
        },
        embedLink: function(ev){
            var url = this.$('.link-embed').val().trim();
            //Disable input
            this.$('.link-embed').prop('disabled', true);
            this.$('.link-add').text('Loading...');
            //Get Preview
            var _this = this;
            var fetchingLinkpreview = DashManager.request('linkPreview:entity', url);
            $.when(fetchingLinkpreview).done(function(data){
                //Save link data
                linkEmbedData = data;
                //Hide preview button
                _this.$('.link-add').text('Preview link').addClass('u-hide');
                //Show Preview
                _this.$('.link-preview').show();
                _this.$('.preview-title').text(data.get('title'));
                _this.$('.preview-provider a').attr('href', data.get('url')).text(data.get('provider_name'));
                //Show shots
                if(data.get('images') && data.get('images').length){
                    for(var i=0; i<data.get('images').length; i++){
                        //Get image with http protocol
                        var imageUrl = data.get('images')[i].url.replace(/^https:\/\//i, 'http://');
                        //Show only images which are large
                        if(data.get('images')[i].width > 200 && data.get('images')[i].height > 100){
                            _this.$('.preview-shots').append("<div class='one-shot last'></div>");
                            _this.$('.preview-shots .one-shot.last').css('background-image', 'url("' + imageUrl + '")').removeClass('last');
                        }
                    }
                    //Show shots and desc
                    if(_this.$('.preview-shots > div').length){
                        _this.$('.preview-shots > div').eq(0).addClass('selected');
                        _this.$('.preview-shots').show();
                    } else {
                        _this.$('.preview-desc').text(data.get('description'));
                    }
                }
                //Show save button
                _this.$('.js-save').removeClass('u-disabled');
            });
        },
        selectImage: function(ev){
            var $target = $(ev.currentTarget);
            this.$('.preview-shots .one-shot').removeClass('selected');
            $target.addClass('selected');
        },
        openFileBrowser: function(ev){
            this.$('.block-file .file-input').click();
        },
        doNothing: function(ev){
            ev.stopPropagation();
        },
        selectJournalType: function(ev){
            ev.preventDefault();
            var $target = $(ev.currentTarget);
            this.$('.select-journal span').removeClass('selected');
            $target.addClass('selected');
        },
        selectButtonColor: function(ev){
            ev.preventDefault();
            var $target = $(ev.currentTarget);
            this.$('.block-button-colors span').removeClass('selected');
            $target.addClass('selected');
        },
        findGIFResults: function(ev){
            var text = this.$('.search-gifs').val().trim();
            var _this = this;
            if(!text){
                if(findTimer){
                    clearTimeout(findTimer);
                    findTimer = null;
                }
                //Hide loading bar and empty results
                this.$('.block-gif .throbber-loader').addClass('u-hide');
                this.$('.search-gifs').html('');
            } else {
                clearTimeout(findTimer);
                findTimer = null;
                findTimer = setTimeout(function(){
                    //Show loading bar
                    _this.$('.block-gif .throbber-loader').removeClass('u-hide');
                    //Find gifs
                    var fetchingGIFResults = DashManager.request('appResults:entity', 'gifs', text);
                    $.when(fetchingGIFResults).done(function(results){
                        var attrs = results.attributes.data;
                        //Save results data
                        resultsData = attrs;
                        _this.$('.block-gif .throbber-loader').addClass('u-hide');
                        _this.$('.gif-results').html('').show();
                        for(var i=0; i<10; i++){
                            var data = attrs[i];
                            if(!data) break;
                            var image = data.images.fixed_height.url;
                            var width = parseInt(data.images.fixed_height.width) + 4;
                            width = width + 'px';
                            _this.$('.gif-results').append("<div class='one-gif' style='width:"+width+"'><img src='"+image+"'></div>");
                        }
                    });
                }, 500);
            }
        },
        saveGIFBlock: function(ev){
            var $target = $(ev.currentTarget);
            var index = $target.index();
            var data = resultsData[index];
            var value = {
                type: 'gif',
                gif_url: data.url,
                gif_embed: data.images.original.mp4,
                width: data.images.original.width,
                height: data.images.original.height
            }
            //Block id
            value.block = $('.all-blocks').data('block');
            //Order
            if($('.one-block').hasClass('selected')){
                value.order = $('.one-block.selected').data('order') + 1;
            } else {
                var num_blocks = $('.all-blocks .one-block').length;
                value.order = num_blocks + 1;
            }
            //Save
            this.trigger('save:articleBlock', value);
        },
        removePerson: function(ev){
            ev.preventDefault();
            var $target = $(ev.currentTarget);
            var value = {
                person_id: $target.parent().data('id')
            }
            $target.parent().remove();
            //Save
            if(this.$('.overlay-box').hasClass('edit-box')){
                this.trigger('removeCreated:person', value);
            } else {
                this.trigger('remove:person', value);
            }
        },
        addImage: function(ev){
            ev.preventDefault();
            if(this.$('.body-c-image-l').val().trim()){
                //Image
                if(this.$('.overlay-box').hasClass('edit-box')){
                    var value = {
                        image_l: this.$('.body-c-image-l').val().trim(),
                        image_m: this.$('.body-c-image-m').val().trim(),
                        title: this.$('.body-c-title').val().trim(),
                        button_text: this.$('.body-c-button-text').val().trim(),
                        button_url: this.$('.body-c-button-url').val().trim()
                    }
                } else {
                    var image_item = {
                        'file.l': this.$('.body-c-image-l').val().trim(),
                        'file.m': this.$('.body-c-image-m').val().trim(),
                        title: this.$('.body-c-title').val().trim(),
                        'button.text': this.$('.body-c-button-text').val().trim(),
                        'button.url': this.$('.body-c-button-url').val().trim(),
                    };
                }
                var image_src = this.$('.body-c-image-l').val().trim();
                //Get bound
                var _this = this;
                var image = new Image();
                image.src = image_src;
                image.onload = function() {
                    var bound = ( image.naturalWidth * 400 ) / image.naturalHeight;
                    if(bound) {
                        bound = parseInt(bound);
                        if(_this.$('.overlay-box').hasClass('edit-box')){
                            value.bound = bound;
                        } else {
                            image_item.bound = bound;
                        }
                    }
                    window.URL.revokeObjectURL(image.src);
                    //Add image or add to array
                    if(_this.$('.overlay-box').hasClass('edit-box')){
                        _this.trigger('add:image', value);
                    } else {
                        carouselImagesArray.push(image_item);
                        _this.$('.body-images-list').append("<div class='one-image'><p class='title'>"+image_src+"</p><span class='remove-image u-delete'>Remove</span></div>");
                    }
                    //Empty states
                    _this.$('.body-c-image-l').val('');
                    _this.$('.body-c-image-m').val('');
                    _this.$('.body-c-title').val('');
                    _this.$('.body-c-button-text').val('');
                    _this.$('.body-c-button-url').val('');
                };
                image.onerror = function(){
                    if(_this.$('.overlay-box').hasClass('edit-box')){
                        value.bound = 600;
                    } else {
                        image_item.bound = 600;
                    }
                    //Add image or add to array
                    if(_this.$('.overlay-box').hasClass('edit-box')){
                        _this.trigger('add:image', value);
                    } else {
                        carouselImagesArray.push(image_item);
                        _this.$('.body-images-list').append("<div class='one-image'><p class='title'>"+image_src+"</p><span class='remove-image u-delete'>Remove</span></div>");
                    }
                    //Empty states
                    _this.$('.body-c-image-l').val('');
                    _this.$('.body-c-image-m').val('');
                    _this.$('.body-c-title').val('');
                    _this.$('.body-c-button-text').val('');
                    _this.$('.body-c-button-url').val('');
                };
            } else {
                return;
            }
        },
        removeImage: function(ev){
            ev.preventDefault();
            var $target = $(ev.currentTarget);
            //Save
            if(this.$('.overlay-box').hasClass('edit-box')){
                var value = {
                    image_id: $target.parent().data('id')
                }
                this.trigger('remove:image', value);
            } else {
                var index = $target.parent().index();
                //Remove from array
                if(index > -1){
                    carouselImagesArray.splice(index, 1);
                }
            }
            //Remove element
            $target.parent().remove();
        },
        saveBlock: function(ev){
            ev.preventDefault();
            if(!this.$('.block-text').hasClass('u-hide')){
                //Text
                var value = {
                    type: 'text'
                }
            } else if(!this.$('.block-gallery').hasClass('u-hide')){
                //Gallery
                var value = {
                    type: 'gallery',
                    gallery: carouselImagesArray
                }
            } else if(!this.$('.block-link').hasClass('u-hide')){
                //Link
                if(!linkEmbedData) return;
                //Selected image
                if(this.$('.one-shot.selected').length){
                    var image = this.$('.one-shot.selected').css('background-image').replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
                }
                var value = {
                    type: 'link',
                    linkdata: linkEmbedData,
                    image: image
                }
                linkEmbedData = '';
            } else if(!this.$('.block-button').hasClass('u-hide')){
                //Button
                //Selected color
                if(this.$('.block-button-colors span.selected').length){
                    var color = this.$('.block-button-colors span.selected').css('backgroundColor')
                }
                var value = {
                    type: 'button',
                    button_text: this.$('.block-button-title').val().trim(),
                    button_url: this.$('.block-button-url').val().trim(),
                    back_color: color
                }
            } else if(!this.$('.block-embed').hasClass('u-hide')){
                //Embed
                var value = {
                    type: 'embed',
                    title: this.$('.block-embed-title').val().trim(),
                    embed: this.$('.block-embed-code').val()
                }
            } else if(!this.$('.block-people').hasClass('u-hide')){
                //People
                var value = {
                    type: 'people',
                    title: this.$('.people-title').val().trim(),
                    desc: this.$('.people-desc').val()
                }
            } else if(!this.$('.block-logos').hasClass('u-hide')){
                //Logos
                var value = {
                    type: 'logos',
                    title: this.$('.logos-title').val().trim(),
                    desc: this.$('.logos-desc').val()
                }
            } else if(!this.$('.block-mcq').hasClass('u-hide')){
                //MCQ
                var value = {
                    type: 'mcq',
                    title: this.$('.block-mcq-title').val().trim()
                }
                //Check if is_multiple
                if(this.$('.is-multiple-label input').is(':checked')){
                    value.is_multiple = true;
                } else {
                    value.is_multiple = false;
                }
            } else if(!this.$('.block-journal').hasClass('u-hide')){
                //Journal
                var value = {
                    type: 'journal',
                    title: this.$('.block-journaling-title').val().trim(),
                    text: this.$('.block-journaling-text').val().trim()
                }
                //Select journal type
                if(this.$('.select-journal .journal-text').hasClass('selected')){
                    value.journal_type = 'text';
                } else if(this.$('.select-journal .journal-file').hasClass('selected')){
                    value.journal_type = 'file';
                } else if(this.$('.select-journal .journal-audio').hasClass('selected')){
                    value.journal_type = 'audio';
                } else if(this.$('.select-journal .journal-video').hasClass('selected')){
                    value.journal_type = 'video';
                } else if(this.$('.select-journal .journal-canvas').hasClass('selected')){
                    value.journal_type = 'canvas';
                }
            }
            //Order
            if($('.one-block').hasClass('selected')){
                value.order = $('.one-block.selected').data('order') + 1;
            } else {
                var num_blocks = $('.all-blocks .one-block').length;
                value.order = num_blocks + 1;
            }
            //Block id
            value.block = $('.all-blocks').data('block');
            //Remove selected class
            $('.one-block.selected').removeClass('selected');
            //Save
            if(this.$('.overlay-box').hasClass('edit-box')){
                if(!this.$('.block-text').hasClass('u-hide')){
                    var $target = $(ev.currentTarget);
                    if($target.hasClass('uploading')){
                        return;
                    } else {
                        this.trigger('update:articleHtmlBlock', value);
                    }
                } else {
                    this.trigger('update:articleBlock', value);
                }
            } else {
                if(!this.$('.block-text').hasClass('u-hide')){
                    var $target = $(ev.currentTarget);
                    if($target.hasClass('uploading')){
                        return;
                    } else {
                        this.trigger('save:articleHtmlBlock', value);
                    }
                } else {
                    this.trigger('save:articleBlock', value);
                }
            }
        },
        deleteBlock: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            if(confirm('Are you sure you want to delete this block?')){
                this.trigger('delete:articleBlock');
            }
        }
    });
    //Article Block item view
    EntityViews.ArticleBlockItemView = Marionette.ItemView.extend({
        className: 'one-block',
        template: 'articleBlockItemTemplate',
        initialize: function(){
            this.$el.attr('data-id', this.model.get('_id'));
            this.$el.attr('data-order', this.model.get('order'));
        },
        events: {
            'click .add-below, .add-inside': 'showAllArticleBlocksOverlay',
            'click .move-up': 'moveUp',
            'click .move-down': 'moveDown',
            'click .edit-block': 'showEditArticleBlockOverlay',
            'click .edit-options': 'showEditOptionsOverlay'
        },
        showAllArticleBlocksOverlay: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            var $target = $(ev.currentTarget);
            if($target.hasClass('add-below')){
                $target.parent().parent().addClass('selected');
            } else {
                $target.parent().parent().addClass('selected-container');
            }
            DashManager.vent.trigger('allArticleBlocksOverlay:show');
        },
        moveUp: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            this.trigger('move:up', this.model);
        },
        moveDown: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            this.trigger('move:down', this.model);
        },
        showEditArticleBlockOverlay: function(ev){
            DashManager.vent.trigger('editArticleBlockOverlay:show', this.model.get('_id'));
        },
        showEditOptionsOverlay: function(ev){
            DashManager.vent.trigger('mcqOptions:show', this.model.get('_id'), true);
        }
    });
    //Article blocks view
    EntityViews.ArticleBlocksView = Marionette.CompositeView.extend({
        template: 'articleBlocksTemplate',
        childView: EntityViews.ArticleBlockItemView,
        childViewContainer: 'div.all-blocks',
        events: {
            'click .js-add-block': 'showAllArticleBlocksOverlay'
        },
        showAllArticleBlocksOverlay: function(ev){
            ev.preventDefault();
            DashManager.vent.trigger('allArticleBlocksOverlay:show');
        }
    });
    //Option item view
    EntityViews.McqOptionItemView = Marionette.ItemView.extend({
        tagName: 'div',
        className: 'one-mcq-option',
        template: 'mcqOptionOneTemplate',
        events: {
            'click .remove-option': 'removeMCQOption'
        },
        removeMCQOption: function(ev){
            ev.preventDefault();
            var value = {
                option_id: this.model.get('_id')
            }
            this.trigger('remove:option', value);
        }
    });
    //MCQ options view
    EntityViews.McqOptionsView = Marionette.CompositeView.extend({
        template: 'mcqOptionsTemplate',
        childView: EntityViews.McqOptionItemView,
        childViewContainer: 'div.mcq-option-list',
        events: {
            'click .js-close, .js-done': 'closeOverlay',
            'click #option-image': 'openFileBrowser',
            'click .file-input': 'doNothing',
            'click .mcq-add': 'addMCQOption'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        },
        openFileBrowser: function(ev){
            this.$('#option-image .file-input').click();
        },
        doNothing: function(ev){
            ev.stopPropagation();
        },
        addMCQOption: function(ev){
            if(!this.$('.option-text').val().trim()) return;
            var value = {
                text: this.$('.option-text').val().trim()
            }
            this.trigger('add:option', value);
        }
    });
    //File item view
    EntityViews.FileItemView = Marionette.ItemView.extend({
        template: 'fileItemTemplate',
        className: 'one-item',
        events: {
            'click': 'showEditFileOverlay',
            'click .input-share-url, .input-image-m-url, .input-image-l-url': 'selectLink'
        },
        showEditFileOverlay: function(ev){
            DashManager.vent.trigger('editFileOverlay:show', this.model.get('_id'));
        },
        selectLink: function(ev){
            ev.stopPropagation();
            var $target = $(ev.currentTarget);
            $target.select();
        }
    });
    //Files view
    EntityViews.FilesView = Marionette.CompositeView.extend({
        template: 'filesTemplate',
        childView: EntityViews.FileItemView,
        childViewContainer: 'div.all-items',
        events: {
            'click #drop': 'openFileBrowser',
            'click .file-input': 'doNothing'
        },
        openFileBrowser: function(ev){
            this.$('.upload-file .file-input').click();
        },
        doNothing: function(ev){
            ev.stopPropagation();
        }
    });
    //Edit file view
    EntityViews.EditFileView = Marionette.ItemView.extend({
        template: 'editFileTemplate',
        events: {
            'click .js-close': 'closeOverlay',
            'click .js-save-file': 'saveFile',
            'click .js-delete-file': 'deleteFile'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        },
        saveFile: function(ev){
            ev.preventDefault();
            var value = {
                title: this.$('.file-title').val().trim()
            }
            this.trigger('update:file', value);
        },
        deleteFile: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            if(confirm('Are you sure you want to delete this file?')){
                this.trigger('delete:file');
            }
        }
    });
    //New person view
    EntityViews.NewPersonView = Marionette.ItemView.extend({
        template: 'newPersonTemplate',
        events: {
            'click .js-close': 'closeOverlay',
            'click .content-choose span': 'selectType',
            'click .upload-btn': 'uploadDp',
            'click .js-save-person': 'savePerson',
            'click .js-delete-person': 'deletePerson'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        },
        uploadDp: function(ev){
            ev.preventDefault();
            this.$('.file-input').click();
        },
        selectType: function(ev){
            var $target = $(ev.currentTarget);
            if($target.hasClass('selected')){
                $target.removeClass('selected');
            } else {
                this.$('.content-choose span').removeClass('selected');
                $target.addClass('selected');
            }
        },
        savePerson: function(ev){
            ev.preventDefault();
            var value = {
                name: this.$('.person-name').val().trim(),
                about: this.$('.person-about').val().trim(),
                desc: this.$('.person-desc').val(),
                email: this.$('.person-email').val(),
                url: this.$('.person-url').val()
            }
            //Dp
            if(this.$('.person-image').data('image')){
                value.image = this.$('.person-image').data('image');
            }
            //Type
            if(this.$('.choose-team').hasClass('selected')){
                value.type = 'team';
            } else if(this.$('.choose-partner').hasClass('selected')){
                value.type = 'partner';
            } else {
                value.type = 'author';
            }
            //Create - Edit person
            if(this.$('.overlay-box').hasClass('edit-box')){
                this.trigger('update:person', value);
            } else {
                this.trigger('save:person', value);
            }
        },
        deletePerson: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            if(confirm('Are you sure you want to delete this person?')){
                this.trigger('delete:person');
            }
        }
    });
    //Person item view
    EntityViews.PersonItemView = Marionette.ItemView.extend({
        template: 'personItemTemplate',
        className: 'one-item',
        events: {
            'click': 'showEditPersonOverlay'
        },
        showEditPersonOverlay: function(ev){
            DashManager.vent.trigger('editPersonOverlay:show', this.model.get('_id'));
        }
    });
    //Persons view
    EntityViews.PersonsView = Marionette.CompositeView.extend({
        template: 'personsTemplate',
        childView: EntityViews.PersonItemView,
        childViewContainer: 'div.all-items',
        events: {
            'click .js-add-person': 'showNewPersonOverlay',
            'click .filter-items span': 'filterPeople'
        },
        showNewPersonOverlay: function(ev){
            ev.preventDefault();
            DashManager.vent.trigger('newPersonOverlay:show');
        },
        filterPeople: function(ev){
            ev.preventDefault();
            var $target = $(ev.currentTarget);
            if($target.hasClass('selected')){
                //Show all people
                DashManager.vent.trigger('people:show');
            } else {
                //Select
                if($target.hasClass('filter-team')){
                    DashManager.vent.trigger('people:show', 'team');
                } else if($target.hasClass('filter-partner')){
                    DashManager.vent.trigger('people:show', 'partner');
                } else if($target.hasClass('filter-author')){
                    DashManager.vent.trigger('people:show', 'author');
                }
            }
        }
    });
    //Person list item view
    EntityViews.PersonListItemView = Marionette.ItemView.extend({
        tagName: 'div',
        className: 'one-person',
        template: 'personOneTemplate',
        events: {
            'click .remove-person': 'removePerson'
        },
        removePerson: function(){
            var value = {
                person_id: this.model.get('_id')
            }
            this.trigger('remove:person', value);
        }
    });
    //Persons list view
    EntityViews.PersonsListView = Marionette.CompositeView.extend({
        template: 'personsListTemplate',
        childView: EntityViews.PersonListItemView,
        childViewContainer: 'div.persons-list',
        events: {
            'click .js-close, .js-done': 'closeOverlay'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        }
    });
    //Related page item view
    EntityViews.RelatedPageItemView = Marionette.ItemView.extend({
        tagName: 'div',
        className: 'one-related',
        template: 'relatedPageOneTemplate',
        events: {
            'click .remove-related': 'removeRelated'
        },
        removeRelated: function(){
            var value = {
                page_id: this.model.get('_id')
            }
            this.trigger('remove:related', value);
        }
    });
    //Related pages view
    EntityViews.RelatedPagesView = Marionette.CompositeView.extend({
        template: 'relatedPagesTemplate',
        childView: EntityViews.RelatedPageItemView,
        childViewContainer: 'div.related-list',
        events: {
            'click .js-close, .js-done': 'closeOverlay'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        }
    });
    //Article tag item view
    EntityViews.ArticleTagItemView = Marionette.ItemView.extend({
        tagName: 'div',
        className: 'one-tag',
        template: 'articleTagOneTemplate',
        events: {
            'click .remove-tag': 'removeRelated'
        },
        removeRelated: function(){
            var value = {
                tag_id: this.model.get('_id'),
                tag_name: this.model.get('name')
            }
            this.trigger('remove:tag', value);
        }
    });
    //Block tags view
    EntityViews.BlockTagsView = Marionette.CompositeView.extend({
        template: 'blockTagsTemplate',
        childView: EntityViews.ArticleTagItemView,
        childViewContainer: 'div.tag-list',
        events: {
            'click .js-close, .js-done': 'closeOverlay'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        }
    });
    //Article tags view
    EntityViews.ArticleTagsView = Marionette.CompositeView.extend({
        template: 'articleTagsTemplate',
        childView: EntityViews.ArticleTagItemView,
        childViewContainer: 'div.tag-list',
        events: {
            'click .js-close, .js-done': 'closeOverlay'
        },
        closeOverlay: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:overlay');
        }
    });
    //Site settings view
    EntityViews.SettingsView = Marionette.ItemView.extend({
        tagName: 'div',
        className: 'contentForm settings-panel',
        template: 'settingsTemplate',
        events: {
            'click .js-update-settings': 'updateSiteSettings'
        },
        updateSiteSettings: function(ev){
            ev.preventDefault();
            if(this.$('.site-title').val().trim()){
                var value = {
                    title: this.$('.site-title').val().trim(),
                    desc: this.$('.site-desc').val().trim(),
                    contact: this.$('.site-contact').val(),
                    facebook: this.$('.site-facebook').val().trim(),
                    twitter: this.$('.site-twitter').val().trim(),
                    instagram: this.$('.site-instagram').val().trim(),
                    youtube: this.$('.site-youtube').val().trim(),
                    image_m: this.$('.site-image-m').val().trim(),
                    image_l: this.$('.site-image-l').val().trim(),
                    favicon: this.$('.site-favicon').val().trim(),
                    apple: this.$('.site-apple').val().trim(),
                    theme: this.$('.site-theme').val().trim(),
                    subscribe_title: this.$('.site-subscribe-title').val().trim(),
                    subscribe_desc: this.$('.site-subscribe-desc').val().trim(),
                    notice_desc: this.$('.site-notice-desc').val().trim(),
                    notice_link: this.$('.site-notice-link').val().trim()
                }
                //Ticker
                if(this.$('.site-ticker').val().trim()){
                    var ticker_data = this.$('.site-ticker').val().trim();
                    var all_tickers = ticker_data.split(';');
                    var tickers = [];
                    for(var i=0; i<all_tickers.length; i++){
                        var ticker_title = all_tickers[i].split(',')[0].trim();
                        var ticker_url = all_tickers[i].split(',')[1].trim();
                        tickers.push({title: ticker_title, url: ticker_url});
                        value.ticker = tickers;
                    }
                }
                this.trigger('update:site', value);
            } else {
                this.$('.site-title').focus();
            }
        }
    });
    //File item view
    EntityViews.SelectImageItemView = Marionette.ItemView.extend({
        template: 'selectImageItemTemplate',
        className: 'one-item one-select-item',
        events: {
            'click': 'getImageURL'
        },
        getImageURL: function(ev){
            ev.preventDefault();
            var image_url = this.model.get('image').l;
            $('.js-select-image.selected').val(image_url);
        }
    });
    //Select image view
    EntityViews.SelectImageView = Marionette.CompositeView.extend({
        template: 'selectImageTemplate',
        childView: EntityViews.SelectImageItemView,
        childViewContainer: 'div.all-items',
        events: {
            'click .js-close': 'closeModal',
            'click #drop': 'openFileBrowser',
            'click .file-input': 'doNothing'
        },
        closeModal: function(ev){
            ev.preventDefault();
            DashManager.commands.execute('close:modal');
        },
        openFileBrowser: function(ev){
            this.$('.upload-file .file-input').click();
        },
        doNothing: function(ev){
            ev.stopPropagation();
        }
    });
});
//Common Views of the application - Loading
DashManager.module('Common.Views', function(Views, DashManager, Backbone, Marionette, $, _){
    //Loading page
    Views.Loading = Marionette.ItemView.extend({
        tagName: 'div',
        className: 'loading-area',
        template: 'loadingTemplate'
    });
});
//Controllers of the Application
DashManager.module('DashApp.EntityController', function (EntityController, DashManager, Backbone, Marionette, $, _) {
    EntityController.Controller = {
        showNewPageOverlay: function(){
            $('.overlay').show();
            //New page view
            var newPageView = new DashManager.DashApp.EntityViews.NewPageView();
            //Show
            newPageView.on('show', function(){
                //Animate overlay box
                setTimeout(function(){
                    newPageView.$('.overlay-box').addClass('animate');
                }, 100);
                //Hide scroll on main page
                DashManager.commands.execute('show:overlay');
                //Focus
                newPageView.$('.page-title').focus();
            });
            //Save
            newPageView.on('save:page', function(value){
                var new_page = new DashManager.Entities.Page({
                    title: value.title,
                    url: value.url,
                    desc: value.desc,
                    image_m: value.image_m,
                    image_l: value.image_l,
                    favicon: value.favicon,
                    apple: value.apple,
                    order: value.order,
                    level: value.level,
                    ref_url: value.ref_url,
                    program: value.program,
                    category: value.category
                });
                new_page.save({}, {success: function(){
                    DashManager.vent.trigger('add:page', new_page);
                    DashManager.commands.execute('close:overlay');
                }});
            });
            DashManager.overlayRegion.show(newPageView);
        },
        showEditPageOverlay: function(page_id){
            $('.overlay').show();
            //Fetch page
            var fetchingPage = DashManager.request('page:entity', page_id);
            $.when(fetchingPage).done(function(page){
                var newPageView = new DashManager.DashApp.EntityViews.NewPageView();
                //Show
                newPageView.on('show', function(){
                    //Add edit class
                    newPageView.$('.overlay-box').addClass('edit-box');
                    //Animate overlay box
                    setTimeout(function(){
                        newPageView.$('.overlay-box').addClass('animate');
                    }, 100);
                    //Hide scroll on main page
                    DashManager.commands.execute('show:overlay');
                    //Fill values
                    newPageView.$('.page-title').val(page.get('title')).focus();
                    newPageView.$('.page-url').val(page.get('url'));
                    newPageView.$('.page-desc').val(page.get('desc'));
                    if(page.get('image')){
                        newPageView.$('.page-image-m').val(page.get('image').m);
                        newPageView.$('.page-image-l').val(page.get('image').l);
                        newPageView.$('.page-favicon').val(page.get('image').favicon);
                        newPageView.$('.page-apple').val(page.get('image').apple);
                    }
                    newPageView.$('.page-ref-url').val(page.get('ref_url'));
                    newPageView.$('.page-program').val(page.get('program'));
                    //Show category
                    var category = page.get('category');
                    if(category == 'institute'){
                        newPageView.$('.choose-institute').addClass('selected');
                    } else if(category == 'newsroom'){
                        newPageView.$('.choose-newsroom').addClass('selected');
                    } else if(category == 'project'){
                        newPageView.$('.choose-project').addClass('selected');
                        newPageView.$('.page-url').removeClass('u-hide');
                        newPageView.$('.page-other').removeClass('u-hide');
                    } else if(category == 'event'){
                        newPageView.$('.choose-event').addClass('selected');
                        newPageView.$('.page-url').removeClass('u-hide');
                    } else if(category == 'external'){
                        newPageView.$('.choose-external').addClass('selected');
                        newPageView.$('.page-ref').removeClass('u-hide');
                        newPageView.$('.page-url, .page-desc, .page-images').addClass('u-hide');
                    } else {
                        newPageView.$('.page-url').removeClass('u-hide');
                    }
                });
                //Update page
                newPageView.on('update:page', function(value){
                    var edit_page = new DashManager.Entities.Page({
                        _id: page_id,
                        _action: 'edit'
                    });
                    edit_page.set({
                        title: value.title,
                        url: value.url,
                        desc: value.desc,
                        image_m: value.image_m,
                        image_l: value.image_l,
                        favicon: value.favicon,
                        apple: value.apple,
                        order: value.order,
                        level: value.level,
                        ref_url: value.ref_url,
                        program: value.program,
                        category: value.category
                    });
                    edit_page.save({}, {success: function(){
                        DashManager.commands.execute('close:overlay');
                    }});
                });
                DashManager.overlayRegion.show(newPageView);
            });
        },
        showPages: function(){
            //Show loading page
            var loadingView = new DashManager.Common.Views.Loading();
            DashManager.mainRegion.show(loadingView);
            //Select
            $('.nav-links a').removeClass('selected');
            $('.nav-links .js-pages').addClass('selected');
            //Fetching pages
            var fetchingPages = DashManager.request('page:entities', 'all');
            $.when(fetchingPages).done(function(pages){
                var pagesView = new DashManager.DashApp.EntityViews.PagesView({
                    collection: pages
                });
                //Show
                pagesView.on('show', function(){
                    //Change margin based on level
                    var pagesLength = $('.one-page').length;
                    for(var i=0; i< pagesLength; i++){
                        var level = $('.one-page').eq(i).data('level');
                        if(level > 1){
                            var margin = (level - 1) * 15 + 'px';
                            $('.one-page').eq(i).css('marginLeft', margin);
                        }
                    }
                });
                //Add page
                DashManager.vent.off('add:page');
                DashManager.vent.on('add:page', function(page){
                    var order = page.get('order');
                    var level = page.get('level')
                    pages.add(page, {at: order});
                    //Change margin based on level
                    var pagesLength = $('.one-page').length;
                    for(var i=0; i< pagesLength - 1; i++){
                        var level = $('.one-page').eq(i).data('level');
                        if(level > 1){
                            var margin = (level - 1) * 15 + 'px';
                            $('.one-page').eq(i).css('marginLeft', margin);
                        }
                    }
                });
                //Increase level
                pagesView.on('childview:increase:level', function(childView, model){
                    var page = new DashManager.Entities.Page({
                        _id: model.get('_id'),
                        _action: 'inc_level'
                    });
                    page.set({});
                    page.save({}, {
                        dataType: 'text',
                        success: function(){
                            var level = model.get('level') + 1;
                            //Update model
                            model.set('level', level);
                            //Update margin
                            var margin = (level - 1) * 15 + 'px';
                            childView.$el.css('marginLeft', margin);
                            //Update data id
                            childView.$el.attr('data-level', level);
                        }
                    });
                });
                //Decrease level
                pagesView.on('childview:decrease:level', function(childView, model){
                    var page = new DashManager.Entities.Page({
                        _id: model.get('_id'),
                        _action: 'dec_level'
                    });
                    page.set({});
                    page.save({}, {
                        dataType: 'text',
                        success: function(){
                            var level = model.get('level') - 1;
                            //Update model
                            model.set('level', level);
                            //Update margin
                            var margin = (level - 1) * 15 + 'px';
                            childView.$el.css('marginLeft', margin);
                            //Update data id
                            childView.$el.attr('data-level', level);
                        }
                    });
                });
                //Move up
                pagesView.on('childview:move:up', function(childView, model){
                    var page = new DashManager.Entities.Page({
                        _id: model.get('_id'),
                        _action: 'move_up'
                    });
                    page.set({});
                    page.save({}, {
                        dataType: 'text',
                        success: function(){
                            var index = pagesView.collection.indexOf(model);
                            if(index){
                                //Remove page
                                pagesView.collection.remove(model);
                                //Add model at index-1
                                pagesView.collection.add(model, {at: index-1});
                                //Get new childView
                                var level = model.get('level');
                                var pageView = pagesView.children.findByModel(model);
                                //Update margin
                                var margin = (level - 1) * 15 + 'px';
                                pageView.$el.css('marginLeft', margin);
                                //Update order
                                var order = pageView.$el.data('order');
                                pageView.$el.attr('data-order', order-1);
                                pageView.$el.next().attr('data-order', order);
                            }
                        }
                    });
                });
                //Move down
                pagesView.on('childview:move:down', function(childView, model){
                    var page = new DashManager.Entities.Page({
                        _id: model.get('_id'),
                        _action: 'move_down'
                    });
                    page.set({});
                    page.save({}, {
                        dataType: 'text',
                        success: function(){
                            var index = pagesView.collection.indexOf(model);
                            //Remove page
                            pagesView.collection.remove(model);
                            //Add model at index+1
                            pagesView.collection.add(model, {at: index+1});
                            //Get new childView
                            var level = model.get('level');
                            var pageView = pagesView.children.findByModel(model);
                            //Update margin
                            var margin = (level - 1) * 15 + 'px';
                            pageView.$el.css('marginLeft', margin);
                            //Update order
                            var order = pageView.$el.data('order');
                            pageView.$el.attr('data-order', order+1);
                            pageView.$el.prev().attr('data-order', order);
                        }
                    });
                });
                //Publish page
                pagesView.on('childview:publish:page', function(childView, model){
                    var page = new DashManager.Entities.Page({
                        _id: model.get('_id'),
                        _action: 'publish'
                    });
                    page.set({});
                    page.save({}, {
                        dataType: 'text',
                        success: function(){}
                    });
                });
                //Unpublish page
                pagesView.on('childview:unpublish:page', function(childView, model){
                    var page = new DashManager.Entities.Page({
                        _id: model.get('_id'),
                        _action: 'unpublish'
                    });
                    page.set({});
                    page.save({}, {
                        dataType: 'text',
                        success: function(){}
                    });
                });
                DashManager.mainRegion.show(pagesView);
            });
        },
        showPageBlocks: function(page_id){
            //Fetch page
            var fetchingPage = DashManager.request('page:entity', page_id);
            $.when(fetchingPage).done(function(page){
                var pageBlocksView = new DashManager.DashApp.EntityViews.PageBlocksView({
                    model: page
                });
                //Show
                pageBlocksView.on('show', function(){

                });
                //Move up
                pageBlocksView.on('childview:move:up', function(childView, model){
                    var block = new DashManager.Entities.Block({
                        _id: model.get('_id'),
                        _action: 'move_up'
                    });
                    block.set({});
                    block.save({}, {
                        dataType: 'text',
                        success: function(){
                            var index = pageBlocksView.collection.indexOf(model);
                            if(index){
                                //Remove block
                                pageBlocksView.collection.remove(model);
                                //Add model at index-1
                                pageBlocksView.collection.add(model, {at: index-1});
                                //Get new childView
                                var blockView = pageBlocksView.children.findByModel(model);
                                //Update order
                                var order = blockView.$el.data('order');
                                blockView.$el.attr('data-order', order-1);
                                blockView.$el.next().attr('data-order', order);
                            }
                        }
                    });
                });
                //Move down
                pageBlocksView.on('childview:move:down', function(childView, model){
                    var block = new DashManager.Entities.Block({
                        _id: model.get('_id'),
                        _action: 'move_down'
                    });
                    block.set({});
                    block.save({}, {
                        dataType: 'text',
                        success: function(){
                            var index = pageBlocksView.collection.indexOf(model);
                            //Remove block
                            pageBlocksView.collection.remove(model);
                            //Add model at index+1
                            pageBlocksView.collection.add(model, {at: index+1});
                            //Get new childView
                            var blockView = pageBlocksView.children.findByModel(model);
                            //Update order
                            var order = blockView.$el.data('order');
                            blockView.$el.attr('data-order', order+1);
                            blockView.$el.prev().attr('data-order', order);
                        }
                    });
                });
                //Move subblock up
                pageBlocksView.on('childview:childview:move:up', function(childView, model, options){
                    var block = new DashManager.Entities.Block({
                        _id: options.block_id,
                        _container: options.container_id,
                        _action: 'move_up'
                    });
                    block.set({});
                    block.save({}, {
                        dataType: 'text',
                        success: function(){
                            location.reload();
                        }
                    });
                });
                //Move subblock down
                pageBlocksView.on('childview:childview:move:down', function(childView, model, options){
                    var block = new DashManager.Entities.Block({
                        _id: options.block_id,
                        _container: options.container_id,
                        _action: 'move_down'
                    });
                    block.set({});
                    block.save({}, {
                        dataType: 'text',
                        success: function(){
                            location.reload();
                        }
                    });
                });
                //Add block
                DashManager.vent.off('add:block');
                DashManager.vent.on('add:block', function(block){
                    var order = block.get('order');
                    pageBlocksView.collection.add(block, {at: order});
                });
                //Remove block
                DashManager.vent.off('remove:block');
                DashManager.vent.on('remove:block', function(){
                    location.reload();
                });
                DashManager.mainRegion.show(pageBlocksView);
            });
        },
        showAllBlocksOverlay: function(){
            $('.overlay').show();
            //All blocks view
            var allBlocksView = new DashManager.DashApp.EntityViews.AllBlocksView();
            //Show
            allBlocksView.on('show', function(){
                //Animate overlay box
                setTimeout(function(){
                    allBlocksView.$('.overlay-box').addClass('animate');
                }, 100);
                //Hide scroll on main page
                DashManager.commands.execute('show:overlay');
            });
            DashManager.overlayRegion.show(allBlocksView);
        },
        showNewBlockOverlay: function(type){
            var excluded_ids = [];
            //New block view
            var newBlockView = new DashManager.DashApp.EntityViews.NewBlockView();
            //Editor
            var richTextEditor;
            var richTextEditorFiles = [];
            //Show
            newBlockView.on('show', function(){
                newBlockView.$('.overlay-box').addClass('animate');
                //Show section based on type
                var class_name = '.new-block-panel .block-' + type;
                newBlockView.$(class_name).removeClass('u-hide');
                //Calendar
                if(type == 'calendar'){
                    //Show date picker
                    newBlockView.$('.event-start, .event-end').datetimepicker({
                        dateFormat: 'dd-mm-yy',
                        changeMonth: true,
                        changeYear: true,
                        minDate: 0,
                        controlType: 'select',
                        oneLine: true,
                        timeFormat: 'hh:mm tt'
                    });
                }
                //Editor
                if(type == 'body-h'){
                    //Wait till editor is ready
                    newBlockView.$('.block-body-h').bind('click mousedown dblclick', function(ev){
                       ev.preventDefault();
                       ev.stopImmediatePropagation();
                    });
                    richTextEditor = setUpAlloyToolbar(false, document.querySelector('.body-h-content'), false, false);
                    var nativeEditor = richTextEditor.get('nativeEditor');
                    //On editor ready
                    nativeEditor.on('instanceReady', function(ev){
                        newBlockView.$('.block-body-h').unbind();
                    });
                    //On image upload
                    nativeEditor.on('imageAdd', function(ev){
                        var id = generateRandomUUID();
                        ev.data.file.id = id;
                        richTextEditorFiles.push(ev.data.file);
                        $(ev.data.el.$).addClass('upload-image').attr('data-id', id);
                    });
                }
                //Typeahead for persons
                //Remote fetch person list
                var personList = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                    queryTokenizer: Bloodhound.tokenizers.obj.whitespace,
                    remote: {
                        url: '/api/search/persons?text=%QUERY&excluded=%EXCLUDED',
                        replace: function(url){
                            return url.replace('%EXCLUDED', JSON.stringify(excluded_ids)).replace('%QUERY', newBlockView.$('.person-input.tt-input').val());
                        },
                        filter: function(parsedResponse){
                            return parsedResponse;
                        }
                    }
                });
                //Initialize personlist
                personList.initialize();
                //Show typeahead
                newBlockView.$('.person-input').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'email',
                    displayKey: 'name',
                    limit: 5,
                    source: personList.ttAdapter(),
                    templates: {
                        empty: [
                          '<div class="no-find">',
                          'No such user exists. Add new in People tab.',
                          '</div>'
                        ].join('\n'),
                        suggestion: Handlebars.compile("<p class='name'>{{name}}</p><p class='email'>{{email}}</p>")
                    }
                });
                //Focus
                newBlockView.$('.person-input').focus();
                //Add new person on typeahead autocomplete
                newBlockView.$('.person-input').on('typeahead:selected typeahead:autocompleted', function(e, datum){
                    var $input = newBlockView.$('.person-input');
                    $input.typeahead('val','').focus();
                    newBlockView.$('.persons-list').append("<div class='one-person' data-id='"+datum._id+"'><p class='name'>"+datum.name+"<br><span>"+datum.email+"</span></p><span class='remove-person u-delete'>Remove</span></div>");
                    excluded_ids.push(datum._id);
                });
                //Typeahead for partners
                //Remote fetch partner list
                var partnerList = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                    queryTokenizer: Bloodhound.tokenizers.obj.whitespace,
                    remote: {
                        url: '/api/search/partners?text=%QUERY&excluded=%EXCLUDED',
                        replace: function(url){
                            return url.replace('%EXCLUDED', JSON.stringify(excluded_ids)).replace('%QUERY', newBlockView.$('.partner-input.tt-input').val());
                        },
                        filter: function(parsedResponse){
                            return parsedResponse;
                        }
                    }
                });
                //Initialize partnerList
                partnerList.initialize();
                //Show typeahead
                newBlockView.$('.partner-input').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'email',
                    displayKey: 'name',
                    limit: 5,
                    source: partnerList.ttAdapter(),
                    templates: {
                        empty: [
                          '<div class="no-find">',
                          'No such partner exists. Add new in People tab.',
                          '</div>'
                        ].join('\n'),
                        suggestion: Handlebars.compile("<p class='name'>{{name}}</p>")
                    }
                });
                //Focus
                newBlockView.$('.partner-input').focus();
                //Add new partner on typeahead autocomplete
                newBlockView.$('.partner-input').on('typeahead:selected typeahead:autocompleted', function(e, datum){
                    var $input = newBlockView.$('.partner-input');
                    $input.typeahead('val','').focus();
                    newBlockView.$('.partners-list').append("<div class='one-person' data-id='"+datum._id+"'><p class='name'>"+datum.name+"</p><span class='remove-person u-delete'>Remove</span></div>");
                    excluded_ids.push(datum._id);
                });
            });
            //Save body HTML
            newBlockView.on('save:htmlBlock', function(value){
                if(richTextEditor){
                    var nativeEditor = richTextEditor.get('nativeEditor');
                    var html = nativeEditor.getData();
                    var new_block = new DashManager.Entities.Block({
                        type: value.type,
                        html: html,
                        order: value.order,
                        block: value.block,
                        page: value.page
                    });
                    //Save and upload image
                    async.series([
                        function(callback){
                            if(newBlockView.$('.body-h-content .upload-image').length){
                                newBlockView.$('.js-save-block').text('Uploading...').addClass('uploading');
                                //Upload
                                editorUploadImage(richTextEditorFiles, function(image_urls){
                                    richTextEditorFiles = [];
                                    if(image_urls && image_urls.length){
                                        new_block.set('html', nativeEditor.getData());
                                        new_block.set('images', image_urls);
                                        callback();
                                    } else {
                                        callback();
                                    }
                                });
                            } else {
                                callback();
                            }
                        }
                    ],
                    function(err){
                        newBlockView.$('.js-save-block').text('Save').removeClass('uploading');
                        new_block.save({}, {success: function(){
                            richTextEditor.destroy();
                            DashManager.vent.trigger('add:block', new_block);
                            DashManager.commands.execute('close:overlay');
                        }});
                    });
                }
            });
            //Remove person
            newBlockView.on('remove:person', function(value){
                //Remove element from excluded ids
                var index = excluded_ids.indexOf(value.person_id);
                if(index > -1){
                    excluded_ids.splice(index, 1);
                }
            });
            //Save block
            newBlockView.on('save:block', function(value){
                var new_block = new DashManager.Entities.Block({
                    type: value.type,
                    title: value.title,
                    desc: value.desc,
                    image_m: value.image_m,
                    image_l: value.image_l,
                    icon: value.icon,
                    embed: value.embed,
                    button_text: value.button_text,
                    button_url: value.button_url,
                    button_embed: value.button_embed,
                    buttonb_text: value.buttonb_text,
                    buttonb_url: value.buttonb_url,
                    buttonb_embed: value.buttonb_embed,
                    story_title: value.story_title,
                    story_text: value.story_text,
                    story_url: value.story_url,
                    formula: value.formula,
                    row_count: value.row_count,
                    people: excluded_ids,
                    events: value.events,
                    gallery: value.gallery,
                    order: value.order,
                    block: value.block,
                    page: value.page
                });
                new_block.save({}, {success: function(){
                    DashManager.vent.trigger('add:block', new_block);
                    //If events
                    if(value.events){
                        calEventsArray = [];
                    }
                    //If gallery
                    if(value.gallery){
                        carouselImagesArray = [];
                    }
                    //If formula is tags
                    if(value.formula == 'tags'){
                        DashManager.vent.trigger('blockTags:show', new_block.get('_id'));
                    } else {
                        DashManager.commands.execute('close:overlay');
                    }
                }});
            });
            DashManager.overlayRegion.show(newBlockView);
        },
        showEditBlockOverlay: function(block_id, container_id){
            var excluded_ids = [];
            $('.overlay').show();
            //Fetch block
            var fetchingBlock = DashManager.request('block:entity', block_id, container_id);
            $.when(fetchingBlock).done(function(block){
                var newBlockView = new DashManager.DashApp.EntityViews.NewBlockView();
                //Editor
                var richTextEditor;
                var richTextEditorFiles = [];
                //Show
                newBlockView.on('show', function(){
                    //Add edit class
                    newBlockView.$('.overlay-box').addClass('edit-box');
                    //Animate overlay box
                    setTimeout(function(){
                        newBlockView.$('.overlay-box').addClass('animate');
                    }, 100);
                    //Hide scroll on main page
                    DashManager.commands.execute('show:overlay');
                    newBlockView.$('.js-delete-block').removeClass('u-hide');
                    //Show section based on type
                    var type = block.get('type');
                    //Fill values
                    if(type == 'header'){
                        newBlockView.$('.block-header').removeClass('u-hide');
                        //Header block
                        this.$('.header-title').val(block.get('text').title);
                        this.$('.header-desc').val(block.get('text').desc);
                        this.$('.header-image-m').val(block.get('image').m);
                        this.$('.header-image-l').val(block.get('image').l);
                        this.$('.header-button-text').val(block.get('button').text);
                        //Button url
                        var button_url = block.get('button').url || block.get('button').embed;
                        this.$('.header-button-url').val(button_url);
                        //Button B
                        if(block.get('buttonb')){
                            this.$('.header-buttonb-text').val(block.get('buttonb').text);
                            var buttonb_url = block.get('buttonb').url || block.get('buttonb').embed;
                            this.$('.header-buttonb-url').val(buttonb_url);
                        }
                        //Story
                        if(block.get('story')){
                            this.$('.header-story-title').val(block.get('story').title);
                            this.$('.header-story-text').val(block.get('story').text);
                            this.$('.header-story-url').val(block.get('story').url);
                        }
                    } else if(type == 'header_video'){
                        newBlockView.$('.block-header-v').removeClass('u-hide');
                        //Header video block
                        this.$('.header-v-title').val(block.get('text').title);
                        this.$('.header-v-desc').val(block.get('text').desc);
                        this.$('.header-v-embed').val(block.get('url').embed);
                        this.$('.header-v-button-text').val(block.get('button').text);
                        //Button url
                        var button_url = block.get('button').url || block.get('button').embed;
                        this.$('.header-v-button-url').val(button_url);
                        //Button B
                        if(block.get('buttonb')){
                            this.$('.header-v-buttonb-text').val(block.get('buttonb').text);
                            var buttonb_url = block.get('buttonb').url || block.get('buttonb').embed;
                            this.$('.header-v-buttonb-url').val(buttonb_url);
                        }
                    } else if(type == 'container'){
                        //Container
                        var value = {
                            type: 'container',
                            row_count: this.$('.container-rows').val().trim()
                        }
                        //Select formula
                        if($('.block-container .choose-projects').hasClass('selected')){
                            value.formula = 'projects';
                        } else if($('.block-container .choose-events').hasClass('selected')){
                            value.formula = 'events';
                        } else if($('.block-container .choose-articles').hasClass('selected')){
                            if($('.block-container .choose-news').hasClass('selected')){
                                value.formula = 'news';
                            } else if($('.block-container .choose-directors').hasClass('selected')){
                                value.formula = 'directors';
                            } else if($('.block-container .choose-resources').hasClass('selected')){
                                value.formula = 'resources';
                            } else {
                                value.formula = 'blog';
                            }
                        } else {
                            value.formula = 'empty';
                        }
                    } else if(type == 'section'){
                        newBlockView.$('.block-section').removeClass('u-hide');
                        //Section
                        this.$('.section-title').val(block.get('text').title);
                        this.$('.section-desc').val(block.get('text').desc);
                        this.$('.section-button-text').val(block.get('button').text);
                        //Button url
                        var button_url = block.get('button').url || block.get('button').embed;
                        this.$('.section-button-url').val(button_url);
                    } else if(type == 'body_text'){
                        newBlockView.$('.block-body-t').removeClass('u-hide');
                        //Body text block
                        this.$('.body-t-title').val(block.get('text').title);
                        this.$('.body-t-desc').val(block.get('text').desc);
                        this.$('.body-t-image-m').val(block.get('image').m);
                        this.$('.body-t-icon').val(block.get('image').icon);
                        this.$('.body-t-button-text').val(block.get('button').text);
                        //Button url
                        var button_url = block.get('button').url || block.get('button').embed;
                        this.$('.body-t-button-url').val(button_url);
                    } else if(type == 'body_html'){
                        newBlockView.$('.block-body-h').removeClass('u-hide');
                        this.$('.body-h-content').html(block.get('text').html);
                        //Wait till editor is ready
                        newBlockView.$('.block-body-h').bind('click mousedown dblclick', function(ev){
                           ev.preventDefault();
                           ev.stopImmediatePropagation();
                        });
                        richTextEditor = setUpAlloyToolbar(false, document.querySelector('.body-h-content'), false, false);
                        var nativeEditor = richTextEditor.get('nativeEditor');
                        //On editor ready
                        nativeEditor.on('instanceReady', function(ev){
                            newBlockView.$('.block-body-h').unbind();
                        });
                        //On image upload
                        nativeEditor.on('imageAdd', function(ev){
                            var id = generateRandomUUID();
                            ev.data.file.id = id;
                            richTextEditorFiles.push(ev.data.file);
                            $(ev.data.el.$).addClass('upload-image').attr('data-id', id);
                        });
                    } else if(type == 'body_embed'){
                        newBlockView.$('.block-body-e').removeClass('u-hide');
                        //Body embed block
                        this.$('.body-e-title').val(block.get('text').title);
                        this.$('.body-e-desc').val(block.get('text').desc);
                        this.$('.body-e-embed').val(block.get('url').embed);
                        this.$('.body-e-button-text').val(block.get('button').text);
                        //Button url
                        var button_url = block.get('button').url || block.get('button').embed;
                        this.$('.body-e-button-url').val(button_url);
                    } else if(type == 'feed'){
                        newBlockView.$('.block-feed').removeClass('u-hide');
                        //Feed
                        this.$('.feed-title').val(block.get('text').title);
                        this.$('.feed-embed').val(block.get('url').embed);
                    } else if(type == 'people'){
                        newBlockView.$('.block-people').removeClass('u-hide');
                        //People
                        this.$('.people-title').val(block.get('text').title);
                        this.$('.people-desc').val(block.get('text').desc);
                        var all_people = block.get('people');
                        if(all_people && all_people.length){
                            for(var i=0; i<all_people.length; i++){
                                var people = all_people[i];
                                newBlockView.$('.persons-list').append("<div class='one-person' data-id='"+people._id+"'><p class='name'>"+people.name+"<br><span>"+people.email+"</span></p><span class='remove-person u-delete'>Remove</span></div>");
                                excluded_ids.push(people._id);
                            }
                        }
                    } else if(type == 'logos'){
                        newBlockView.$('.block-logos').removeClass('u-hide');
                        //Logos
                        this.$('.logos-title').val(block.get('text').title);
                        this.$('.logos-desc').val(block.get('text').desc);
                        var all_partners = block.get('people');
                        if(all_partners && all_partners.length){
                            for(var i=0; i<all_partners.length; i++){
                                var partner = all_partners[i];
                                newBlockView.$('.partners-list').append("<div class='one-person' data-id='"+partner._id+"'><p class='name'>"+partner.name+"<br><span>"+partner.email+"</span></p><span class='remove-person u-delete'>Remove</span></div>");
                                excluded_ids.push(partner._id);
                            }
                        }
                    } else if(type == 'calendar'){
                        newBlockView.$('.block-calendar').removeClass('u-hide');
                        //Calendar
                        this.$('.calendar-title').val(block.get('text').title);
                        var all_events = block.get('events');
                        if(all_events && all_events.length){
                            for(var i=0; i<all_events.length; i++){
                                var event = all_events[i];
                                newBlockView.$('.events-list').append("<div class='one-event' data-id='"+event._id+"'><p class='title'>"+event.title+"</p><span class='remove-event u-delete'>Remove</span></div>");
                            }
                        }
                        //Show date picker
                        newBlockView.$('.event-start, .event-end').datetimepicker({
                            dateFormat: 'dd-mm-yy',
                            changeMonth: true,
                            changeYear: true,
                            minDate: 0,
                            controlType: 'select',
                            oneLine: true,
                            timeFormat: 'hh:mm tt'
                        });
                    } else if(type == 'body_carousel'){
                        newBlockView.$('.block-body-c').removeClass('u-hide');
                        //Carousel
                        var all_images = block.get('gallery');
                        if(all_images && all_images.length){
                            for(var i=0; i<all_images.length; i++){
                                var image = all_images[i];
                                newBlockView.$('.body-images-list').append("<div class='one-image' data-id='"+image._id+"'><p class='title'>"+image.file.l+"</p><span class='remove-image u-delete'>Remove</span></div>");
                            }
                        }
                    }
                    //Typeahead for persons
                    //Remote fetch person list
                    var personList = new Bloodhound({
                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                        queryTokenizer: Bloodhound.tokenizers.obj.whitespace,
                        remote: {
                            url: '/api/search/persons?text=%QUERY&excluded=%EXCLUDED',
                            replace: function(url){
                                return url.replace('%EXCLUDED', JSON.stringify(excluded_ids)).replace('%QUERY', newBlockView.$('.person-input.tt-input').val());
                            },
                            filter: function(parsedResponse){
                                return parsedResponse;
                            }
                        }
                    });
                    //Initialize personlist
                    personList.initialize();
                    //Show typeahead
                    newBlockView.$('.person-input').typeahead({
                        hint: true,
                        highlight: true,
                        minLength: 1
                    },
                    {
                        name: 'email',
                        displayKey: 'name',
                        limit: 5,
                        source: personList.ttAdapter(),
                        templates: {
                            empty: [
                              '<div class="no-find">',
                              'No such user exists. Add new in People tab.',
                              '</div>'
                            ].join('\n'),
                            suggestion: Handlebars.compile("<p class='name'>{{name}}</p><p class='email'>{{email}}</p>")
                        }
                    });
                    //Focus
                    newBlockView.$('.person-input').focus();
                    //Add new person on typeahead autocomplete
                    newBlockView.$('.person-input').on('typeahead:selected typeahead:autocompleted', function(e, datum){
                        if(container_id){
                            var edit_block = new DashManager.Entities.Block({
                                _id: block_id,
                                _container: container_id,
                                _action: 'add_person'
                            });
                        } else {
                            var edit_block = new DashManager.Entities.Block({
                                _id: block_id,
                                _action: 'add_person'
                            });
                        }
                        //Set
                        edit_block.set({
                            person: datum._id
                        });
                        edit_block.save({}, {success: function(){
                            var $input = newBlockView.$('.person-input');
                            $input.typeahead('val','').focus();
                            newBlockView.$('.persons-list').append("<div class='one-person' data-id='"+datum._id+"'><p class='name'>"+datum.name+"<br><span>"+datum.email+"</span></p><span class='remove-person u-delete'>Remove</span></div>");
                            excluded_ids.push(datum._id);
                        }});
                    });
                    //Typeahead for partners
                    //Remote fetch partner list
                    var partnerList = new Bloodhound({
                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                        queryTokenizer: Bloodhound.tokenizers.obj.whitespace,
                        remote: {
                            url: '/api/search/partners?text=%QUERY&excluded=%EXCLUDED',
                            replace: function(url){
                                return url.replace('%EXCLUDED', JSON.stringify(excluded_ids)).replace('%QUERY', newBlockView.$('.partner-input.tt-input').val());
                            },
                            filter: function(parsedResponse){
                                return parsedResponse;
                            }
                        }
                    });
                    //Initialize partnerList
                    partnerList.initialize();
                    //Show typeahead
                    newBlockView.$('.partner-input').typeahead({
                        hint: true,
                        highlight: true,
                        minLength: 1
                    },
                    {
                        name: 'email',
                        displayKey: 'name',
                        limit: 5,
                        source: partnerList.ttAdapter(),
                        templates: {
                            empty: [
                              '<div class="no-find">',
                              'No such partner exists. Add new in People tab.',
                              '</div>'
                            ].join('\n'),
                            suggestion: Handlebars.compile("<p class='name'>{{name}}</p>")
                        }
                    });
                    //Focus
                    newBlockView.$('.partner-input').focus();
                    //Add new partner on typeahead autocomplete
                    newBlockView.$('.partner-input').on('typeahead:selected typeahead:autocompleted', function(e, datum){
                        if(container_id){
                            var edit_block = new DashManager.Entities.Block({
                                _id: block_id,
                                _container: container_id,
                                _action: 'add_person'
                            });
                        } else {
                            var edit_block = new DashManager.Entities.Block({
                                _id: block_id,
                                _action: 'add_person'
                            });
                        }
                        //Set
                        edit_block.set({
                            person: datum._id
                        });
                        edit_block.save({}, {success: function(){
                            var $input = newBlockView.$('.partner-input');
                            $input.typeahead('val','').focus();
                            newBlockView.$('.partners-list').append("<div class='one-person' data-id='"+datum._id+"'><p class='name'>"+datum.name+"<br><span>"+datum.email+"</span></p><span class='remove-person u-delete'>Remove</span></div>");
                            excluded_ids.push(datum._id);
                        }});
                    });
                });
                //Remove person
                newBlockView.on('removeCreated:person', function(value){
                    if(container_id){
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _container: container_id,
                            _action: 'remove_person'
                        });
                    } else {
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _action: 'remove_person'
                        });
                    }
                    //Set
                    edit_block.set({
                        person: value.person_id
                    });
                    edit_block.save({}, {
                        dataType: 'text',
                        success: function(){
                            //Remove element from excluded ids
                            var index = excluded_ids.indexOf(value.person_id);
                            if(index > -1){
                                excluded_ids.splice(index, 1);
                            }
                        }
                    });
                });
                //Add event
                newBlockView.on('add:event', function(value){
                    if(container_id){
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _container: container_id,
                            _action: 'add_event'
                        });
                    } else {
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _action: 'add_event'
                        });
                    }
                    //Set
                    edit_block.set({
                        title: value.title,
                        desc: value.desc,
                        start_date: value.start_date,
                        end_date: value.end_date,
                        image_l: value.image_l,
                        image_m: value.image_m,
                        url: value.url,
                        location: value.location
                    });
                    edit_block.save({}, {success: function(){
                        newBlockView.$('.events-list').append("<div class='one-event' data-id='"+edit_block.get('_id')+"'><p class='title'>"+edit_block.get('title')+"</p><span class='remove-event u-delete'>Remove</span></div>");
                    }});
                });
                //Remove event
                newBlockView.on('remove:event', function(value){
                    if(container_id){
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _container: container_id,
                            _action: 'remove_event'
                        });
                    } else {
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _action: 'remove_event'
                        });
                    }
                    //Set
                    edit_block.set({
                        event: value.event_id
                    });
                    edit_block.save();
                });
                //Add image
                newBlockView.on('add:image', function(value){
                    if(container_id){
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _container: container_id,
                            _action: 'add_image'
                        });
                    } else {
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _action: 'add_image'
                        });
                    }
                    //Set
                    edit_block.set({
                        title: value.title,
                        image_l: value.image_l,
                        image_m: value.image_m,
                        button_text: value.button_text,
                        button_url: value.button_url,
                        bound: value.bound
                    });
                    edit_block.save({}, {success: function(){
                        newBlockView.$('.body-images-list').append("<div class='one-image' data-id='"+edit_block.get('_id')+"'><p class='title'>"+edit_block.get('file').l+"</p><span class='remove-image u-delete'>Remove</span></div>");
                    }});
                });
                //Remove image
                newBlockView.on('remove:image', function(value){
                    if(container_id){
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _container: container_id,
                            _action: 'remove_image'
                        });
                    } else {
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _action: 'remove_image'
                        });
                    }
                    //Set
                    edit_block.set({
                        image: value.image_id
                    });
                    edit_block.save();
                });
                //Update block
                newBlockView.on('update:block', function(value){
                    if(container_id){
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _container: container_id,
                            _action: 'edit'
                        });
                    } else {
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _action: 'edit'
                        });
                    }
                    //Set
                    edit_block.set({
                        title: value.title,
                        desc: value.desc,
                        image_m: value.image_m,
                        image_l: value.image_l,
                        icon: value.icon,
                        embed: value.embed,
                        button_text: value.button_text,
                        button_url: value.button_url,
                        button_embed: value.button_embed,
                        buttonb_text: value.buttonb_text,
                        buttonb_url: value.buttonb_url,
                        buttonb_embed: value.buttonb_embed,
                        story_title: value.story_title,
                        story_text: value.story_text,
                        story_url: value.story_url
                    });
                    edit_block.save({}, {success: function(){
                        DashManager.commands.execute('close:overlay');
                    }});
                });
                //Update HTML block
                newBlockView.on('update:htmlBlock', function(value){
                    if(richTextEditor){
                        var nativeEditor = richTextEditor.get('nativeEditor');
                        var html = nativeEditor.getData();
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _action: 'edit'
                        });
                        edit_block.set({
                            html: html
                        });
                        //Save and upload image
                        async.series([
                            function(callback){
                                if(newBlockView.$('.body-h-content .upload-image').length){
                                    newBlockView.$('.js-save-block').text('Uploading...').addClass('uploading');
                                    //Upload
                                    editorUploadImage(richTextEditorFiles, function(image_urls){
                                        richTextEditorFiles = [];
                                        if(image_urls && image_urls.length){
                                            edit_block.set('html', nativeEditor.getData());
                                            edit_block.set('images', image_urls);
                                            callback();
                                        } else {
                                            callback();
                                        }
                                    });
                                } else {
                                    callback();
                                }
                            }
                        ],
                        function(err){
                            newBlockView.$('.js-save-block').text('Save').removeClass('uploading');
                            edit_block.save({}, {success: function(){
                                richTextEditor.destroy();
                                DashManager.commands.execute('close:overlay');
                            }});
                        });
                    }
                });
                //Delete block
                newBlockView.on('delete:block', function(value){
                    if(container_id){
                        //Remove sub block
                        var block = new DashManager.Entities.Block({
                            _id: container_id,
                            _action: 'remove_block'
                        });
                        block.set({
                            sub_block: block_id
                        });
                        block.save({}, {
                            dataType: 'text',
                            success: function(){
                                DashManager.commands.execute('close:overlay');
                                location.reload();
                            }
                        });
                    } else {
                        //Delete block
                        var block = new DashManager.Entities.Block({
                            _id: block_id
                        });
                        block.destroy({
                            dataType: 'text',
                            success: function(model, response){
                                DashManager.commands.execute('close:overlay');
                                DashManager.vent.trigger('remove:block');
                            }
                        });
                    }
                });
                DashManager.overlayRegion.show(newBlockView);
            });
        },
        showEditThemeOverlay: function(block_id, container_id){
            $('.overlay').show();
            //Fetch block
            var fetchingBlock = DashManager.request('block:entity', block_id, container_id);
            $.when(fetchingBlock).done(function(block){
                var blockThemeView = new DashManager.DashApp.EntityViews.BlockThemeView();
                //Show
                blockThemeView.on('show', function(){
                    //Add edit class
                    blockThemeView.$('.overlay-box').addClass('edit-box');
                    //Animate overlay box
                    setTimeout(function(){
                        blockThemeView.$('.overlay-box').addClass('animate');
                    }, 100);
                    //Hide scroll on main page
                    DashManager.commands.execute('show:overlay');
                    //Fill values
                    if(block.get('color').a) this.$('.color-a').val(block.get('color').a);
                    if(block.get('color').b) this.$('.color-b').val(block.get('color').b);
                });
                //Update block's theme
                blockThemeView.on('update:block', function(value){
                    if(container_id){
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _container: container_id,
                            _action: 'edit'
                        });
                    } else {
                        var edit_block = new DashManager.Entities.Block({
                            _id: block_id,
                            _action: 'edit'
                        });
                    }
                    //Set
                    edit_block.set({
                        color_a: value.color_a,
                        color_b: value.color_b
                    });
                    edit_block.save({}, {success: function(){
                        DashManager.commands.execute('close:overlay');
                    }});
                });
                DashManager.overlayRegion.show(blockThemeView);
            });
        },
        showBlockTagsOverlay: function(block_id){
            $('.overlay').show();
            var excluded_tags = [];
            //Fetch block
            var fetchingBlock = DashManager.request('block:entity', block_id);
            $.when(fetchingBlock).done(function(block){
                var tags = new Backbone.Collection(block.get('tags'));
                var blockTagsView = new DashManager.DashApp.EntityViews.BlockTagsView({
                    collection: tags
                });
                //Show
                blockTagsView.on('show', function(){
                    //Animate overlay box
                    setTimeout(function(){
                        blockTagsView.$('.overlay-box').addClass('animate');
                    }, 100);
                    //Hide scroll on main page
                    DashManager.commands.execute('show:overlay');
                    //Add previous tag ids to excluded ids
                    if(block.get('tags') && block.get('tags').length){
                        var all_tags = block.get('tags');
                        for(var i=0; i<all_tags.length; i++){
                            excluded_tags.push(all_tags[i].name);
                        }
                    }
                    //Typeahead for block tags
                    //Remote fetch tag list
                    var tagList = new Bloodhound({
                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                        queryTokenizer: Bloodhound.tokenizers.obj.whitespace,
                        remote: {
                            url: '/api/search/articleTags?name=%QUERY&excluded=%EXCLUDED',
                            replace: function(url){
                                return url.replace('%EXCLUDED', JSON.stringify(excluded_tags)).replace('%QUERY', blockTagsView.$('.tags-input.tt-input').val());
                            },
                            filter: function(parsedResponse){
                                return parsedResponse;
                            }
                        }
                    });
                    //Initialize tagList
                    tagList.initialize();
                    //Show typeahead
                    blockTagsView.$('.tags-input').typeahead({
                        hint: true,
                        highlight: true,
                        minLength: 1
                    },
                    {
                        name: 'name',
                        displayKey: 'name',
                        limit: 5,
                        source: tagList.ttAdapter(),
                        templates: {
                            empty: [
                              '<div class="no-find">',
                              'No such tag found.',
                              '</div>'
                            ].join('\n'),
                            suggestion: Handlebars.compile("<p class='name'>{{name}}</p><p class='count'>{{count}} articles</p>")
                        }
                    });
                    //Focus
                    blockTagsView.$('.tags-input').focus();
                    //Add new related page on typeahead autocomplete
                    blockTagsView.$('.tags-input').on('typeahead:selected typeahead:autocompleted', function(e, datum){
                        var new_tag = new DashManager.Entities.Block({
                            _id: block_id,
                            _action: 'add_tag'
                        });
                        new_tag.set({
                            tag: datum.name
                        });
                        new_tag.save({}, {success: function(){
                            var $input = blockTagsView.$('.tags-input');
                            $input.typeahead('val', '').focus();
                            //Add tag
                            var tag = new_tag.toJSON();
                            tags.add(tag);
                            //Add to excluded
                            excluded_tags.push(datum.name);
                        }});
                    });
                });
                //Remove tag
                blockTagsView.on('childview:remove:tag', function(childView, model){
                    var block = new DashManager.Entities.Block({
                        _id: block_id,
                        _action: 'remove_tag'
                    });
                    block.set({
                        tag: model.tag_id
                    });
                    block.save({}, {
                        dataType: 'text',
                        success: function(){
                            childView.$el.remove();
                            var index = excluded_tags.indexOf(model.tag_name);
                            if(index > -1){
                                excluded_tags.splice(index, 1);
                            }
                        }
                    });
                });
                DashManager.overlayRegion.show(blockTagsView);
            });
        },
        showNewArticleOverlay: function(){
            $('.overlay').show();
            //New article view
            var newArticleView = new DashManager.DashApp.EntityViews.NewArticleView();
            //Show
            newArticleView.on('show', function(){
                //Animate overlay box
                setTimeout(function(){
                    newArticleView.$('.overlay-box').addClass('animate');
                }, 100);
                //Hide scroll on main page
                DashManager.commands.execute('show:overlay');
                //Focus
                newArticleView.$('.article-title').focus();
            });
            //Save
            newArticleView.on('save:article', function(value){
                var new_article = new DashManager.Entities.Block({
                    type: 'content',
                    title: value.title,
                    desc: value.desc,
                    image_m: value.image_m,
                    image_l: value.image_l,
                    icon: value.icon,
                    ref: value.ref,
                    embed: value.embed,
                    category: value.category
                });
                new_article.save({}, {success: function(){
                    DashManager.vent.trigger('add:article', new_article);
                    DashManager.commands.execute('close:overlay');
                }});
            });
            DashManager.overlayRegion.show(newArticleView);
        },
        showArticlePersonsOverlay: function(article_id){
            $('.overlay').show();
            var excluded_ids = [];
            //Fetch article
            var fetchingArticle = DashManager.request('block:entity', article_id);
            $.when(fetchingArticle).done(function(article){
                var persons = new Backbone.Collection(article.get('persons'));
                var personsView = new DashManager.DashApp.EntityViews.PersonsListView({
                    collection: persons
                });
                //Show
                personsView.on('show', function(){
                    //Animate overlay box
                    setTimeout(function(){
                        personsView.$('.overlay-box').addClass('animate');
                    }, 100);
                    //Hide scroll on main page
                    DashManager.commands.execute('show:overlay');
                    //Add previous persons ids to excluded ids
                    if(article.get('persons') && article.get('persons').length){
                        var article_persons = article.get('persons');
                        for(var i=0; i<article_persons.length; i++){
                            excluded_ids.push(article_persons[i]._id);
                        }
                    }
                    //Typeahead for persons
                    //Remote fetch person list
                    var personList = new Bloodhound({
                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                        queryTokenizer: Bloodhound.tokenizers.obj.whitespace,
                        remote: {
                            url: '/api/search/persons?text=%QUERY&excluded=%EXCLUDED',
                            replace: function(url){
                                return url.replace('%EXCLUDED', JSON.stringify(excluded_ids)).replace('%QUERY', personsView.$('.person-input.tt-input').val());
                            },
                            filter: function(parsedResponse){
                                return parsedResponse;
                            }
                        }
                    });
                    //Initialize personlist
                    personList.initialize();
                    //Show typeahead
                    personsView.$('.person-input').typeahead({
                        hint: true,
                        highlight: true,
                        minLength: 1
                    },
                    {
                        name: 'email',
                        displayKey: 'name',
                        limit: 5,
                        source: personList.ttAdapter(),
                        templates: {
                            empty: [
                              '<div class="no-find">',
                              'No such user exists. Add new in People tab.',
                              '</div>'
                            ].join('\n'),
                            suggestion: Handlebars.compile("<p class='name'>{{name}}</p><p class='email'>{{email}}</p>")
                        }
                    });
                    //Focus
                    personsView.$('.person-input').focus();
                    //Add new person on typeahead autocomplete
                    personsView.$('.person-input').on('typeahead:selected typeahead:autocompleted', function(e, datum){
                        var new_person = new DashManager.Entities.Block({
                            _id: article_id,
                            _action: 'add_person'
                        });
                        new_person.set({
                            person: datum._id
                        });
                        new_person.save({}, {success: function(){
                            var $input = personsView.$('.person-input');
                            $input.typeahead('val','').focus();
                            //Add person
                            persons.add(new_person);
                            //Add to excluded
                            excluded_ids.push(datum._id);
                        }});
                    });
                });
                //Remove person
                personsView.on('childview:remove:person', function(childView, model){
                    var block = new DashManager.Entities.Block({
                        _id: article_id,
                        _action: 'remove_person'
                    });
                    block.set({
                        person: model.person_id
                    });
                    block.save({}, {
                        dataType: 'text',
                        success: function(){
                            childView.$el.remove();
                            var index = excluded_ids.indexOf(model.person_id);
                            if(index > -1){
                                excluded_ids.splice(index, 1);
                            }
                        }
                    });
                });
                DashManager.overlayRegion.show(personsView);
            });
        },
        showRelatedPagesOverlay: function(article_id){
            $('.overlay').show();
            var excluded_ids = [];
            //Fetch article
            var fetchingArticle = DashManager.request('block:entity', article_id);
            $.when(fetchingArticle).done(function(article){
                var related_pages = new Backbone.Collection(article.get('related'));
                var relatedPagesView = new DashManager.DashApp.EntityViews.RelatedPagesView({
                    collection: related_pages
                });
                //Show
                relatedPagesView.on('show', function(){
                    //Animate overlay box
                    setTimeout(function(){
                        relatedPagesView.$('.overlay-box').addClass('animate');
                    }, 100);
                    //Hide scroll on main page
                    DashManager.commands.execute('show:overlay');
                    //Add previous related page ids to excluded ids
                    if(article.get('related') && article.get('related').length){
                        var article_related = article.get('related');
                        for(var i=0; i<article_related.length; i++){
                            excluded_ids.push(article_related[i]._id);
                        }
                    }
                    //Typeahead for related page
                    //Remote fetch related pages list
                    var relatedList = new Bloodhound({
                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                        queryTokenizer: Bloodhound.tokenizers.obj.whitespace,
                        remote: {
                            url: '/api/search/projectsAndEvents?text=%QUERY&excluded=%EXCLUDED',
                            replace: function(url){
                                return url.replace('%EXCLUDED', JSON.stringify(excluded_ids)).replace('%QUERY', relatedPagesView.$('.related-input.tt-input').val());
                            },
                            filter: function(parsedResponse){
                                return parsedResponse;
                            }
                        }
                    });
                    //Initialize relatedlist
                    relatedList.initialize();
                    //Show typeahead
                    relatedPagesView.$('.related-input').typeahead({
                        hint: true,
                        highlight: true,
                        minLength: 1
                    },
                    {
                        name: 'title',
                        displayKey: 'title',
                        limit: 5,
                        source: relatedList.ttAdapter(),
                        templates: {
                            empty: [
                              '<div class="no-find">',
                              'No such project or event exists. Add new in Pages tab.',
                              '</div>'
                            ].join('\n'),
                            suggestion: Handlebars.compile("<p class='name'>{{title}}</p>")
                        }
                    });
                    //Focus
                    relatedPagesView.$('.related-input').focus();
                    //Add new related page on typeahead autocomplete
                    relatedPagesView.$('.related-input').on('typeahead:selected typeahead:autocompleted', function(e, datum){
                        var new_related = new DashManager.Entities.Block({
                            _id: article_id,
                            _action: 'add_page'
                        });
                        new_related.set({
                            page: datum._id
                        });
                        new_related.save({}, {success: function(){
                            var $input = relatedPagesView.$('.related-input');
                            $input.typeahead('val','').focus();
                            //Add person
                            related_pages.add(new_related);
                            //Add to excluded
                            excluded_ids.push(datum._id);
                        }});
                    });
                });
                //Remove related
                relatedPagesView.on('childview:remove:related', function(childView, model){
                    var block = new DashManager.Entities.Block({
                        _id: article_id,
                        _action: 'remove_page'
                    });
                    block.set({
                        page: model.page_id
                    });
                    block.save({}, {
                        dataType: 'text',
                        success: function(){
                            childView.$el.remove();
                            var index = excluded_ids.indexOf(model.page_id);
                            if(index > -1){
                                excluded_ids.splice(index, 1);
                            }
                        }
                    });
                });
                DashManager.overlayRegion.show(relatedPagesView);
            });
        },
        showArticleTagsOverlay: function(article_id){
            $('.overlay').show();
            var excluded_tags = [];
            //Fetch article
            var fetchingArticle = DashManager.request('block:entity', article_id);
            $.when(fetchingArticle).done(function(article){
                var tags = new Backbone.Collection(article.get('tags'));
                var articleTagsView = new DashManager.DashApp.EntityViews.ArticleTagsView({
                    collection: tags
                });
                //Show
                articleTagsView.on('show', function(){
                    //Animate overlay box
                    setTimeout(function(){
                        articleTagsView.$('.overlay-box').addClass('animate');
                    }, 100);
                    //Hide scroll on main page
                    DashManager.commands.execute('show:overlay');
                    //Add previous tag ids to excluded ids
                    if(article.get('tags') && article.get('tags').length){
                        var all_tags = article.get('tags');
                        for(var i=0; i<all_tags.length; i++){
                            excluded_tags.push(all_tags[i].name);
                        }
                    }
                    //Typeahead for article tags
                    //Remote fetch tag list
                    var tagList = new Bloodhound({
                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                        queryTokenizer: Bloodhound.tokenizers.obj.whitespace,
                        remote: {
                            url: '/api/search/articleTags?name=%QUERY&excluded=%EXCLUDED',
                            replace: function(url){
                                return url.replace('%EXCLUDED', JSON.stringify(excluded_tags)).replace('%QUERY', articleTagsView.$('.tags-input.tt-input').val());
                            },
                            filter: function(parsedResponse){
                                return parsedResponse;
                            }
                        }
                    });
                    //Initialize tagList
                    tagList.initialize();
                    //Show typeahead
                    articleTagsView.$('.tags-input').typeahead({
                        hint: true,
                        highlight: true,
                        minLength: 1
                    },
                    {
                        name: 'name',
                        displayKey: 'name',
                        limit: 5,
                        source: tagList.ttAdapter(),
                        templates: {
                            empty: [
                              '<div class="no-find">',
                              'Press enter to add this tag.',
                              '</div>'
                            ].join('\n'),
                            suggestion: Handlebars.compile("<p class='name'>{{name}}</p><p class='count'>{{count}} articles</p>")
                        }
                    }).keyup(function(ev){
                        var $input = articleTagsView.$('.tags-input');
                        var tag_name = $input.typeahead('val');
                        if(articleTagsView.$('.no-find').length){
                            if(ev.which == ENTER_KEY){
                                //Check if in excluded
                                if(excluded_tags.indexOf(tag_name) > -1){
                                    $input.typeahead('val','').focus();
                                    return;
                                }
                                //Add new tag
                                var new_tag = new DashManager.Entities.Block({
                                    _id: article_id,
                                    _action: 'add_tag'
                                });
                                new_tag.set({
                                    tag: tag_name
                                });
                                new_tag.save({}, {success: function(){
                                    $input.typeahead('val','').focus();
                                    //Add tag
                                    var tag = new_tag.toJSON();
                                    tags.add(tag);
                                    //Add to excluded
                                    excluded_tags.push(tag.name);
                                }});
                            }
                        }
                    });
                    //Focus
                    articleTagsView.$('.tags-input').focus();
                    //Add new related page on typeahead autocomplete
                    articleTagsView.$('.tags-input').on('typeahead:selected typeahead:autocompleted', function(e, datum){
                        var new_tag = new DashManager.Entities.Block({
                            _id: article_id,
                            _action: 'add_tag'
                        });
                        new_tag.set({
                            tag: datum.name
                        });
                        new_tag.save({}, {success: function(){
                            var $input = articleTagsView.$('.tags-input');
                            $input.typeahead('val','').focus();
                            //Add tag
                            var tag = new_tag.toJSON();
                            tags.add(tag);
                            //Add to excluded
                            excluded_tags.push(tag.name);
                        }});
                    });
                });
                //Remove tag
                articleTagsView.on('childview:remove:tag', function(childView, model){
                    var block = new DashManager.Entities.Block({
                        _id: article_id,
                        _action: 'remove_tag'
                    });
                    block.set({
                        tag: model.tag_id
                    });
                    block.save({}, {
                        dataType: 'text',
                        success: function(){
                            childView.$el.remove();
                            var index = excluded_tags.indexOf(model.tag_name);
                            if(index > -1){
                                excluded_tags.splice(index, 1);
                            }
                        }
                    });
                });
                DashManager.overlayRegion.show(articleTagsView);
            });
        },
        showEditArticleOverlay: function(article_id){
            $('.overlay').show();
            //Fetch article
            var fetchingArticle = DashManager.request('block:entity', article_id);
            $.when(fetchingArticle).done(function(article){
                var newArticleView = new DashManager.DashApp.EntityViews.NewArticleView();
                //Show
                newArticleView.on('show', function(){
                    //Add edit class
                    newArticleView.$('.overlay-box').addClass('edit-box');
                    //Animate overlay box
                    setTimeout(function(){
                        newArticleView.$('.overlay-box').addClass('animate');
                    }, 100);
                    //Hide scroll on main page
                    DashManager.commands.execute('show:overlay');
                    newArticleView.$('.js-delete-article').removeClass('u-hide');
                    //Fill values
                    newArticleView.$('.article-title').val(article.get('text').title).focus();
                    newArticleView.$('.article-desc').val(article.get('text').desc);
                    newArticleView.$('.article-image').val(article.get('image').m);
                    newArticleView.$('.article-icon').val(article.get('image').icon);
                    //Show category
                    var category = article.get('category');
                    if(category == 'news'){
                        newArticleView.$('.choose-news').addClass('selected');
                    } else if(category == 'blog'){
                        newArticleView.$('.choose-blog').addClass('selected');
                    } else if(category == 'directors'){
                        newArticleView.$('.choose-directors').addClass('selected');
                    } else if(category == 'resources'){
                        newArticleView.$('.choose-resources').addClass('selected');
                    }
                    //Show url
                    if(article.get('url')){
                        var url = article.get('url').embed || article.get('url').ref;
                        newArticleView.$('.article-url').val(url);
                    }
                });
                //Update article
                newArticleView.on('update:article', function(value){
                    var edit_article = new DashManager.Entities.Block({
                        _id: article_id,
                        _action: 'edit'
                    });
                    edit_article.set({
                        title: value.title,
                        desc: value.desc,
                        image_m: value.image_m,
                        image_l: value.image_l,
                        icon: value.icon,
                        ref: value.ref,
                        embed: value.embed,
                        category: value.category
                    });
                    edit_article.save({}, {success: function(){
                        DashManager.commands.execute('close:overlay');
                    }});
                });
                //Delete article
                newArticleView.on('delete:article', function(value){
                    var block = new DashManager.Entities.Block({
                        _id: article_id
                    });
                    block.destroy({
                        dataType: 'text',
                        success: function(model, response){
                            DashManager.commands.execute('close:overlay');
                            DashManager.vent.trigger('remove:article');
                        }
                    });
                });
                DashManager.overlayRegion.show(newArticleView);
            });
        },
        showEditContentOverlay: function(article_id){
            $('.overlay').show();
            //Fetch article
            var fetchingArticle = DashManager.request('block:entity', article_id);
            $.when(fetchingArticle).done(function(article){
                var articleContentView = new DashManager.DashApp.EntityViews.ArticleContentView({
                    model: article
                });
                //Editor
                var articleEditor;
                var articleEditorFiles = [];
                //Show
                articleContentView.on('show', function(){
                    setTimeout(function(){
                        articleContentView.$('.overlay-box').addClass('animate');
                    }, 100);
                    //Hide scroll on main page
                    DashManager.commands.execute('show:overlay');
                    //Editor
                    //Wait till editor is ready
                    articleContentView.$('.article-content-panel').bind('click mousedown dblclick', function(ev){
                       ev.preventDefault();
                       ev.stopImmediatePropagation();
                    });
                    articleEditor = setUpAlloyToolbar(false, document.querySelector('.article-content'), false, false);
                    var nativeEditor = articleEditor.get('nativeEditor');
                    //On editor ready
                    nativeEditor.on('instanceReady', function(ev){
                        articleContentView.$('.article-content-panel').unbind();
                    });
                    //On image upload
                    nativeEditor.on('imageAdd', function(ev){
                        var id = generateRandomUUID();
                        ev.data.file.id = id;
                        articleEditorFiles.push(ev.data.file);
                        $(ev.data.el.$).addClass('upload-image').attr('data-id', id);
                    });
                });
                //Update article content
                articleContentView.on('update:articleContent', function(){
                    if(articleEditor){
                        var nativeEditor = articleEditor.get('nativeEditor');
                        var html = nativeEditor.getData();
                        //Update
                        var edit_article = new DashManager.Entities.Block({
                            _id: article_id,
                            _action: 'edit'
                        });
                        edit_article.set({
                            html: html
                        });
                        //Save and upload image
                        async.series([
                            function(callback){
                                if(articleContentView.$('.article-content .upload-image').length){
                                    articleContentView.$('.js-update-article').text('Uploading...').addClass('uploading');
                                    //Upload
                                    editorUploadImage(articleEditorFiles, function(image_urls){
                                        articleEditorFiles = [];
                                        if(image_urls && image_urls.length){
                                            var first_image = articleContentView.$('.article-content img').attr('src');
                                            edit_article.set('html', nativeEditor.getData());
                                            edit_article.set('images', image_urls);
                                            edit_article.set('thumbnail', first_image);
                                            callback();
                                        } else {
                                            callback();
                                        }
                                    });
                                } else {
                                    callback();
                                }
                            }
                        ],
                        function(err){
                            articleContentView.$('.js-update-article').text('Save').removeClass('uploading');
                            edit_article.save({}, {success: function(){
                                articleEditor.destroy();
                                DashManager.commands.execute('close:overlay');
                            }});
                        });
                    }
                });
                DashManager.overlayRegion.show(articleContentView);
            });
        },
        showArticles: function(category){
            $(window).off('scroll', scrollHandler);
            //Show loading page
            var loadingView = new DashManager.Common.Views.Loading();
            DashManager.mainRegion.show(loadingView);
            //Select
            $('.nav-links a').removeClass('selected');
            $('.nav-links .js-articles').addClass('selected');
            //Fetching articles
            var fetchingArticles = DashManager.request('article:entities', category);
            $.when(fetchingArticles).done(function(articles){
                var articlesView = new DashManager.DashApp.EntityViews.ArticlesView({
                    collection: articles
                });
                //Show
                articlesView.on('show', function(){
                    //Masonry
                    $('.all-items').masonry({
                        itemSelector: '.one-item',
                        columnWidth: 450
                    });
                    //Choose filter
                    if(category){
                        var element = '.filter-items .filter-' + category;
                        articlesView.$(element).addClass('selected');
                    }
                    //Pagination
                    $('.loading-data').data('page', 1).removeClass('u-loaded');
                    //Check if less than page size
                    if(articles.length < PAGE_SIZE) {
                        $('.loading-data').addClass('u-loaded');
                    }
                    //Fetch more articles
                    scrollHandler = function(){
                        if(($(window).scrollTop() + $(window).height() > $(document).height() - 50) &&  !$('.loading-data').hasClass('u-loaded') && !$('.loading-data').hasClass('u-loading')) {
                            $('.loading-data').slideDown().addClass('u-loading');
                            var fetchingMoreArticles = DashManager.request('article:entities', category, $('.loading-data').data('page') + 1);
                            $.when(fetchingMoreArticles).done(function(moreArticles){
                                articles.add(moreArticles.models);
                                $('.loading-data').data('page',  $('.loading-data').data('page') + 1).slideUp().removeClass('u-loading');
                                if(moreArticles.length < PAGE_SIZE) {
                                    $('.loading-data').addClass('u-loaded');
                                }
                                //Find view by model and update masonry
                                for(var i=0; i<moreArticles.models.length; i++){
                                    var child = articlesView.children.findByModel(moreArticles.models[i]);
                                    $('.all-items').masonry('appended', child.$el);
                                }
                            });
                        }
                    }
                    $(window).on('scroll', scrollHandler);
                });
                //Add article
                DashManager.vent.off('add:article');
                DashManager.vent.on('add:article', function(article){
                    articles.add(article, {at: 0});
                    articlesView.$('.all-items').masonry('prepended', $('.one-item').eq(0));
                });
                DashManager.mainRegion.show(articlesView);
            });
        },
        showArticleBlocks: function(article_id){
            //Show loading page
            var loadingView = new DashManager.Common.Views.Loading();
            DashManager.mainRegion.show(loadingView);
            //Select
            $('.nav-links a').removeClass('selected');
            $('.nav-links .js-articles').addClass('selected');
            //Fetch article
            var fetchingArticleBlocks = DashManager.request('articleBlock:entities', article_id);
            $.when(fetchingArticleBlocks).done(function(articleBlocks){
                var articleBlocksView = new DashManager.DashApp.EntityViews.ArticleBlocksView({
                    collection: articleBlocks
                });
                //Show
                articleBlocksView.on('show', function(){
                    articleBlocksView.$('.all-blocks').data('block', article_id);
                });
                //Move up
                articleBlocksView.on('childview:move:up', function(childView, model){
                    var block = new DashManager.Entities.ArticleBlock({
                        _id: model.get('_id'),
                        _action: 'move_up'
                    });
                    block.set({});
                    block.save({}, {
                        dataType: 'text',
                        success: function(){
                            var index = articleBlocksView.collection.indexOf(model);
                            if(index){
                                //Remove block
                                articleBlocksView.collection.remove(model);
                                //Add model at index-1
                                articleBlocksView.collection.add(model, {at: index-1});
                                //Get new childView
                                var blockView = articleBlocksView.children.findByModel(model);
                                //Update order
                                var order = blockView.$el.data('order');
                                blockView.$el.attr('data-order', order-1);
                                blockView.$el.next().attr('data-order', order);
                            }
                        }
                    });
                });
                //Move down
                articleBlocksView.on('childview:move:down', function(childView, model){
                    var block = new DashManager.Entities.ArticleBlock({
                        _id: model.get('_id'),
                        _action: 'move_down'
                    });
                    block.set({});
                    block.save({}, {
                        dataType: 'text',
                        success: function(){
                            var index = articleBlocks.indexOf(model);
                            //Remove block
                            articleBlocks.remove(model);
                            //Add model at index+1
                            articleBlocks.add(model, {at: index+1});
                            //Get new childView
                            var blockView = articleBlocksView.children.findByModel(model);
                            //Update order
                            var order = blockView.$el.data('order');
                            blockView.$el.attr('data-order', order+1);
                            blockView.$el.prev().attr('data-order', order);
                        }
                    });
                });
                //Add block
                DashManager.vent.off('add:articleBlock');
                DashManager.vent.on('add:articleBlock', function(block){
                    var order = block.get('order');
                    articleBlocks.add(block, {at: order});
                });
                //Remove block
                DashManager.vent.off('remove:articleBlock');
                DashManager.vent.on('remove:articleBlock', function(){
                    location.reload();
                });
                DashManager.mainRegion.show(articleBlocksView);
            });
        },
        showAllArticleBlocksOverlay: function(){
            $('.overlay').show();
            //All article blocks view
            var allArticleBlocksView = new DashManager.DashApp.EntityViews.AllArticleBlocksView();
            //Show
            allArticleBlocksView.on('show', function(){
                //Animate overlay box
                setTimeout(function(){
                    allArticleBlocksView.$('.overlay-box').addClass('animate');
                }, 100);
                //Hide scroll on main page
                DashManager.commands.execute('show:overlay');
            });
            DashManager.overlayRegion.show(allArticleBlocksView);
        },
        showNewArticleBlockOverlay: function(type){
            var excluded_ids = [];
            //New article block view
            var newArticleBlockView = new DashManager.DashApp.EntityViews.NewArticleBlockView();
            //Editor
            var richTextEditor;
            var richTextEditorFiles = [];
            //Show
            newArticleBlockView.on('show', function(){
                newArticleBlockView.$('.overlay-box').addClass('animate');
                //Show section based on type
                var class_name = '.new-block-panel .block-' + type;
                newArticleBlockView.$(class_name).removeClass('u-hide');
                //Editor
                if(type == 'text'){
                    //Wait till editor is ready
                    newArticleBlockView.$('.block-text').bind('click mousedown dblclick', function(ev){
                       ev.preventDefault();
                       ev.stopImmediatePropagation();
                    });
                    richTextEditor = setUpAlloyToolbar(false, document.querySelector('.body-h-content'), false, false);
                    var nativeEditor = richTextEditor.get('nativeEditor');
                    //On editor ready
                    nativeEditor.on('instanceReady', function(ev){
                        newArticleBlockView.$('.block-text').unbind();
                    });
                    //On image upload
                    nativeEditor.on('imageAdd', function(ev){
                        var id = generateRandomUUID();
                        ev.data.file.id = id;
                        richTextEditorFiles.push(ev.data.file);
                        $(ev.data.el.$).addClass('upload-image').attr('data-id', id);
                    });
                } else if(type == 'audio' || type == 'video' || type == 'file'){
                    newArticleBlockView.$('.direct-upload').each(function(){
                        //For each file selected, process and upload
                        var form = $(this);
                        var fileCount = 0;
                        var uploadCount = 0;
                        $(this).fileupload({
                            dropZone: $('#drop'),
                            url: form.attr('action'), //Grab form's action src
                            type: 'POST',
                            autoUpload: true,
                            dataType: 'xml', //S3's XML response,
                            add: function(event, data){
                                if(data.files[0].size >= MAX_FILE_SIZE) return;
                                fileCount += 1;
                                //Upload through CORS
                                $.ajax({
                                    url: '/api/signed',
                                    type: 'GET',
                                    dataType: 'json',
                                    data: {title: data.files[0].name}, // Send filename to /signed for the signed response
                                    async: false,
                                    success: function(data){
                                        // Now that we have our data, we update the form so it contains all
                                        // the needed data to sign the request
                                        form.find('input[name=key]').val(data.key);
                                        form.find('input[name=policy]').val(data.policy);
                                        form.find('input[name=signature]').val(data.signature);
                                        form.find('input[name=Content-Type]').val(data.contentType);
                                    }
                                });
                                data.files[0].s3Url = form.find('input[name=key]').val();
                                data.submit();
                            },
                            start: function(e){
                                $('#drop span').html('Uploaded <b></b>');
                            },
                            progressall: function(e, data){
                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                $('#drop span b').text(progress + '%'); // Update progress bar percentage
                            },
                            fail: function(e, data){
                                $('#drop span').html('Choose files or drag and drop them here');
                            },
                            done: function(e, data){
                                var file_name = data.files[0].name;
                                //Get extension of the file
                                var index = file_name.lastIndexOf('.');
                                var file_ext = file_name.substring(index+1, file_name.length);
                                //Get block title
                                if(newArticleBlockView.$('.block-file-title').val()){
                                    var block_title = newArticleBlockView.$('.block-file-title').val().trim();
                                } else {
                                    var block_title = decodeURIComponent(file_name.substring(0, index));
                                }
                                //Url
                                var url = 'https://d27gr4uvgxfbqz.cloudfront.net/' +  encodeURIComponent(data.files[0].s3Url).replace(/'/g,"%27").replace(/"/g,"%22");
                                //Get extension
                                var image_extensions = ['jpg', 'png', 'gif', 'jpeg'];
                                if(image_extensions.indexOf(file_ext) < 0) {
                                    //Save file
                                    var new_block = new DashManager.Entities.ArticleBlock({
                                        type: type,
                                        title: block_title,
                                        provider: {
                                            name: 'MGIEP',
                                            url: url
                                        },
                                        file: {
                                            size: data.files[0].size,
                                            ext: file_ext
                                        },
                                        block: $('.all-blocks').data('block')
                                    });
                                    //Order
                                    if($('.one-block').hasClass('selected')){
                                        var order = $('.one-block.selected').data('order') + 1;
                                    } else {
                                        var num_blocks = $('.all-blocks .one-block').length;
                                        var order = num_blocks + 1;
                                    }
                                    new_block.set('order', order);
                                    //Save block
                                    new_block.save({}, {success: function(){
                                        uploadCount += 1;
                                        DashManager.commands.execute('close:overlay');
                                        DashManager.vent.trigger('add:articleBlock', new_block);
                                    }});
                                } else {
                                    //Save image
                                    var new_block = new DashManager.Entities.ArticleBlock({
                                        type: 'image',
                                        title: block_title,
                                        provider: {
                                            name: 'MGIEP',
                                            url: url
                                        },
                                        file: {
                                            size: data.files[0].size,
                                            ext: file_ext
                                        },
                                        image: url,
                                        block: $('.all-blocks').data('block')
                                    });
                                    //Order
                                    if($('.one-block').hasClass('selected')){
                                        var order = $('.one-block.selected').data('order') + 1;
                                    } else {
                                        var num_blocks = $('.all-blocks .one-block').length;
                                        var order = num_blocks + 1;
                                    }
                                    new_block.set('order', order);
                                    //Image
                                    var image = new Image();
                                    image.src = window.URL.createObjectURL( data.files[0] );
                                    image.onload = function() {
                                        var bound = ( image.naturalHeight * 400 ) / image.naturalWidth;
                                        if(bound) {
                                            bound = parseInt(bound);
                                            new_block.set('bound', bound);
                                        }
                                        window.URL.revokeObjectURL(image.src);
                                        //Save block
                                        new_block.save({}, {success: function(){
                                            uploadCount += 1;
                                            DashManager.commands.execute('close:overlay');
                                            DashManager.vent.trigger('add:articleBlock', new_block);
                                        }});
                                    };
                                    image.onerror = function(){
                                        //Save block
                                        new_block.save({}, {success: function(){
                                            uploadCount += 1;
                                            DashManager.commands.execute('close:overlay');
                                            DashManager.vent.trigger('add:articleBlock', new_block);
                                        }});
                                    };
                                }
                            }
                        });
                    });
                }
                //Typeahead for persons
                //Remote fetch person list
                var personList = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                    queryTokenizer: Bloodhound.tokenizers.obj.whitespace,
                    remote: {
                        url: '/api/search/persons?text=%QUERY&excluded=%EXCLUDED',
                        replace: function(url){
                            return url.replace('%EXCLUDED', JSON.stringify(excluded_ids)).replace('%QUERY', newArticleBlockView.$('.person-input.tt-input').val());
                        },
                        filter: function(parsedResponse){
                            return parsedResponse;
                        }
                    }
                });
                //Initialize personlist
                personList.initialize();
                //Show typeahead
                newArticleBlockView.$('.person-input').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'email',
                    displayKey: 'name',
                    limit: 5,
                    source: personList.ttAdapter(),
                    templates: {
                        empty: [
                          '<div class="no-find">',
                          'No such user exists. Add new in People tab.',
                          '</div>'
                        ].join('\n'),
                        suggestion: Handlebars.compile("<p class='name'>{{name}}</p><p class='email'>{{email}}</p>")
                    }
                });
                //Focus
                newArticleBlockView.$('.person-input').focus();
                //Add new person on typeahead autocomplete
                newArticleBlockView.$('.person-input').on('typeahead:selected typeahead:autocompleted', function(e, datum){
                    var $input = newArticleBlockView.$('.person-input');
                    $input.typeahead('val','').focus();
                    newArticleBlockView.$('.persons-list').append("<div class='one-person' data-id='"+datum._id+"'><p class='name'>"+datum.name+"<br><span>"+datum.email+"</span></p><span class='remove-person u-delete'>Remove</span></div>");
                    excluded_ids.push(datum._id);
                });
                //Typeahead for partners
                //Remote fetch partner list
                var partnerList = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                    queryTokenizer: Bloodhound.tokenizers.obj.whitespace,
                    remote: {
                        url: '/api/search/partners?text=%QUERY&excluded=%EXCLUDED',
                        replace: function(url){
                            return url.replace('%EXCLUDED', JSON.stringify(excluded_ids)).replace('%QUERY', newArticleBlockView.$('.partner-input.tt-input').val());
                        },
                        filter: function(parsedResponse){
                            return parsedResponse;
                        }
                    }
                });
                //Initialize partnerList
                partnerList.initialize();
                //Show typeahead
                newArticleBlockView.$('.partner-input').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'email',
                    displayKey: 'name',
                    limit: 5,
                    source: partnerList.ttAdapter(),
                    templates: {
                        empty: [
                          '<div class="no-find">',
                          'No such partner exists. Add new in People tab.',
                          '</div>'
                        ].join('\n'),
                        suggestion: Handlebars.compile("<p class='name'>{{name}}</p>")
                    }
                });
                //Focus
                newArticleBlockView.$('.partner-input').focus();
                //Add new partner on typeahead autocomplete
                newArticleBlockView.$('.partner-input').on('typeahead:selected typeahead:autocompleted', function(e, datum){
                    var $input = newArticleBlockView.$('.partner-input');
                    $input.typeahead('val','').focus();
                    newArticleBlockView.$('.partners-list').append("<div class='one-person' data-id='"+datum._id+"'><p class='name'>"+datum.name+"</p><span class='remove-person u-delete'>Remove</span></div>");
                    excluded_ids.push(datum._id);
                });
            });
            //Remove person
            newArticleBlockView.on('remove:person', function(value){
                //Remove element from excluded ids
                var index = excluded_ids.indexOf(value.person_id);
                if(index > -1){
                    excluded_ids.splice(index, 1);
                }
            });
            //Save body HTML
            newArticleBlockView.on('save:articleHtmlBlock', function(value){
                if(richTextEditor){
                    var nativeEditor = richTextEditor.get('nativeEditor');
                    var text = nativeEditor.getData();
                    var new_block = new DashManager.Entities.ArticleBlock({
                        type: value.type,
                        title: value.title,
                        text: text,
                        order: value.order,
                        block: value.block
                    });
                    //Save and upload image
                    async.series([
                        function(callback){
                            if(newArticleBlockView.$('.body-h-content .upload-image').length){
                                newArticleBlockView.$('.js-save-block').text('Uploading...').addClass('uploading');
                                //Upload
                                editorUploadImage(richTextEditorFiles, function(image_urls){
                                    richTextEditorFiles = [];
                                    if(image_urls && image_urls.length){
                                        new_block.set('text', nativeEditor.getData());
                                        new_block.set('images', image_urls);
                                        callback();
                                    } else {
                                        callback();
                                    }
                                });
                            } else {
                                callback();
                            }
                        }
                    ],
                    function(err){
                        newArticleBlockView.$('.js-save-block').text('Save').removeClass('uploading');
                        new_block.save({}, {success: function(){
                            richTextEditor.destroy();
                            DashManager.vent.trigger('add:articleBlock', new_block);
                            DashManager.commands.execute('close:overlay');
                        }});
                    });
                }
            });
            //Save articleBlock
            newArticleBlockView.on('save:articleBlock', function(value){
                if(value.type == 'gallery'){
                    //Gallery
                    var new_block = new DashManager.Entities.ArticleBlock({
                        type: 'gallery',
                        gallery: value.gallery,
                        order: value.order,
                        block: value.block
                    });
                    new_block.save({}, {success: function(){
                        carouselImagesArray = [];
                        DashManager.vent.trigger('add:articleBlock', new_block);
                        DashManager.commands.execute('close:overlay');
                    }});
                } else if(value.type == 'link'){
                    //Link
                    if(!value.image){
                        var new_block = new DashManager.Entities.ArticleBlock({
                            type: 'link',
                            linkdata: value.linkdata,
                            order: value.order,
                            block: value.block
                        });
                        new_block.save({}, {success: function(){
                            DashManager.commands.execute('close:overlay');
                            DashManager.vent.trigger('add:articleBlock', new_block);
                        }});
                    } else {
                        var new_block = new DashManager.Entities.ArticleBlock({
                            type: 'link',
                            linkdata: value.linkdata,
                            image: value.image,
                            order: value.order,
                            block: value.block
                        });
                        //Get bound
                        var image = new Image();
                        image.src = value.image;
                        image.onload = function(){
                            var bound = ( this.height * 400 ) / this.width;
                            if(bound) {
                                bound = parseInt(bound);
                                new_block.set('bound', bound);
                            }
                            new_block.save({}, {success: function(){
                                DashManager.commands.execute('close:overlay');
                                DashManager.vent.trigger('add:articleBlock', new_block);
                            }});
                        };
                        image.onerror = function(){
                            new_block.save({}, {success: function(){
                                DashManager.commands.execute('close:overlay');
                                DashManager.vent.trigger('add:articleBlock', new_block);
                            }});
                        };
                    }
                } else if(value.type == 'button'){
                    //Button
                    var new_block = new DashManager.Entities.ArticleBlock({
                        type: 'button',
                        button_text: value.button_text,
                        button_url: value.button_url,
                        back_color: value.back_color,
                        order: value.order,
                        block: value.block
                    });
                    new_block.save({}, {success: function(){
                        DashManager.commands.execute('close:overlay');
                        DashManager.vent.trigger('add:articleBlock', new_block);
                    }});
                } else if(value.type == 'embed'){
                    //Embed
                    var new_block = new DashManager.Entities.ArticleBlock({
                        type: 'embed',
                        title: value.title,
                        embed: value.embed,
                        order: value.order,
                        block: value.block
                    });
                    new_block.save({}, {success: function(){
                        DashManager.commands.execute('close:overlay');
                        DashManager.vent.trigger('add:articleBlock', new_block);
                    }});
                } else if(value.type == 'people' || value.type == 'logos'){
                    //People or logos
                    var new_block = new DashManager.Entities.ArticleBlock({
                        type: value.type,
                        title: value.title,
                        people: excluded_ids,
                        order: value.order,
                        block: value.block
                    });
                    new_block.save({}, {success: function(){
                        DashManager.commands.execute('close:overlay');
                        DashManager.vent.trigger('add:articleBlock', new_block);
                    }});
                } else if(value.type == 'gif'){
                    //GIF
                    var new_block = new DashManager.Entities.ArticleBlock({
                        type: 'gif',
                        gif_embed: value.gif_embed,
                        gif_url: value.gif_url,
                        width: value.width,
                        height: value.height,
                        order: value.order,
                        block: value.block
                    });
                    new_block.save({}, {success: function(){
                        DashManager.commands.execute('close:overlay');
                        DashManager.vent.trigger('add:articleBlock', new_block);
                    }});
                } else if(value.type == 'mcq'){
                    //MCQ
                    var new_block = new DashManager.Entities.ArticleBlock({
                        type: 'mcq',
                        title: value.title,
                        is_multiple: value.is_multiple,
                        order: value.order,
                        block: value.block
                    });
                    new_block.save({}, {success: function(){
                        DashManager.vent.trigger('mcqOptions:show', new_block.get('_id'));
                        DashManager.vent.trigger('add:articleBlock', new_block);
                    }});
                } else if(value.type == 'journal'){
                    //Journal
                    var new_block = new DashManager.Entities.ArticleBlock({
                        type: 'journal',
                        title: value.title,
                        text: value.text,
                        journal_type: value.journal_type,
                        order: value.order,
                        block: value.block
                    });
                    new_block.save({}, {success: function(){
                        DashManager.commands.execute('close:overlay');
                        DashManager.vent.trigger('add:articleBlock', new_block);
                    }});
                }
            });
            DashManager.overlayRegion.show(newArticleBlockView);
        },
        showEditArticleBlockOverlay: function(block_id){
            var excluded_ids = [];
            $('.overlay').show();
            //Fetch article block
            var fetchingArticleBlock = DashManager.request('articleBlock:entity', block_id);
            $.when(fetchingArticleBlock).done(function(block){
                //New article block view
                var newArticleBlockView = new DashManager.DashApp.EntityViews.NewArticleBlockView();
                //Editor
                var richTextEditor;
                var richTextEditorFiles = [];
                //Show
                newArticleBlockView.on('show', function(){
                    //Add edit class
                    newArticleBlockView.$('.overlay-box').addClass('edit-box');
                    //Animate overlay box
                    setTimeout(function(){
                        newArticleBlockView.$('.overlay-box').addClass('animate');
                    }, 100);
                    //Hide scroll on main page
                    DashManager.commands.execute('show:overlay');
                    newArticleBlockView.$('.js-delete-block').removeClass('u-hide');
                    //Show section based on type
                    var type = block.get('type');
                    //Fill values
                    if(type == 'text'){
                        newArticleBlockView.$('.block-text').removeClass('u-hide');
                        this.$('.body-h-content').html(block.get('text'));
                        //Wait till editor is ready
                        newArticleBlockView.$('.block-text').bind('click mousedown dblclick', function(ev){
                           ev.preventDefault();
                           ev.stopImmediatePropagation();
                        });
                        richTextEditor = setUpAlloyToolbar(false, document.querySelector('.body-h-content'), false, false);
                        var nativeEditor = richTextEditor.get('nativeEditor');
                        //On editor ready
                        nativeEditor.on('instanceReady', function(ev){
                            newArticleBlockView.$('.block-text').unbind();
                        });
                        //On image upload
                        nativeEditor.on('imageAdd', function(ev){
                            var id = generateRandomUUID();
                            ev.data.file.id = id;
                            richTextEditorFiles.push(ev.data.file);
                            $(ev.data.el.$).addClass('upload-image').attr('data-id', id);
                        });
                    } else if(type == 'gallery'){
                        newArticleBlockView.$('.block-gallery').removeClass('u-hide');
                        //Gallery
                        var all_images = block.get('gallery');
                        if(all_images && all_images.length){
                            for(var i=0; i<all_images.length; i++){
                                var image = all_images[i];
                                newArticleBlockView.$('.body-images-list').append("<div class='one-image' data-id='"+image._id+"'><p class='title'>"+image.file.l+"</p><span class='remove-image u-delete'>Remove</span></div>");
                            }
                        }
                    } else if(type == 'button'){
                        newArticleBlockView.$('.block-button').removeClass('u-hide');
                        newArticleBlockView.$('.block-button-title').val(block.get('button').text);
                        newArticleBlockView.$('.block-button-url').val(block.get('button').url);
                    } else if(type == 'embed'){
                        newArticleBlockView.$('.block-embed').removeClass('u-hide');
                        newArticleBlockView.$('.block-embed-title').val(block.get('title'));
                        newArticleBlockView.$('.block-embed-code').val(block.get('embed'));
                    } else if(type == 'people'){
                        newArticleBlockView.$('.block-people').removeClass('u-hide');
                        //People
                        newArticleBlockView.$('.people-title').val(block.get('title'));
                        newArticleBlockView.$('.people-desc').val(block.get('text'));
                        var all_people = block.get('people');
                        if(all_people && all_people.length){
                            for(var i=0; i<all_people.length; i++){
                                var people = all_people[i];
                                newArticleBlockView.$('.persons-list').append("<div class='one-person' data-id='"+people._id+"'><p class='name'>"+people.name+"<br><span>"+people.email+"</span></p><span class='remove-person u-delete'>Remove</span></div>");
                                excluded_ids.push(people._id);
                            }
                        }
                    } else if(type == 'logos'){
                        newArticleBlockView.$('.block-logos').removeClass('u-hide');
                        //Logos
                        newArticleBlockView.$('.logos-title').val(block.get('title'));
                        newArticleBlockView.$('.logos-desc').val(block.get('text'));
                        var all_partners = block.get('people');
                        if(all_partners && all_partners.length){
                            for(var i=0; i<all_partners.length; i++){
                                var partner = all_partners[i];
                                newArticleBlockView.$('.partners-list').append("<div class='one-person' data-id='"+partner._id+"'><p class='name'>"+partner.name+"<br><span>"+partner.email+"</span></p><span class='remove-person u-delete'>Remove</span></div>");
                                excluded_ids.push(partner._id);
                            }
                        }
                    } else if(type == 'mcq'){
                        newArticleBlockView.$('.block-mcq').removeClass('u-hide');
                        newArticleBlockView.$('.block-mcq-title').val(block.get('title'));
                        //Check if is_multiple
                        if(block.get('is_multiple')){
                            newArticleBlockView.$('.is-multiple-label input').prop('checked', true);
                        }
                    } else if(type == 'journal'){
                        newArticleBlockView.$('.block-journal').removeClass('u-hide');
                        newArticleBlockView.$('.block-journaling-title').val(block.get('title'));
                        //Show text
                        var html = block.get('text').replace(/<br\s*[\/]?>/gi, '\n');
                        var tmp = document.createElement('div');
                        tmp.innerHTML = html;
                        var text = tmp.textContent || tmp.innerText || "";
                        newArticleBlockView.$('.block-journaling-text').val(text);
                        //Hide other options
                        newArticleBlockView.$('.select-journal span').addClass('u-hide');
                        newArticleBlockView.$('.select-journal span.journal-' + block.get('journal_type')).removeClass('u-hide').addClass('selected');
                    } else {
                        newArticleBlockView.$('.js-save-block').addClass('u-hide');
                    }
                    //Typeahead for persons
                    //Remote fetch person list
                    var personList = new Bloodhound({
                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                        queryTokenizer: Bloodhound.tokenizers.obj.whitespace,
                        remote: {
                            url: '/api/search/persons?text=%QUERY&excluded=%EXCLUDED',
                            replace: function(url){
                                return url.replace('%EXCLUDED', JSON.stringify(excluded_ids)).replace('%QUERY', newArticleBlockView.$('.person-input.tt-input').val());
                            },
                            filter: function(parsedResponse){
                                return parsedResponse;
                            }
                        }
                    });
                    //Initialize personlist
                    personList.initialize();
                    //Show typeahead
                    newArticleBlockView.$('.person-input').typeahead({
                        hint: true,
                        highlight: true,
                        minLength: 1
                    },
                    {
                        name: 'email',
                        displayKey: 'name',
                        limit: 5,
                        source: personList.ttAdapter(),
                        templates: {
                            empty: [
                              '<div class="no-find">',
                              'No such user exists. Add new in People tab.',
                              '</div>'
                            ].join('\n'),
                            suggestion: Handlebars.compile("<p class='name'>{{name}}</p><p class='email'>{{email}}</p>")
                        }
                    });
                    //Focus
                    newArticleBlockView.$('.person-input').focus();
                    //Add new person on typeahead autocomplete
                    newArticleBlockView.$('.person-input').on('typeahead:selected typeahead:autocompleted', function(e, datum){
                        var edit_block = new DashManager.Entities.ArticleBlock({
                            _id: block_id,
                            _action: 'add_person'
                        });
                        //Set
                        edit_block.set({
                            person: datum._id
                        });
                        edit_block.save({}, {success: function(){
                            var $input = newArticleBlockView.$('.person-input');
                            $input.typeahead('val','').focus();
                            newArticleBlockView.$('.persons-list').append("<div class='one-person' data-id='"+datum._id+"'><p class='name'>"+datum.name+"<br><span>"+datum.email+"</span></p><span class='remove-person u-delete'>Remove</span></div>");
                            excluded_ids.push(datum._id);
                        }});
                    });
                    //Typeahead for partners
                    //Remote fetch partner list
                    var partnerList = new Bloodhound({
                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                        queryTokenizer: Bloodhound.tokenizers.obj.whitespace,
                        remote: {
                            url: '/api/search/partners?text=%QUERY&excluded=%EXCLUDED',
                            replace: function(url){
                                return url.replace('%EXCLUDED', JSON.stringify(excluded_ids)).replace('%QUERY', newArticleBlockView.$('.partner-input.tt-input').val());
                            },
                            filter: function(parsedResponse){
                                return parsedResponse;
                            }
                        }
                    });
                    //Initialize partnerList
                    partnerList.initialize();
                    //Show typeahead
                    newArticleBlockView.$('.partner-input').typeahead({
                        hint: true,
                        highlight: true,
                        minLength: 1
                    },
                    {
                        name: 'email',
                        displayKey: 'name',
                        limit: 5,
                        source: partnerList.ttAdapter(),
                        templates: {
                            empty: [
                              '<div class="no-find">',
                              'No such partner exists. Add new in People tab.',
                              '</div>'
                            ].join('\n'),
                            suggestion: Handlebars.compile("<p class='name'>{{name}}</p>")
                        }
                    });
                    //Focus
                    newArticleBlockView.$('.partner-input').focus();
                    //Add new partner on typeahead autocomplete
                    newArticleBlockView.$('.partner-input').on('typeahead:selected typeahead:autocompleted', function(e, datum){
                        var edit_block = new DashManager.Entities.ArticleBlock({
                            _id: block_id,
                            _action: 'add_person'
                        });
                        //Set
                        edit_block.set({
                            person: datum._id
                        });
                        edit_block.save({}, {success: function(){
                            var $input = newArticleBlockView.$('.partner-input');
                            $input.typeahead('val','').focus();
                            newArticleBlockView.$('.partners-list').append("<div class='one-person' data-id='"+datum._id+"'><p class='name'>"+datum.name+"<br><span>"+datum.email+"</span></p><span class='remove-person u-delete'>Remove</span></div>");
                            excluded_ids.push(datum._id);
                        }});
                    });
                });
                //Remove person
                newArticleBlockView.on('removeCreated:person', function(value){
                    var edit_block = new DashManager.Entities.ArticleBlock({
                        _id: block_id,
                        _action: 'remove_person'
                    });
                    //Set
                    edit_block.set({
                        person: value.person_id
                    });
                    edit_block.save({}, {
                        dataType: 'text',
                        success: function(){
                            //Remove element from excluded ids
                            var index = excluded_ids.indexOf(value.person_id);
                            if(index > -1){
                                excluded_ids.splice(index, 1);
                            }
                        }
                    });
                });
                //Add image
                newArticleBlockView.on('add:image', function(value){
                    var edit_block = new DashManager.Entities.ArticleBlock({
                        _id: block_id,
                        _action: 'add_image'
                    });
                    //Set
                    edit_block.set({
                        title: value.title,
                        image_l: value.image_l,
                        image_m: value.image_m,
                        button_text: value.button_text,
                        button_url: value.button_url,
                        bound: value.bound
                    });
                    edit_block.save({}, {success: function(){
                        newArticleBlockView.$('.body-images-list').append("<div class='one-image' data-id='"+edit_block.get('_id')+"'><p class='title'>"+edit_block.get('file').l+"</p><span class='remove-image u-delete'>Remove</span></div>");
                    }});
                });
                //Remove image
                newArticleBlockView.on('remove:image', function(value){
                    var edit_block = new DashManager.Entities.ArticleBlock({
                        _id: block_id,
                        _action: 'remove_image'
                    });
                    //Set
                    edit_block.set({
                        image: value.image_id
                    });
                    edit_block.save();
                });
                //Update HTML block
                newArticleBlockView.on('update:articleHtmlBlock', function(value){
                    if(richTextEditor){
                        var nativeEditor = richTextEditor.get('nativeEditor');
                        var text = nativeEditor.getData();
                        var edit_block = new DashManager.Entities.ArticleBlock({
                            _id: block_id,
                            _action: 'edit'
                        });
                        edit_block.set({
                            text: text
                        });
                        //Save and upload image
                        async.series([
                            function(callback){
                                if(newArticleBlockView.$('.body-h-content .upload-image').length){
                                    newArticleBlockView.$('.js-save-block').text('Uploading...').addClass('uploading');
                                    //Upload
                                    editorUploadImage(richTextEditorFiles, function(image_urls){
                                        richTextEditorFiles = [];
                                        if(image_urls && image_urls.length){
                                            edit_block.set('text', nativeEditor.getData());
                                            edit_block.set('images', image_urls);
                                            callback();
                                        } else {
                                            callback();
                                        }
                                    });
                                } else {
                                    callback();
                                }
                            }
                        ],
                        function(err){
                            newArticleBlockView.$('.js-save-block').text('Save').removeClass('uploading');
                            edit_block.save({}, {success: function(){
                                richTextEditor.destroy();
                                DashManager.commands.execute('close:overlay');
                            }});
                        });
                    }
                });
                //Update block
                newArticleBlockView.on('update:articleBlock', function(value){
                    var edit_block = new DashManager.Entities.ArticleBlock({
                        _id: block_id,
                        _action: 'edit'
                    });
                    //Set
                    edit_block.set({
                        title: value.title,
                        text: value.text,
                        button_text: value.button_text,
                        button_url: value.button_url,
                        back_color: value.back_color
                    });
                    edit_block.save({}, {success: function(){
                        DashManager.commands.execute('close:overlay');
                    }});
                });
                //Delete block
                newArticleBlockView.on('delete:articleBlock', function(value){
                    //Delete block
                    var block = new DashManager.Entities.ArticleBlock({
                        _id: block_id
                    });
                    block.destroy({
                        dataType: 'text',
                        success: function(model, response){
                            DashManager.commands.execute('close:overlay');
                            DashManager.vent.trigger('remove:articleBlock');
                        }
                    });
                });
                DashManager.overlayRegion.show(newArticleBlockView);
            });
        },
        showMcqOptionsOverlay: function(block_id, is_edit){
            if(is_edit) $('.overlay').show();
            var image_url, bound;
            //Fetch block
            var fetchingArticleBlock = DashManager.request('articleBlock:entity', block_id);
            $.when(fetchingArticleBlock).done(function(block){
                var options = new Backbone.Collection(block.get('mcqs'));
                var optionsView = new DashManager.DashApp.EntityViews.McqOptionsView({
                    collection: options
                });
                //Show
                optionsView.on('show', function(){
                    if(is_edit){
                        optionsView.$('.overlay-box').addClass('edit-box');
                        //Animate overlay box
                        setTimeout(function(){
                            optionsView.$('.overlay-box').addClass('animate');
                        }, 100);
                        //Hide scroll on main page
                        DashManager.commands.execute('show:overlay');
                    } else {
                        optionsView.$('.overlay-box').addClass('animate');
                    }
                    //Upload option image
                    $('.option-image-upload').each(function(){
                        /* For each file selected, process and upload */
                        var form = $(this);
                        $(this).fileupload({
                            dropZone: $('#option-image'),
                            url: form.attr('action'), //Grab form's action src
                            type: 'POST',
                            autoUpload: true,
                            dataType: 'xml', //S3's XML response,
                            add: function(event, data){
                                //Check file type
                                var fileType = data.files[0].name.split('.').pop(), allowedtypes = 'jpeg,jpg,png,gif';
                                if (allowedtypes.indexOf(fileType) < 0) {
                                    alert('Invalid file type, aborted');
                                    return false;
                                }
                                //Get bound
                                var image = new Image();
                                image.src = window.URL.createObjectURL(data.files[0]);
                                image.onload = function() {
                                    bound = ( image.naturalHeight * 200 ) / image.naturalWidth;
                                    if(bound) bound = parseInt(bound);
                                    window.URL.revokeObjectURL(image.src);
                                };
                                //Upload through CORS
                                $.ajax({
                                    url: '/api/signed',
                                    type: 'GET',
                                    dataType: 'json',
                                    data: {title: data.files[0].name}, // Send filename to /signed for the signed response
                                    async: false,
                                    success: function(data){
                                        // Now that we have our data, we update the form so it contains all
                                        // the needed data to sign the request
                                        form.find('input[name=key]').val(data.key);
                                        form.find('input[name=policy]').val(data.policy);
                                        form.find('input[name=signature]').val(data.signature);
                                        form.find('input[name=Content-Type]').val(data.contentType);
                                    }
                                });
                                data.submit();
                            },
                            send: function(e, data){
                                optionsView.$('#option-image span').html('Uploading <b>...</b>');
                                optionsView.$('.mcq-add').addClass('u-hide');
                                optionsView.$('.js-done').addClass('u-disabled');
                            },
                            progress: function(e, data){
                                var percent = Math.round((e.loaded / e.total) * 100);
                                optionsView.$('#option-image span b').text(percent + '%');
                            },
                            fail: function(e, data){
                                optionsView.$('#option-image span').html('Optional image');
                                optionsView.$('.mcq-add').removeClass('u-hide');
                                optionsView.$('.js-done').removeClass('u-disabled');
                            },
                            success: function(data){
                                image_url = 'https://d27gr4uvgxfbqz.cloudfront.net/' +  form.find('input[name=key]').val();
                                image_url = encodeURI(image_url);
                                optionsView.$('#option-image span').addClass('u-hide');
                                optionsView.$('#option-image').css('backgroundImage', 'url('+image_url+')');
                                optionsView.$('.mcq-add').removeClass('u-hide');
                                optionsView.$('.js-done').removeClass('u-disabled');
                            }
                        });
                    });
                });
                //Add option
                optionsView.on('add:option', function(value){
                    var new_option = new DashManager.Entities.ArticleBlock({
                        _id: block_id,
                        _action: 'add_option'
                    });
                    new_option.set({
                        text: value.text,
                        image: image_url,
                        bound: bound
                    });
                    new_option.save({}, {success: function(){
                        //Add option
                        var option = new_option.toJSON();
                        optionsView.collection.add(option);
                        //Reset
                        optionsView.$('.option-text').val('').focus();
                        optionsView.$('#option-image').css('backgroundImage', '');
                        optionsView.$('#option-image span').html('Optional image');
                        optionsView.$('.mcq-add').removeClass('u-hide');
                        optionsView.$('.js-done').removeClass('u-disabled');
                    }});
                });
                //Remove option
                optionsView.on('childview:remove:option', function(childView, model){
                    var block = new DashManager.Entities.ArticleBlock({
                        _id: block_id,
                        _action: 'remove_option'
                    });
                    block.set({
                        option: model.option_id
                    });
                    block.save({}, {
                        dataType: 'text',
                        success: function(){
                            childView.$el.remove();
                        }
                    });
                });
                DashManager.overlayRegion.show(optionsView);
            });
        },
        showFiles: function(){
            $(window).off('scroll', scrollHandler);
            //Show loading page
            var loadingView = new DashManager.Common.Views.Loading();
            DashManager.mainRegion.show(loadingView);
            //Select
            $('.nav-links a').removeClass('selected');
            $('.nav-links .js-files').addClass('selected');
            //Fetching files
            var fetchingFiles = DashManager.request('file:entities');
            $.when(fetchingFiles).done(function(files){
                var filesView = new DashManager.DashApp.EntityViews.FilesView({
                    collection: files
                });
                //Show
                filesView.on('show', function(){
                    //Masonry
                    $('.all-items').masonry({
                        itemSelector: '.one-item',
                        columnWidth: 450
                    });
                    //Pagination
                    $('.loading-data').data('page', 1).removeClass('u-loaded');
                    //Check if less than page size
                    if(files.length < PAGE_SIZE) {
                        $('.loading-data').addClass('u-loaded');
                    }
                    //Fetch more files
                    scrollHandler = function(){
                        if(($(window).scrollTop() + $(window).height() > $(document).height() - 50) &&  !$('.loading-data').hasClass('u-loaded') && !$('.loading-data').hasClass('u-loading')) {
                            $('.loading-data').slideDown().addClass('u-loading');
                            var fetchingMoreFiles = DashManager.request('file:entities', '', $('.loading-data').data('page') + 1);
                            $.when(fetchingMoreFiles).done(function(moreFiles){
                                files.add(moreFiles.models);
                                $('.loading-data').data('page',  $('.loading-data').data('page') + 1).slideUp().removeClass('u-loading');
                                if(moreFiles.length < PAGE_SIZE) {
                                    $('.loading-data').addClass('u-loaded');
                                }
                                //Find view by model and update masonry
                                for(var i=0; i<moreFiles.models.length; i++){
                                    var child = filesView.children.findByModel(moreFiles.models[i]);
                                    $('.all-items').masonry('appended', child.$el);
                                }
                            });
                        }
                    }
                    $(window).on('scroll', scrollHandler);
                    //Upload file
                    filesView.$('.direct-upload').each(function(){
                        //For each file selected, process and upload
                        var form = $(this);
                        var fileCount = 0;
                        var uploadCount = 0;
                        $(this).fileupload({
                            dropZone: $('#drop'),
                            url: form.attr('action'), //Grab form's action src
                            type: 'POST',
                            autoUpload: true,
                            dataType: 'xml', //S3's XML response,
                            add: function(event, data){
                                if(data.files[0].size >= MAX_FILE_SIZE) return;
                                fileCount += 1;
                                //Upload through CORS
                                $.ajax({
                                    url: '/api/signed',
                                    type: 'GET',
                                    dataType: 'json',
                                    data: {title: data.files[0].name}, // Send filename to /signed for the signed response
                                    async: false,
                                    success: function(data){
                                        // Now that we have our data, we update the form so it contains all
                                        // the needed data to sign the request
                                        form.find('input[name=key]').val(data.key);
                                        form.find('input[name=policy]').val(data.policy);
                                        form.find('input[name=signature]').val(data.signature);
                                        form.find('input[name=Content-Type]').val(data.contentType);
                                    }
                                });
                                data.files[0].s3Url = form.find('input[name=key]').val();
                                data.submit();
                            },
                            start: function(e){
                                $('#drop span').html('Uploaded <b></b>');
                            },
                            progressall: function(e, data){
                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                $('#drop span b').text(progress + '%'); // Update progress bar percentage
                            },
                            fail: function(e, data){
                                $('#drop span').html('Choose files or drag and drop them here');
                            },
                            done: function(e, data){
                                var file_name = data.files[0].name;
                                //Get extension of the file
                                var index = file_name.lastIndexOf('.');
                                var file_ext = file_name.substring(index+1, file_name.length);
                                var file_title = decodeURIComponent(file_name.substring(0, index));
                                //Url
                                var url = 'https://d27gr4uvgxfbqz.cloudfront.net/' +  encodeURIComponent(data.files[0].s3Url).replace(/'/g,"%27").replace(/"/g,"%22");
                                //Get extension
                                var image_extensions = ['jpg', 'png', 'gif', 'jpeg'];
                                if(image_extensions.indexOf(file_ext) < 0) {
                                    //Save file
                                    var new_file = new DashManager.Entities.File({
                                        title: file_title,
                                        url: url,
                                        size: data.files[0].size,
                                        ext: file_ext
                                    });
                                    //Save file
                                    new_file.save({}, {success: function(){
                                        uploadCount += 1;
                                        //Add to view
                                        files.add(new_file, {at: 0});
                                        filesView.$('.all-items').masonry('prepended', $('.one-item').eq(0));
                                    }});
                                } else {
                                    //Save image
                                    var new_file = new DashManager.Entities.File({
                                        title: file_title,
                                        url: url,
                                        image: url,
                                        size: data.files[0].size,
                                        ext: file_ext
                                    });
                                    var image = new Image();
                                    image.src = window.URL.createObjectURL( data.files[0] );
                                    image.onload = function() {
                                        var bound = ( image.naturalHeight * 400 ) / image.naturalWidth;
                                        if(bound) {
                                            bound = parseInt(bound);
                                            new_file.set('bound', bound);
                                        }
                                        window.URL.revokeObjectURL(image.src);
                                        //Save file
                                        new_file.save({}, {success: function(){
                                            uploadCount += 1;
                                            //Add to view
                                            files.add(new_file, {at: 0});
                                            filesView.$('.all-items').masonry('prepended', $('.one-item').eq(0));
                                        }});
                                    };
                                    image.onerror = function(){
                                        //Save file
                                        new_file.save({}, {success: function(){
                                            uploadCount += 1;
                                            //Add to view
                                            files.add(new_file, {at: 0});
                                            filesView.$('.all-items').masonry('prepended', $('.one-item').eq(0));
                                        }});
                                    };
                                }
                            }
                        });
                    });
                });
                DashManager.mainRegion.show(filesView);
            });
        },
        showEditFileOverlay: function(file_id){
            $('.overlay').show();
            //Fetch file
            var fetchingFile = DashManager.request('file:entity', file_id);
            $.when(fetchingFile).done(function(file){
                var editFileView = new DashManager.DashApp.EntityViews.EditFileView();
                //Show
                editFileView.on('show', function(){
                    //Animate overlay box
                    setTimeout(function(){
                        editFileView.$('.overlay-box').addClass('animate');
                    }, 100);
                    //Hide scroll on main page
                    DashManager.commands.execute('show:overlay');
                    //Fill values
                    editFileView.$('.file-title').val(file.get('title')).focus();
                });
                //Update file
                editFileView.on('update:file', function(value){
                    var edit_file = new DashManager.Entities.File({
                        _id: file_id
                    });
                    edit_file.set({
                        title: value.title
                    });
                    edit_file.save({}, {success: function(){
                        DashManager.commands.execute('close:overlay');
                    }});
                });
                //Delete file
                editFileView.on('delete:file', function(value){
                    var file = new DashManager.Entities.File({
                        _id: file_id
                    });
                    file.destroy({
                        dataType: 'text',
                        success: function(model, response){
                            DashManager.commands.execute('close:overlay');
                            DashManager.vent.trigger('remove:file');
                        }
                    });
                });
                DashManager.overlayRegion.show(editFileView);
            });
        },
        showNewPersonOverlay: function(){
            $('.overlay').show();
            //New person view
            var newPersonView = new DashManager.DashApp.EntityViews.NewPersonView();
            //Show
            newPersonView.on('show', function(){
                setTimeout(function(){
                    newPersonView.$('.overlay-box').addClass('animate');
                }, 100);
                //Hide scroll on main page
                DashManager.commands.execute('show:overlay');
                //Focus
                newPersonView.$('.person-name').focus();
                //Upload dp
                newPersonView.$('.dp-upload').each(function(){
                    /* For each file selected, process and upload */
                    var form = $(this);
                    $(this).fileupload({
                        dropZone: $('.person-dp'),
                        url: form.attr('action'), //Grab form's action src
                        type: 'POST',
                        autoUpload: true,
                        dataType: 'xml', //S3's XML response,
                        add: function(event, data){
                            //Check file type
                            var fileType = data.files[0].name.split('.').pop(), allowedtypes = 'jpeg,jpg,png,gif';
                            if(allowedtypes.indexOf(fileType) < 0) {
                                alert('Invalid file type, aborted');
                                return false;
                            }
                            //Upload through CORS
                            $.ajax({
                                url: '/api/signed',
                                type: 'GET',
                                dataType: 'json',
                                data: {title: data.files[0].name}, // Send filename to /signed for the signed response
                                async: false,
                                success: function(data){
                                    // Now that we have our data, we update the form so it contains all
                                    // the needed data to sign the request
                                    form.find('input[name=key]').val(data.key);
                                    form.find('input[name=policy]').val(data.policy);
                                    form.find('input[name=signature]').val(data.signature);
                                    form.find('input[name=Content-Type]').val(data.contentType);
                                }
                            });
                            data.submit();
                        },
                        send: function(e, data){
                            newPersonView.$('.upload-btn').html('Uploading <span>...</span>');
                        },
                        progress: function(e, data){
                            var percent = Math.round((e.loaded / e.total) * 100);
                            newPersonView.$('.upload-btn span').text(percent + '%');
                        },
                        fail: function(e, data){
                            newPersonView.$('.upload-btn').html('Upload');
                        },
                        success: function(data){
                            var image_url = 'https://d27gr4uvgxfbqz.cloudfront.net/' +  form.find('input[name=key]').val();
                            image_url = encodeURI(image_url);
                            //Show image
                            newPersonView.$('.person-image').css('backgroundImage', 'url('+image_url+')');
                            newPersonView.$('.person-image').data('image', image_url);
                            newPersonView.$('.upload-btn').html('Upload');
                        }
                    });
                });
            });
            //Save
            newPersonView.on('save:person', function(value){
                var new_person = new DashManager.Entities.Person({
                    name: value.name,
                    type: value.type,
                    about: value.about,
                    desc: value.desc,
                    email: value.email,
                    url: value.url,
                    image: value.image
                });
                new_person.save({}, {success: function(){
                    DashManager.vent.trigger('add:person', new_person);
                    DashManager.commands.execute('close:overlay');
                }});
            });
            DashManager.overlayRegion.show(newPersonView);
        },
        showEditPersonOverlay: function(person_id){
            $('.overlay').show();
            //Fetch person
            var fetchingPerson = DashManager.request('person:entity', person_id);
            $.when(fetchingPerson).done(function(person){
                var newPersonView = new DashManager.DashApp.EntityViews.NewPersonView();
                //Show
                newPersonView.on('show', function(){
                    //Add edit class
                    newPersonView.$('.overlay-box').addClass('edit-box');
                    //Animate overlay box
                    setTimeout(function(){
                        newPersonView.$('.overlay-box').addClass('animate');
                    }, 100);
                    //Hide scroll on main page
                    DashManager.commands.execute('show:overlay');
                    newPersonView.$('.js-delete-person').removeClass('u-hide');
                    //Fill values
                    newPersonView.$('.person-name').val(person.get('name')).focus();
                    newPersonView.$('.person-about').val(person.get('about'));
                    newPersonView.$('.person-email').val(person.get('email'));
                    newPersonView.$('.person-url').val(person.get('url'));
                    //Show type
                    if(person.get('type') == 'team'){
                        newPersonView.$('.choose-team').addClass('selected');
                    } else if(person.get('type') == 'partner'){
                        newPersonView.$('.choose-partner').addClass('selected');
                    }
                    //Show description
                    var html = person.get('desc').replace(/<br\s*[\/]?>/gi, '\n');
                    var tmp = document.createElement('div');
                    tmp.innerHTML = html;
                    var desc = tmp.textContent || tmp.innerText || "";
                    newPersonView.$('.person-desc').val(desc);
                    //Show image
                    if(person.get('image')){
                        var image_url = person.get('image').m;
                        newPersonView.$('.person-image').css('backgroundImage', 'url('+image_url+')');
                        newPersonView.$('.person-image').data('image', image_url);
                    }
                    //Upload dp
                    newPersonView.$('.dp-upload').each(function(){
                        /* For each file selected, process and upload */
                        var form = $(this);
                        $(this).fileupload({
                            dropZone: $('.person-dp'),
                            url: form.attr('action'), //Grab form's action src
                            type: 'POST',
                            autoUpload: true,
                            dataType: 'xml', //S3's XML response,
                            add: function(event, data){
                                //Check file type
                                var fileType = data.files[0].name.split('.').pop(), allowedtypes = 'jpeg,jpg,png,gif';
                                if(allowedtypes.indexOf(fileType) < 0) {
                                    alert('Invalid file type, aborted');
                                    return false;
                                }
                                //Upload through CORS
                                $.ajax({
                                    url: '/api/signed',
                                    type: 'GET',
                                    dataType: 'json',
                                    data: {title: data.files[0].name}, // Send filename to /signed for the signed response
                                    async: false,
                                    success: function(data){
                                        // Now that we have our data, we update the form so it contains all
                                        // the needed data to sign the request
                                        form.find('input[name=key]').val(data.key);
                                        form.find('input[name=policy]').val(data.policy);
                                        form.find('input[name=signature]').val(data.signature);
                                        form.find('input[name=Content-Type]').val(data.contentType);
                                    }
                                });
                                data.submit();
                            },
                            send: function(e, data){
                                newPersonView.$('.upload-btn').html('Uploading <span>...</span>');
                            },
                            progress: function(e, data){
                                var percent = Math.round((e.loaded / e.total) * 100);
                                newPersonView.$('.upload-btn span').text(percent + '%');
                            },
                            fail: function(e, data){
                                newPersonView.$('.upload-btn').html('Upload');
                            },
                            success: function(data){
                                var image_url = 'https://d27gr4uvgxfbqz.cloudfront.net/' +  form.find('input[name=key]').val();
                                image_url = encodeURI(image_url);
                                //Edit person's image
                                var edit_person = new DashManager.Entities.Person({
                                    _id: person_id,
                                    _action: 'edit_image'
                                });
                                edit_person.set({
                                    image: image_url
                                });
                                edit_person.save({}, {success: function(){
                                    //Show image
                                    newPersonView.$('.person-image').css('backgroundImage', 'url('+image_url+')');
                                    newPersonView.$('.person-image').data('image', image_url);
                                    newPersonView.$('.upload-btn').html('Upload');
                                }});
                            }
                        });
                    });
                });
                //Update person
                newPersonView.on('update:person', function(value){
                    var edit_person = new DashManager.Entities.Person({
                        _id: person_id,
                        _action: 'edit'
                    });
                    edit_person.set({
                        name: value.name,
                        type: value.type,
                        about: value.about,
                        desc: value.desc,
                        email: value.email,
                        url: value.url
                    });
                    edit_person.save({}, {success: function(){
                        DashManager.commands.execute('close:overlay');
                    }});
                });
                //Delete person
                newPersonView.on('delete:person', function(value){
                    var person = new DashManager.Entities.Person({
                        _id: person_id
                    });
                    person.destroy({
                        dataType: 'text',
                        success: function(model, response){
                            DashManager.commands.execute('close:overlay');
                            DashManager.vent.trigger('remove:personCard');
                        }
                    });
                });
                DashManager.overlayRegion.show(newPersonView);
            });
        },
        showPeople: function(type){
            $(window).off('scroll', scrollHandler);
            //Show loading page
            var loadingView = new DashManager.Common.Views.Loading();
            DashManager.mainRegion.show(loadingView);
            //Select
            $('.nav-links a').removeClass('selected');
            $('.nav-links .js-people').addClass('selected');
            //Fetching persons
            var fetchingPersons = DashManager.request('person:entities', type);
            $.when(fetchingPersons).done(function(persons){
                var personsView = new DashManager.DashApp.EntityViews.PersonsView({
                    collection: persons
                });
                //Show
                personsView.on('show', function(){
                    //Masonry
                    $('.all-items').masonry({
                        itemSelector: '.one-item',
                        columnWidth: 450
                    });
                    //Choose filter
                    if(type){
                        var element = '.filter-items .filter-' + type;
                        personsView.$(element).addClass('selected');
                    }
                    //Pagination
                    $('.loading-data').data('page', 1).removeClass('u-loaded');
                    //Check if less than page size
                    if(persons.length < PAGE_SIZE) {
                        $('.loading-data').addClass('u-loaded');
                    }
                    //Fetch more persons
                    scrollHandler = function(){
                        if(($(window).scrollTop() + $(window).height() > $(document).height() - 50) &&  !$('.loading-data').hasClass('u-loaded') && !$('.loading-data').hasClass('u-loading')) {
                            $('.loading-data').slideDown().addClass('u-loading');
                            var fetchingMorePersons = DashManager.request('person:entities', type, $('.loading-data').data('page') + 1);
                            $.when(fetchingMorePersons).done(function(morePersons){
                                persons.add(morePersons.models);
                                $('.loading-data').data('page',  $('.loading-data').data('page') + 1).slideUp().removeClass('u-loading');
                                if(morePersons.length < PAGE_SIZE) {
                                    $('.loading-data').addClass('u-loaded');
                                }
                                //Find view by model and update masonry
                                for(var i=0; i<morePersons.models.length; i++){
                                    var child = personsView.children.findByModel(morePersons.models[i]);
                                    $('.all-items').masonry('appended', child.$el);
                                }
                            });
                        }
                    }
                    $(window).on('scroll', scrollHandler);
                });
                //Add person
                DashManager.vent.off('add:person');
                DashManager.vent.on('add:person', function(person){
                    persons.add(person, {at: 0});
                    personsView.$('.all-items').masonry('prepended', $('.one-item').eq(0));
                });
                //Remove personCard
                DashManager.vent.off('remove:personCard');
                DashManager.vent.on('remove:personCard', function(){
                    location.reload();
                });
                DashManager.mainRegion.show(personsView);
            });
        },
        showSettings: function(){
            //Show loading page
            var loadingView = new DashManager.Common.Views.Loading();
            DashManager.mainRegion.show(loadingView);
            //Select
            $('.nav-links a').removeClass('selected');
            $('.nav-links .js-settings').addClass('selected');
            //Fetch site details
            var fetchingSite = DashManager.request('siteDetails:entity');
            $.when(fetchingSite).done(function(site){
                var settingsView = new DashManager.DashApp.EntityViews.SettingsView({
                    model:site
                });
                //Show
                settingsView.on('show', function(){
                    document.title = 'Site settings - UNESCO MGIEP';
                    //Show contact
                    var html = site.get('contact').replace(/<br\s*[\/]?>/gi, '\n');
                    var tmp = document.createElement('div');
                    tmp.innerHTML = html;
                    var contact = tmp.textContent || tmp.innerText || "";
                    settingsView.$('.site-contact').val(contact);
                    //Show ticker
                    var all_tickers = site.get('ticker');
                    var ticker_string = '';
                    for(var i=0; i<all_tickers.length; i++){
                        if(i == all_tickers.length-1){
                            ticker_string += all_tickers[i].title + ', ' + all_tickers[i].url;
                        } else {
                            ticker_string += all_tickers[i].title + ', ' + all_tickers[i].url + '; ';
                        }
                    }
                    settingsView.$('.site-ticker').val(ticker_string);
                });
                //Update settings
                settingsView.on('update:site', function(value){
                    var site_settings = new DashManager.Entities.Site({
                        _id: site.get('_id')
                    });
                    site_settings.set({
                        name: value.name,
                        domain: value.domain,
                        title: value.title,
                        desc: value.desc,
                        contact: value.contact,
                        facebook: value.facebook,
                        twitter: value.twitter,
                        instagram: value.instagram,
                        youtube: value.youtube,
                        image_m: value.image_m,
                        image_l: value.image_l,
                        favicon: value.favicon,
                        apple: value.apple,
                        theme: value.theme,
                        subscribe_title: value.subscribe_title,
                        subscribe_desc: value.subscribe_desc,
                        notice_desc: value.notice_desc,
                        notice_link: value.notice_link,
                        ticker: value.ticker
                    });
                    site_settings.save({}, {success: function(){

                    }});
                });
                DashManager.mainRegion.show(settingsView);
            });
        },
        showActivity: function(){

        },
        showProfile: function(){

        },
        showSelectImageModal: function(){
            $('.modal').show();
            //Fetching files
            var fetchingFiles = DashManager.request('file:entities', 'images');
            $.when(fetchingFiles).done(function(files){
                var filesView = new DashManager.DashApp.EntityViews.SelectImageView({
                   collection: files
               });
               //Show
               filesView.on('show', function(){
                   //Animate overlay box
                   setTimeout(function(){
                       filesView.$('.modal-box').addClass('animate');
                   }, 100);
                   //Masonry
                   $('.all-items').masonry({
                       itemSelector: '.one-item',
                       columnWidth: 250
                   });
                   //Upload file
                   filesView.$('.direct-upload').each(function(){
                        //For each file selected, process and upload
                        var form = $(this);
                        var fileCount = 0;
                        var uploadCount = 0;
                        $(this).fileupload({
                            dropZone: $('#drop'),
                            url: form.attr('action'), //Grab form's action src
                            type: 'POST',
                            autoUpload: true,
                            dataType: 'xml', //S3's XML response,
                            add: function(event, data){
                                if(data.files[0].size >= MAX_FILE_SIZE) return;
                                fileCount += 1;
                                //Upload through CORS
                                $.ajax({
                                    url: '/api/signed',
                                    type: 'GET',
                                    dataType: 'json',
                                    data: {title: data.files[0].name}, // Send filename to /signed for the signed response
                                    async: false,
                                    success: function(data){
                                        // Now that we have our data, we update the form so it contains all
                                        // the needed data to sign the request
                                        form.find('input[name=key]').val(data.key);
                                        form.find('input[name=policy]').val(data.policy);
                                        form.find('input[name=signature]').val(data.signature);
                                        form.find('input[name=Content-Type]').val(data.contentType);
                                    }
                                });
                                data.files[0].s3Url = form.find('input[name=key]').val();
                                data.submit();
                            },
                            start: function(e){
                                $('#drop span').html('Uploaded <b></b>');
                            },
                            progressall: function(e, data){
                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                $('#drop span b').text(progress + '%'); // Update progress bar percentage
                            },
                            fail: function(e, data){
                                $('#drop span').html('Choose files or drag and drop them here');
                            },
                            done: function(e, data){
                                var file_name = data.files[0].name;
                                //Get extension of the file
                                var index = file_name.lastIndexOf('.');
                                var file_ext = file_name.substring(index+1, file_name.length);
                                var file_title = decodeURIComponent(file_name.substring(0, index));
                                //Url
                                var url = 'https://d27gr4uvgxfbqz.cloudfront.net/' +  encodeURIComponent(data.files[0].s3Url).replace(/'/g,"%27").replace(/"/g,"%22");
                                //Get extension
                                var image_extensions = ['jpg', 'png', 'gif', 'jpeg'];
                                if(image_extensions.indexOf(file_ext) < 0) {
                                    //Save file
                                    var new_file = new DashManager.Entities.File({
                                        title: file_title,
                                        url: url,
                                        size: data.files[0].size,
                                        ext: file_ext
                                    });
                                    //Save file
                                    new_file.save({}, {success: function(){
                                        uploadCount += 1;
                                        if(new_file.get('image') && new_file.get('image').l){
                                            //Add to view
                                            files.add(new_file, {at: 0});
                                            filesView.$('.all-items').masonry('prepended', $('.one-item').eq(0));
                                        }
                                    }});
                                } else {
                                    //Save image
                                    var new_file = new DashManager.Entities.File({
                                        title: file_title,
                                        url: url,
                                        image: url,
                                        size: data.files[0].size,
                                        ext: file_ext
                                    });
                                    var image = new Image();
                                    image.src = window.URL.createObjectURL( data.files[0] );
                                    image.onload = function() {
                                        var bound = ( image.naturalHeight * 400 ) / image.naturalWidth;
                                        if(bound) {
                                            bound = parseInt(bound);
                                            new_file.set('bound', bound);
                                        }
                                        window.URL.revokeObjectURL(image.src);
                                        //Save file
                                        new_file.save({}, {success: function(){
                                            uploadCount += 1;
                                            if(new_file.get('image') && new_file.get('image').l){
                                                //Add to view
                                                files.add(new_file, {at: 0});
                                                filesView.$('.all-items').masonry('prepended', $('.one-item').eq(0));
                                            }
                                        }});
                                    };
                                    image.onerror = function(){
                                        //Save file
                                        new_file.save({}, {success: function(){
                                            uploadCount += 1;
                                            if(new_file.get('image') && new_file.get('image').l){
                                                //Add to view
                                                files.add(new_file, {at: 0});
                                                filesView.$('.all-items').masonry('prepended', $('.one-item').eq(0));
                                            }
                                        }});
                                    };
                                }
                            }
                        });
                    });
                });
                DashManager.modalRegion.show(filesView);
            });
        }
    }
});