//Client side of MGIEP Website
var PageManager = new Backbone.Marionette.Application();
//Site url
var siteURL = 'http://mgiep.unesco.org/';
//Scroll value and scroll handler
var prevScroll, scrollHandler;
//Add regions of the application
PageManager.addRegions({
    mainRegion: '.mainWrap',
    contentRegion: '.contentWrap',
    overlayRegion: '.overlay',
    menuRegion: '.menuWrap',
    footerRegion: '.footerWrap'
});
//Navigate function to change url
PageManager.navigate = function(route, options){
    options || (options = {});
    Backbone.history.navigate(route, options);
};
//Find current route
PageManager.getCurrentRoute = function(){
  return Backbone.history.fragment;
};
//Start
PageManager.on('start', function(){
    if(Backbone.history){
        Backbone.history.start({pushState: true});
    }
    //Close overlay on ESC Key and mousedown
    $(document).keyup(function(e){
        if (e.keyCode == 27 && $('.overlay').css('display') != 'none') {
            PageManager.commands.execute('close:overlay');
        }
    });
    $(document).mousedown(function(e){
        var $target = $(e.target);
        //Close overlay
        var container = $('.overlay-box');
        if (container.length && !container.is(e.target) && container.has(e.target).length === 0) {
            PageManager.commands.execute('close:overlay');
        }
    });
    //On resize
    $(window).resize(function(){
        if($('body').width() >= 1419){
            var widthSide = parseInt(($('body').width() - 1419) / 2);
            var rightPx = widthSide + 25 + 'px';
            $('#earth-canvas').css('right', rightPx);
        } else {
            $('#earth-canvas').css('right', '25px');
        }
        //Offset of TECH Animation
        if($('body').hasClass('page-tech')){
            var rightOffset = $('body').width() - ($('.mainWrap').offset().left + $('.mainWrap').width()) -20;
            $('.topAnimation').css('right', rightOffset + 'px');
        }
    });
    //Cookie
    if($('body').hasClass('page-institute') && $('body').hasClass('page-home')){
        var visit_count = Cookies.get('visit_count');
        if(visit_count){
            visit_count = parseInt(visit_count) + 1;
            Cookies.set('visit_count', visit_count);
        } else {
            Cookies.set('visit_count', 1);
        }
        //Redirect if more than 3
        if(Cookies.get('visit_count')){
            var visit_count = parseInt(Cookies.get('visit_count'));
            if(visit_count >  3){
                window.location = '/newsroom';
                Cookies.set('visit_count', 0);
            }
        }
    }
});
//Show overlay
PageManager.commands.setHandler('show:overlay', function(){
    //Hide scroll on main page
    prevScroll = $('body').scrollTop();
    $('body').css('overflow', 'hidden');
    if($('body').width() < 700 || $('html').hasClass('touchevents')){
        $('html').css('overflow', 'hidden');
        $('html, body').css('position', 'fixed');
    }
    $('body').scrollTop(prevScroll);
});
PageManager.commands.setHandler('close:overlay', function(view){
    if(!$('.overlay-box > *').length) return;
    //remove animate class on overlay box
    $('.overlay-box').removeClass('animate');
    //after animation, remove view, change route and hide overlay
    setTimeout(function(){
        $('.overlay-box > *').remove();
        $('.overlay').hide();
        if(!$('.sidebarWrap').hasClass('u-mobile')){
            $('html, body').css('overflow', '').css('position', '');
            if(prevScroll) {
                $('html, body').scrollTop(prevScroll);
                prevScroll = '';
            }
        }
    }, 300);
});
//Router of the application
PageManager.module('PageApp', function (PageApp, PageManager, Backbone, Marionette, $, _) {
    PageApp.Router = Marionette.AppRouter.extend({
        appRoutes: {
            '' : 'pageView',
            'newsroom': 'pageView',
            ':url': 'pageView',
            'article/:slug': 'articleView'
        }
    });
    //API functions for each route
    var API = {
        pageView: function(){
            PageManager.PageApp.EntityController.Controller.showPage();
        },
        articleView: function(slug){
            PageManager.PageApp.EntityController.Controller.showArticle(slug);
        }
    };
    //Triggers to particular views
    //Show page
    PageManager.vent.on('page:show', function(){
        API.pageView();
    });
    //Show
    //Initialize router with API
    PageManager.addInitializer(function(){
        new PageApp.Router({ controller: API });
    });
});
//Models and Collections of the application
PageManager.module('Entities', function (Entities, PageManager, Backbone, Marionette, $, _) {
    //Page Model
    Entities.Page = Backbone.Model.extend({
        initialize: function(options){
            this._id = options._id;
        },
        url: function(){
            if(this._id) {
                return '/api/public/page/' + this._id
            }
        },
        idAttribute: '_id'
    });
    Entities.PageCollection = Backbone.Collection.extend({
        initialize: function(models, options){
            //_type is type of pages: all, public etc
            this._type = options._type;
        },
        url: function(){
            return '/api/pages/' + this._type
        },
        model: Entities.Page
    });
    //ArticleBlock Models and Collection
    Entities.ArticleBlock = Backbone.Model.extend({
        initialize: function(options){
            this._id = options._id;
        },
        url: function(){
            return '/api/articleblock/' + this._id
        },
        idAttribute: '_id'
    });
    Entities.ArticleBlockCollection = Backbone.Collection.extend({
        initialize: function(models, options){
            //slug is article slug
            this.slug = options.slug;
        },
        url: function(){
            return '/api/public/articleblocks/' + this.slug
        },
        model: Entities.ArticleBlock
    });
    //Functions to get data
    var API = {
        getPages: function(_type){
            var pages = new Entities.PageCollection([], {
                _type: _type
            });
            var defer = $.Deferred();
            pages.fetch({
                success: function(data){
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
        getOnePage: function(_id){
            var page = new Entities.Page({
                _id: _id
            });
            var defer = $.Deferred();
            page.fetch({
                success: function(data){
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
        getArticleBlocks: function(slug){
            var articleBlocks = new Entities.ArticleBlockCollection([], {
                slug: slug
            });
            var defer = $.Deferred();
            articleBlocks.fetch({
                success: function (data) {
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },
    };
    PageManager.reqres.setHandler('page:entities', function(_type){
        return API.getPages(_type);
    });
    PageManager.reqres.setHandler('page:entity', function(_id){
        return API.getOnePage(_id);
    });
    PageManager.reqres.setHandler('articleBlock:entities', function(_id){
        return API.getArticleBlocks(_id);
    });
});
//Views of the application
PageManager.module('PageApp.EntityViews', function (EntityViews, PageManager, Backbone, Marionette, $, _) {
    //Block item view
    EntityViews.PublicBlockItemView = Marionette.ItemView.extend({
        className: 'one-block',
        template: 'publicBlockItemTemplate',
        events: {
            'click .button-embed': 'showVideo'
        },
        showVideo: function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            var $target = $(ev.currentTarget);
            if($target.hasClass('block-btnb')){
                var embed_code = this.model.get('buttonb').embed;
            } else {
                var embed_code = this.model.get('button').embed;
            }
            $('.overlay .overlay-box').append(embed_code);
            $('.overlay').show();
            //Animate overlay box
            setTimeout(function(){
                $('.overlay .overlay-box').addClass('animate');
            }, 100);
            //Hide scroll on main page
            PageManager.commands.execute('show:overlay');
        }
    });
    //Page blocks view
    EntityViews.PublicBlocksView = Marionette.CollectionView.extend({
        className: 'blocksWrap',
        childView: EntityViews.PublicBlockItemView,
        initialize: function(){
            var blocks = this.model.get('blocks');
            this.collection = new Backbone.Collection(blocks);
        }
    });
    //Article block item view
    EntityViews.PublicArticleBlockItemView = Marionette.ItemView.extend({
        className: 'article-block',
        template: 'publicArticleBlockItemTemplate'
    });
    //Article blocks view
    EntityViews.PublicArticleBlocksView = Marionette.CollectionView.extend({
        className: 'publicBlocksWrap',
        childView: EntityViews.PublicArticleBlockItemView
    });
    //Public page item view
    EntityViews.PageItemView = Marionette.ItemView.extend({
        template: 'publicPageItemView',
        tagName: 'a',
        className: 'one-page',
        initialize: function(){
            this.$el.attr('data-order', this.model.get('order'));
            this.$el.attr('data-level', this.model.get('level'));
            //Link
            if(this.model.get('ref_url')){
                this.$el.attr('href', this.model.get('ref_url'));
            } else if(this.model.get('url')){
                this.$el.attr('href', siteURL + this.model.get('url'));
            }
        }
    });
    //Public pages view
    EntityViews.PublicPagesView = Marionette.CompositeView.extend({
        template: 'publicPagesTemplate',
        childView: EntityViews.PageItemView,
        childViewContainer: 'div.all-items'
    });
});
//Common Views of the application - Loading
PageManager.module('Common.Views', function(Views, PageManager, Backbone, Marionette, $, _){
    //Loading page
    Views.Loading = Marionette.ItemView.extend({
        tagName: 'div',
        className: 'loading-area',
        template: 'loadingTemplate'
    });
});
//Controllers of the Application
PageManager.module('PageApp.EntityController', function (EntityController, PageManager, Backbone, Marionette, $, _) {
    EntityController.Controller = {
        showPage: function(){
            //Fetch page
            var fetchingPage = PageManager.request('page:entity', $('.mainWrap').data('page'));
            $.when(fetchingPage).done(function(page){
                var publicBlocksView = new PageManager.PageApp.EntityViews.PublicBlocksView({
                    model: page
                });
                //Show
                publicBlocksView.on('show', function(){
                    if($('body').data('program')){
                        publicBlocksView.$('.header-program').text($('body').data('program')).removeClass('u-hide');
                    }
                    //Twitter widget load
                    if(publicBlocksView.$('.feed-block').length){
                        twttr.widgets.load();
                    }
                    //Gallery
                    if(publicBlocksView.$('.carousel-block').length){
                        $('.carousel-block').slick({
                            autoplay: true,
                            dots: true,
                            infinite: true,
                            speed: 300,
                            slidesToShow: 1,
                            centerMode: true,
                            variableWidth: true
                        });
                    }
                });
                PageManager.mainRegion.show(publicBlocksView);
            });
        },
        showArticle: function(slug){
            //Show loading page
            var loadingView = new PageManager.Common.Views.Loading();
            PageManager.contentRegion.show(loadingView);
            //Fetch article
            var fetchingArticleBlocks = PageManager.request('articleBlock:entities', slug);
            $.when(fetchingArticleBlocks).done(function(articleBlocks){
                var articleBlocksView = new PageManager.PageApp.EntityViews.PublicArticleBlocksView({
                    collection: articleBlocks
                });
                //Show
                articleBlocksView.on('show', function(){
                    //Gallery
                    if(articleBlocksView.$('.carousel-block').length){
                        $('.carousel-block').slick({
                            autoplay: true,
                            dots: true,
                            infinite: true,
                            speed: 300,
                            slidesToShow: 1,
                            centerMode: true,
                            variableWidth: true
                        });
                    }
                    //Show audio player
                    articleBlocksView.$('.audio-block audio').audioPlayer();
                    //Show audio player
                    if(articleBlocksView.$('.view-audio').length){
                        var audioPlayerOptions = {
                            controls: true,
                            width: 600,
                            height: 300,
                            fluid: false,
                            plugins: {
                                wavesurfer: {
                                    src: "live",
                                    waveColor: "#ffffff",
                                    progressColor: "#ffffff",
                                    debug: false,
                                    cursorWidth: 1,
                                    msDisplayMax: 20,
                                    hideScrollbar: true
                                }
                            }
                        };
                        articleBlocksView.$('.view-audio').each(function(index){
                            videojs(document.getElementsByClassName('view-audio')[index], audioPlayerOptions, function(){});
                        });
                    }
                    //Show video player
                    if(articleBlocksView.$('.view-video').length){
                       articleBlocksView.$('.view-video').each(function(index){
                           videojs(document.getElementsByClassName('view-video')[index], {}, function(){});
                       });
                    }
                    //Show audio journal
                    var audioJournalOptions = {
                        controls: true,
                        width: 600,
                        height: 300,
                        fluid: false,
                        plugins: {
                            wavesurfer: {
                                src: "live",
                                waveColor: "#ffffff",
                                progressColor: "#ffffff",
                                debug: false,
                                cursorWidth: 1,
                                msDisplayMax: 20,
                                hideScrollbar: true
                            },
                            record: {
                                audio: true,
                                video: false,
                                maxLength: 20,
                                debug: false
                            }
                        }
                    };
                    var audioJournalPlayers = [];
                    var audioPlayerIds = [];
                    articleBlocksView.$('.journal-audio').each(function(){
                        audioPlayerIds.push(this.id);
                        audioJournalPlayers.push(videojs(this.id, audioJournalOptions));
                    });
                    audioJournalPlayers.forEach(function(player, i){
                        //data is available
                        player.on('finishRecord', function(){
                            //Upload file
                            var element = $('#' + audioPlayerIds[i]).next().next();
                            element.each(function(){
                                //For each file selected, process and upload
                                var form = $(this);
                                $(this).fileupload({
                                    url: form.attr('action'), //Grab form's action src
                                    type: 'POST',
                                    autoUpload: true,
                                    dataType: 'xml', //S3's XML response,
                                    add: function(event, data){
                                        //Upload through CORS
                                        $.ajax({
                                            url: '/api/signed',
                                            type: 'GET',
                                            dataType: 'json',
                                            data: {title: data.files[0].name}, // Send filename to /signed for the signed response
                                            async: false,
                                            success: function(data){
                                                // Now that we have our data, we update the form so it contains all
                                                // the needed data to sign the request
                                                form.find('input[name=key]').val(data.key);
                                                form.find('input[name=policy]').val(data.policy);
                                                form.find('input[name=signature]').val(data.signature);
                                                form.find('input[name=Content-Type]').val(data.contentType);
                                            }
                                        });
                                        data.files[0].s3Url = form.find('input[name=key]').val();
                                        data.submit();
                                    },
                                    start: function(e){
                                        form.prev().html('Uploaded <b></b>');
                                    },
                                    progressall: function(e, data){
                                        var progress = parseInt(data.loaded / data.total * 100, 10);
                                        form.prev().find('b').text(progress + '%'); // Update progress bar percentage
                                    },
                                    fail: function(e, data){
                                        form.prev().html('Click microphone above to record your audio');
                                    },
                                    done: function(e, data){
                                    }
                                });
                            });
                        });
                    });
                    //Show video journal
                    var videoJournalOptions = {
                        controls: true,
                        width: 480,
                        height: 360,
                        fluid: false,
                        plugins: {
                            record: {
                                audio: true,
                                video: true,
                                maxLength: 10,
                                debug: false
                            }
                        }
                    };
                    var videoJournalPlayers = [];
                    var videoPlayerIds = [];
                    articleBlocksView.$('.journal-video').each(function(){
                        videoPlayerIds.push(this.id);
                        videoJournalPlayers.push(videojs(this.id, videoJournalOptions));
                    });
                    videoJournalPlayers.forEach(function(player, i){
                        //data is available
                        player.on('finishRecord', function(){
                            //Upload file
                            var element = $('#' + videoPlayerIds[i]).next().next();
                            element.each(function(){
                                //For each file selected, process and upload
                                var form = $(this);
                                $(this).fileupload({
                                    url: form.attr('action'), //Grab form's action src
                                    type: 'POST',
                                    autoUpload: true,
                                    dataType: 'xml', //S3's XML response,
                                    add: function(event, data){
                                        //Upload through CORS
                                        $.ajax({
                                            url: '/api/signed',
                                            type: 'GET',
                                            dataType: 'json',
                                            data: {title: data.files[0].name}, // Send filename to /signed for the signed response
                                            async: false,
                                            success: function(data){
                                                // Now that we have our data, we update the form so it contains all
                                                // the needed data to sign the request
                                                form.find('input[name=key]').val(data.key);
                                                form.find('input[name=policy]').val(data.policy);
                                                form.find('input[name=signature]').val(data.signature);
                                                form.find('input[name=Content-Type]').val(data.contentType);
                                            }
                                        });
                                        data.files[0].s3Url = form.find('input[name=key]').val();
                                        data.submit();
                                    },
                                    start: function(e){
                                        form.prev().html('Uploaded <b></b>');
                                    },
                                    progressall: function(e, data){
                                        var progress = parseInt(data.loaded / data.total * 100, 10);
                                        form.prev().find('b').text(progress + '%'); // Update progress bar percentage
                                    },
                                    fail: function(e, data){
                                        form.prev().html('Click microphone above to record your video');
                                    },
                                    done: function(e, data){
                                    }
                                });
                            });
                        });
                    });
                });
                PageManager.contentRegion.show(articleBlocksView);
            });
        }
    }
});
