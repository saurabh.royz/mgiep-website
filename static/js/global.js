//Global functions
//Format date
function formatDateInDDMMYYYY(date){
    var formattedDate = date.getDate() + '-' + (date.getMonth()+1) + '-' + date.getFullYear();
    return formattedDate;
}
function formatDateInMMYYYY(date){
    var formattedDate = (date.getMonth()+1) + '-' + date.getFullYear();
    return formattedDate;
}
//Function to group discussions by sections on basis of date
function groupDiscussionsByDate(discussions){
    var dateToday = new Date();
    var today = formatDateInDDMMYYYY(dateToday);
    //Yesterday
    dateToday.setDate(dateToday.getDate() - 1)
    var yesterday = formatDateInDDMMYYYY(dateToday);
    //Sections collection
    var sections = new Backbone.Collection();
    var sectionArray = [];
    discussions.each(function(discussion){
        //Get discussion object
        var discussionObj = discussion.toJSON();
        //Update time to local
        var discussion_date = formatDateInDDMMYYYY(new Date(discussion.get('updated_at')));
        var discussion_month = formatDateInMMYYYY(new Date(discussion.get('updated_at')));
        //Group
        if(sectionArray.indexOf(discussion_date) > -1 || sectionArray.indexOf(discussion_month) > -1){
            //Get section model
            if(discussion_date == today){
                var sectionModel = sections.where({date: 'Today'});
            } else if(discussion_date == yesterday){
                var sectionModel = sections.where({date: 'Yesterday'});
            } else {
                var sectionModel = sections.where({date: discussion_month});
            }
            sectionModel = sectionModel[0];
            //Add discussion
            sectionModel.get('discussions').push(discussionObj);
        } else {
            //Create section model
            var sectionModel = new Backbone.Model();
            //Set date
            if(discussion_date == today){
                sectionModel.set('date', 'Today');
                sectionArray.push(discussion_date);
            } else if(discussion_date == yesterday){
                sectionModel.set('date', 'Yesterday');
                sectionArray.push(discussion_date);
            } else {
                sectionModel.set('date', discussion_month);
                sectionArray.push(discussion_month);
            }
            //Add discussion
            sectionModel.set('discussions', [discussionObj]);
            //Add to sections collection
            sections.push(sectionModel);
        }
    });
    return sections;
}
//Function to group discussions by sections on basis of comment count
function groupDiscussionsByTop(discussions){
    //Sections collection
    var sections = new Backbone.Collection();
    var index = 0;
    var lastSectionModel;
    discussions.each(function(discussion){
        //Get discussion object
        var discussionObj = discussion.toJSON();
        //Group
        if(index % 20 == 0){
            //Create section model
            var sectionModel = new Backbone.Model();
            //Get range
            var startIndex = index + 1;
            var endIndex = index + 20;
            var range = 'Top: '+ startIndex + ' - ' + endIndex;
            //Set range
            sectionModel.set('range', range);
            //Add discussion
            sectionModel.set('discussions', [discussionObj]);
            //Add to sections collection
            sections.push(sectionModel);
            lastSectionModel = sectionModel;
        } else {
            //Push discussion
            lastSectionModel.get('discussions').push(discussionObj);
        }
        index++;
    });
    return sections;
}
//Place caret at end of contenteditable
function placeCaretAtEnd(el) {
    el.focus();
    if (typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {
        var range = document.createRange();
        range.selectNodeContents(el);
        range.collapse(false);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (typeof document.body.createTextRange != "undefined") {
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(el);
        textRange.collapse(false);
        textRange.select();
    }
}
//Function to get caret position
function getCaretCharacterOffsetWithin(element) {
    var caretOffset = 0;
    var doc = element.ownerDocument || element.document;
    var win = doc.defaultView || doc.parentWindow;
    var sel;
    if (typeof win.getSelection != "undefined") {
        sel = win.getSelection();
        if (sel.rangeCount > 0) {
            var range = win.getSelection().getRangeAt(0);
            var preCaretRange = range.cloneRange();
            preCaretRange.selectNodeContents(element);
            preCaretRange.setEnd(range.endContainer, range.endOffset);
            caretOffset = preCaretRange.toString().length;
        }
    } else if ( (sel = doc.selection) && sel.type != "Control") {
        var textRange = sel.createRange();
        var preCaretTextRange = doc.body.createTextRange();
        preCaretTextRange.moveToElementText(element);
        preCaretTextRange.setEndPoint("EndToEnd", textRange);
        caretOffset = preCaretTextRange.text.length;
    }
    return caretOffset;
}
//Insert text at cursor without changing cursor position
function insertTextAtCursor(text) {
    var sel, range, html;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            range.insertNode( document.createTextNode(text) );
        }
    } else if (document.selection && document.selection.createRange) {
        document.selection.createRange().text = text;
    }
}
//Scroll to selected
function scrollToSelectedBlock(index){
    var offset = $('.one-block').eq(index).offset().top - $(window).scrollTop();
    offset -= 50;
    $('html, body').animate({scrollTop: offset}, 800);
}
//Animation
(function() {
    if(!$('body').hasClass('page-home') && !$('body').hasClass('page-tech')) return;
    //Animation on home
    if($('body').hasClass('page-home')){
        var camera, drawStars, fillLight, mesh, renderer, scene;
        camera = mesh = scene = renderer = fillLight = void 0;
        //Draw start
        drawStars = function() {
            var canvas, ctx, i, j, sizeRandom;
            canvas = document.createElement('canvas');
            canvas.setAttribute('width', window.innerWidth);
            canvas.setAttribute('height', window.innerHeight);
            canvas.setAttribute('id', "stars");
            ctx = canvas.getContext('2d');
            ctx.fillStyle = "#ffffff";
            for (i = j = 0; j <= 200; i = ++j) {
                ctx.beginPath();
                sizeRandom = Math.random() * 2;
                ctx.arc(Math.random() * window.innerWidth, Math.random() * window.innerHeight, sizeRandom, 0, 2 * Math.PI, 0);
                ctx.fill();
            }
            return document.body.appendChild(canvas);
        };
        window.onload = function() {
            var animate, base, baseMat, e, geometryBase, highTerran, highTerranMat, light, material, terran, terranGeom, terranHighGeom;
            drawStars();
            camera = new THREE.PerspectiveCamera(75, 500 / 500, 1, 10000);
            camera.position.z = 1000;
            scene = new THREE.Scene;
            geometryBase = new THREE.SphereGeometry(400, 50, 56);
            terranGeom = new THREE.SphereGeometry(398, 30, 30);
            terranHighGeom = new THREE.SphereGeometry(390, 20, 20);
            baseMat = new THREE.MeshLambertMaterial({
                color: 0x0599f1,
                shading: THREE.FlatShading
            });
            material = new THREE.MeshLambertMaterial({
                color: 0xb8b658,
                shading: THREE.FlatShading
            });
            highTerranMat = new THREE.MeshLambertMaterial({
                color: 0xe3c97f,
                shading: THREE.FlatShading
            });
            geometryBase.vertices.forEach(function(v) {
                return v[["x", "y", "z"][~~(Math.random() * 3)]] += Math.random() * 10;
            });
            [terranHighGeom.vertices, terranGeom.vertices].forEach(function(g) {
                return g.forEach(function(v) {
                    return v[["x", "y", "z"][~~(Math.random() * 3)]] += Math.random() * 40;
                });
            });
            base = new THREE.Mesh(geometryBase, baseMat);
            terran = new THREE.Mesh(terranGeom, material);
            highTerran = new THREE.Mesh(terranHighGeom, highTerranMat);
            scene.add(base);
            base.add(terran);
            base.add(highTerran);
            light = new THREE.DirectionalLight(0xffffff);
            light.position.set(1, 1, 1);
            scene.add(light);
            fillLight = new THREE.AmbientLight(0x2e1527);
            scene.add(fillLight);
            try {
                renderer = new THREE.WebGLRenderer();
            } catch (error) {
                e = error;
                renderer = new THREE.CanvasRenderer();
            }
            renderer.setSize(500, 500);
            cv = renderer.domElement;
            cv.setAttribute('id', "earth-canvas");
            document.body.appendChild(cv);
            animate = function() {
                base.rotation.y += 0.00225;
                requestAnimationFrame(animate);
                return renderer.render(scene, camera);
            };
            if($('body').width() >= 1419){
                var widthSide = parseInt(($('body').width() - 1419) / 2);
                var rightPx = widthSide + 25 + 'px';
                $('#earth-canvas').css('right', rightPx);
            }
            return animate();
        };
    } else {
        //Show animation
        var rightOffset = $('body').width() - ($('.mainWrap').offset().left + $('.mainWrap').width()) -20;
        $('.topAnimation').css('right', rightOffset + 'px');
        //Navigation click
        $('.link-ref.link-about').click(function(ev){
            ev.preventDefault();
            scrollToSelectedBlock(7);
        });
        $('.link-ref.link-k-speakers').click(function(ev){
            ev.preventDefault();
            scrollToSelectedBlock(5);
        });
        $('.link-ref.link-c-speakers').click(function(ev){
            ev.preventDefault();
            scrollToSelectedBlock(14);
        });
        $('.link-ref.link-apply').click(function(ev){
            ev.preventDefault();
            scrollToSelectedBlock(3);
        });
        $('.link-ref.link-themes').click(function(ev){
            ev.preventDefault();
            scrollToSelectedBlock(16);
        });
        $('.link-ref.link-advisory').click(function(ev){
            ev.preventDefault();
            scrollToSelectedBlock(21);
        });
        $('.link-ref.link-venue').click(function(ev){
            ev.preventDefault();
            scrollToSelectedBlock(25);
        });
        //Show particles
        particlesJS(
            'js-particles', {
            'particles': {
                'number': {
                    'value': 200
                },
                'color': {
                    'value': ['#f7544c', '#f7ac03', '#4bb4e7', '#ff004d', '#12B1B7']
                },
                'shape': {
                    'type': 'circle'
                },
                'opacity': {
                    'value': 1,
                    'random': false,
                    'anim': {
                        'enable': false
                    }
                },
                'size': {
                    'value': 3,
                    'random': true,
                    'anim': {
                        'enable': false,
                    }
                },
                'line_linked': {
                    'enable': false
                },
                'move': {
                    'enable': true,
                    'speed': 2,
                    'direction': 'none',
                    'random': true,
                    'straight': false,
                    'out_mode': 'out'
                }
            },
            'interactivity': {
                'detect_on': 'canvas',
                'events': {
                    'onhover': {
                        'enable': false
                    },
                    'onclick': {
                        'enable': false
                    },
                    'resize': true
                }
            },
            'retina_detect': true
        });
    }
}).call(this);
