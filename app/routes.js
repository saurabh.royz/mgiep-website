var crypto = require('crypto'),
    uuid = require('node-uuid'),
    async = require('async'),
    mongoose = require('mongoose'),
    User = require('../app/models/user').User,
    LoginToken = require('../app/models/logintoken').LoginToken,
    Site = require('../app/models/entity').Site,
    Page = require('../app/models/entity').Page,
    Block = require('../app/models/entity').Block,
    Activity = require('../app/models/entity').Activity,
    Email = require('../config/mail.js');
//Utilities
var Utility = require('../app/utility');
//Routes
module.exports = function(app, passport) {
    var siteRoute = {
        home: function(req, res){
            Site.findOne({}, function(err, site){
                Page.findOne({category: 'institute'}, function(err, page){
                    if(!page) return res.sendStatus(404);
                    res.render('site/page', {
                        page: page._id,
                        title: page.title || site.title,
                        desc: page.desc || site.desc,
                        notice_desc: site.notice.desc,
                        notice_link: site.notice.link,
                        ticker: site.ticker,
                        theme: site.theme,
                        type: 'institute',
                        subtype: 'home'
                    });
                });
            });
        },
        newsroom: function(req, res){
            Site.findOne({}, function(err, site){
                Page.findOne({category: 'newsroom'}, function(err, page){
                    if(!page) return res.sendStatus(404);
                    res.render('site/page', {
                        page: page._id,
                        title: page.title || site.title,
                        desc: page.desc || site.desc,
                        notice_desc: site.notice.desc,
                        notice_link: site.notice.link,
                        ticker: site.ticker,
                        theme: site.theme,
                        type: 'newsroom',
                        subtype: 'home'
                    });
                });
            });
        },
        socialLogin: function(req, res){
            if(req.isAuthenticated()){
                res.redirect('/social');
            } else {
                Site.findOne({}, function(err, site){
                    //Send
                    res.render('site/social', {
                        notice_desc: site.notice.desc,
                        notice_link: site.notice.link,
                        ticker: site.ticker,
                        theme: site.theme,
                        type: 'social'
                    });
                });
            }
        },
        social: function(req, res){
            if(req.isAuthenticated()){
                Site.findOne({}, function(err, site){
                    Activity.find({user_for: req.user.id, is_new: true}, function(err, activities){
                        if(activities && activities.length){
                            var activityCount = activities.length;
                        } else {
                            var activityCount = 0;
                        }
                        //Render
                        res.render('site/social', {
                            userid: req.user.id,
                            name: req.user.name,
                            email: req.user.email,
                            username: req.user.username,
                            initials: req.user.initials,
                            dp: req.user.dp.s,
                            usertype: req.user.type,
                            anon: req.user.anon.id,
                            karma: req.user.karma,
                            notice_desc: site.notice.desc,
                            notice_link: site.notice.link,
                            ticker: site.ticker,
                            theme: site.theme,
                            activityCount: activityCount,
                            type: 'social'
                        });
                    });
                });
            } else if(req.url == '/social') {
                Site.findOne({}, function(err, site){
                    req.session.redirectURL = null;
                    //Send
                    res.render('site/social', {
                        notice_desc: site.notice.desc,
                        notice_link: site.notice.link,
                        ticker: site.ticker,
                        theme: site.theme,
                        type: 'social'
                    });
                });
            } else {
                req.session.redirectURL = req.url;
                res.redirect('/social/login');
            }
        },
        tech: function(req, res){
            Site.findOne({}, function(err, site){
                Page.findOne({url: 'tech2018'}, function(err, page){
                    if(!page) return res.sendStatus(404);
                    res.render('site/tech', {
                        page: page._id,
                        title: page.title || site.title,
                        desc: page.desc || site.desc,
                        notice_desc: site.notice.desc,
                        notice_link: site.notice.link,
                        ticker: site.ticker,
                        theme: site.theme,
                        program: page.program,
                        type: 'institute',
                        subtype: 'tech'
                    });
                });
            });
        },
        kindness: function(req, res){
            Site.findOne({}, function(err, site){
                Page.findOne({url: 'kindness'}, function(err, page){
                    if(!page) return res.sendStatus(404);
                    res.render('site/kindness', {
                        page: page._id,
                        title: page.title || site.title,
                        desc: page.desc || site.desc,
                        notice_desc: site.notice.desc,
                        notice_link: site.notice.link,
                        ticker: site.ticker,
                        theme: site.theme,
                        program: page.program,
                        type: 'institute',
                        subtype: 'kindness'
                    });
                });
            });
        },
        page: function(req, res){
            Site.findOne({}, function(err, site){
                Page.findOne({url: req.params.url}, function(err, page){
                    if(!page) return res.sendStatus(404);
                    res.render('site/page', {
                        page: page._id,
                        title: page.title || site.title,
                        desc: page.desc || site.desc,
                        notice_desc: site.notice.desc,
                        notice_link: site.notice.link,
                        ticker: site.ticker,
                        theme: site.theme,
                        program: page.program,
                        type: 'institute'
                    });
                });
            });
        },
        article: function(req, res){
            Block.findOne({slug: req.params.slug, type: 'content'}, function(err, article){
                if(!article) return res.sendStatus(404);
                if(article.url.ref) res.redirect(article.url.ref);
                else {
                    Site.findOne({}, function(err, site){
                        res.render('site/article', {
                            id: article._id,
                            slug: article.slug,
                            category: article.category,
                            title: article.text.title,
                            desc: article.text.desc,
                            image: article.image.m,
                            html: article.text.html,
                            embed: article.url.embed,
                            theme: site.theme,
                            type: 'institute'
                        });
                    });
                }
            });
        },
        dashboard: function(req, res){
            if(req.isAuthenticated()){
                Site.findOne({}, function(err, site){
                    //Render
                    res.render('app/dashboard', {
                        userid: req.user.id,
                        username: req.user.username,
                        initials: req.user.initials,
                        dp: req.user.dp.s,
                        type: req.user.type,
                        theme: site.theme
                    });
                });
            } else {
                res.redirect('/social/login');
            }
        }
    };
    //Site main page
    app.get('/', siteRoute.home);
    app.get('/newsroom', siteRoute.newsroom);
    app.get('/social/login', siteRoute.socialLogin);
    app.get('/social/signup', siteRoute.socialLogin);
    app.get('/social/forgot', siteRoute.socialLogin);
    app.get('/social', siteRoute.social);
    app.get('/social/:type', siteRoute.social);
    app.get('/social/tag/:slug', siteRoute.social);
    app.get('/social/tag/:slug/:type', siteRoute.social);
    app.get('/social/discussion/:slug', siteRoute.social);
    app.get('/social/me', siteRoute.social);
    app.get('/social/user/:slug', siteRoute.social);
    app.get('/social/me/:type', siteRoute.social);
    app.get('/social/user/:slug/:type', siteRoute.social);
    app.get('/dashboard', siteRoute.dashboard);
    app.get('/tech2018', siteRoute.tech);
    app.get('/kindness', siteRoute.kindness);
    app.get('/:url', siteRoute.page);
    app.get('/article/:slug', siteRoute.article);
    app.get('/dashboard/:type', siteRoute.dashboard);
    app.get('/dashboard/:type/:filter', siteRoute.dashboard);
    //process the login form
    app.post('/login',
        passport.authenticate('local-login', { failureRedirect: '/social/login', failureFlash: true}),
        function(req, res, next){
            // Issue a remember me cookie if the option was checked
            if (!req.body.remember_me) { return next(); }
            var token = req.user._id + uuid.v4();
            var hashed_token = crypto.createHash('md5').update(token).digest('hex');
            var loginToken = new LoginToken({userid: req.user._id, token: hashed_token });
            loginToken.save(function(){
                res.cookie('remember_me', token, {path: '/', httpOnly: true, maxAge: 604800000});
                return next();
            });
        },
        function(req, res) {
            if(req.session.redirectURL){
                res.redirect(req.session.redirectURL);
                req.session.redirectURL = null;
            } else {
                res.redirect('/social');
            }
        });
    //process the signup form
    app.post('/signup',
        passport.authenticate('local-signup', { failureRedirect: '/social/signup', failureFlash: true}),
        function(req, res, next){
            // Issue a remember me cookie
            var token = req.user._id + uuid.v4();
            var hashed_token = crypto.createHash('md5').update(token).digest('hex');
            var loginToken = new LoginToken({userid: req.user._id, token: hashed_token });
            loginToken.save(function(){
                res.cookie('remember_me', token, {path: '/', httpOnly: true, maxAge: 604800000});
                return next();
            });
        },
        function(req, res){
            if(req.session.redirectURL){
                res.redirect(req.session.redirectURL);
                req.session.redirectURL = null;
            } else {
                res.redirect('/social');
            }
        });
    //process the forgot password form
    app.post('/forgot', function(req, res, next){
        /* async waterfall - Runs the tasks array of functions in series,
         each passing their results to the next in the array. */
         async.waterfall([
            // create random reset Token
            function(done){
                crypto.randomBytes(32, function(err, buf){
                    var token = buf.toString('hex');
                    done(err, token);
                });
            },
            //Update user's resetPasswordToken and resetPasswordExpires
            function(token, done){
                User.findOne({ email: req.body.email}, function(err, user){
                    if(!user){
                        req.flash('errorMessage', 'No account with that email address exists');
                        return res.redirect('/forgot');
                    }
                    user.resetPasswordToken = token;
                    user.resetPasswordExpires = Date.now() + 60 * 60 * 1000; // 1 hour
                    user.save(function(err){
                        done(err, token, user);
                        req.flash('successMessage', 'An email has been sent to '+ user.email +' with further instructions.');
                        res.redirect('/');
                    });
                });
            },
            //send Password reset email
            function(token, user, done){
                var content = {
                    email: user.email,
                    name: user.name,
                    firstName: user.name.split(' ')[0],
                    subject: "UNESCO MGIEP's Social: Password Reset",
                    resetUrl: 'http://mgiep.unesco.org/social/reset/' + token
                };
                Email.sendOneMail('reset', content, function(err, responseStatus){
                    done(err, user.email);
                });
            }
        ], function(err, userEmail){
            if(err) return next(err);
            return true;
        });
    });
    //password reset page
    app.get('/social/reset/:token', function(req, res){
        User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: {$gt: Date.now()} }, function(err, user){
            if(!user){
                req.flash('errorMessage', 'Password reset token is invalid or has expired.')
                return res.redirect('/social/forgot');
            }
            res.render('site/reset', {email: user.email, token: req.params.token, errorMessage: req.flash('errorMessage')});
        });
    });
    //Save new password
    app.post('/reset/:token', function(req, res){
        async.waterfall([
            function(done){
                User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: {$gt: Date.now()} }, function(err, user){
                    if(!user){
                        req.flash('errorMessage', 'Password reset token is invalid or has expired.');
                        return res.redirect('/social/forgot');
                    } else if(!req.body.password || req.body.password.length < 8){
                        req.flash('errorMessage', 'Password must be 8 character or more.');
                        return res.redirect('/social/reset/' + req.params.token);
                    } else if(req.body.password != req.body.password2){
                        req.flash('errorMessage', "The passwords don't match, please try again.");
                        return res.redirect('/social/reset/' + req.params.token);
                    }
                    user.prev_password = user.password;
                    user.password = user.generateHash(req.body.password);
                    //Remove reset password token to make them invalid
                    user.resetPasswordToken = undefined;
                    user.resetPasswordExpires = undefined;
                    //Reset login Attempts on password change
                    user.loginAttempts = 0;
                    user.lockUntil = undefined;
                    user.save(function(err){
                        //Login - Passport exposes a login() function on req
                        req.login(user, function(err){
                            done(err, user);
                        });
                    });
                });
            },
            function(user, done){
                //Send password confirmation email
                var content = {
                    email: user.email,
                    name: user.name,
                    firstName: user.name.split(' ')[0],
                    subject: "UNESCO MGIEP's Social: Password changed",
                    resetUrl: 'http://mgiep.unesco.org/social/forgot'
                };
                Email.sendOneMail('password_changed', content, function(err, responseStatus){
                    done(err, responseStatus);
                });
            }
        ], function(err, responseStatus){
            if(err) return next(err);
            return res.redirect('/social');
        });
    });
    // Route for facebook authentication and login
    app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));
    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', { failureRedirect: '/social/login', failureFlash: true}),
        function(req, res, next){
            // Issue a remember me cookie
            var token = req.user._id + uuid.v4();
            var hashed_token = crypto.createHash('md5').update(token).digest('hex');
            var loginToken = new LoginToken({userid: req.user._id, token: hashed_token });
            loginToken.save(function(){
                res.cookie('remember_me', token, {path: '/', httpOnly: true, maxAge: 604800000});
                return next();
            });
        },
        function(req, res){
            if(req.session.redirectURL){
                res.redirect(req.session.redirectURL);
                req.session.redirectURL = null;
            } else {
                res.redirect('/social');
            }
        });
    // send to google to do the authentication
    // profile gets us their basic information including their name
    // email gets their emails
    app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));
    // the callback after google has authenticated the user
    app.get('/auth/google/callback',
        passport.authenticate('google', { failureRedirect: '/social/login', failureFlash: true}),
        function(req, res, next){
            // Issue a remember me cookie
            var token = req.user._id + uuid.v4();
            var hashed_token = crypto.createHash('md5').update(token).digest('hex');
            var loginToken = new LoginToken({userid: req.user._id, token: hashed_token });
            loginToken.save(function(){
                res.cookie('remember_me', token, {path: '/', httpOnly: true, maxAge: 604800000});
                return next();
            });
        },
        function(req, res){
            if(req.session.redirectURL){
                res.redirect(req.session.redirectURL);
                req.session.redirectURL = null;
            } else {
                res.redirect('/social');
            }
        });
    // send to twitter to do the authentication
    app.get('/auth/twitter', passport.authenticate('twitter'));
    // the callback after twitter has authenticated the user
    app.get('/auth/twitter/callback',
        passport.authenticate('twitter', { failureRedirect: '/social/login', failureFlash: true}),
        function(req, res, next){
            // Issue a remember me cookie
            var token = req.user._id + uuid.v4();
            var hashed_token = crypto.createHash('md5').update(token).digest('hex');
            var loginToken = new LoginToken({userid: req.user._id, token: hashed_token });
            loginToken.save(function(){
                res.cookie('remember_me', token, {path: '/', httpOnly: true, maxAge: 604800000});
                return next();
            });
        },
        function(req, res){
            if(req.session.redirectURL){
                res.redirect(req.session.redirectURL);
                req.session.redirectURL = null;
            } else {
                res.redirect('/social');
            }
        });
    //Logout handler by passport
    app.get('/site/logout', function(req, res){
        // clear the remember me cookie when logging out
        res.clearCookie('remember_me');
        req.logout();
        req.session.destroy(function(err){
            res.redirect('/social/login');
        });
    });
};